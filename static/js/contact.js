// Handle the contact form, antibot-style.
;(function () {

  // Replace the form action with it's real value.
  // This function is triggered if javascript registers:
  //  * a mouse movement
  //  * a touch movement
  //  * a keypress (Tab or Enter)
  var unlock_form = function() {
    var form_data = new FormData(document.getElementById("contact-form"));
    var form_id = form_data.get("real-action");
    $('#contact-form').attr('action', 'https://docs.google.com/forms/d/e/'+form_id+'/formResponse');
  };

  // On document load, bind to the trigger events.
  $(function() {

    // Mouse movement
    $('body').mousemove(function() {
      unlock_form();
    });

    // Touch movement
    $('body').bind('touchmove', function() {
      unlock_form();
    });

    $('body').keydown(function(e) {
      // Tab or enter key counts as human
      if ((e.keyCode == 9) || (e.keyCode == 13)) {
        unlock_form();
      }
    });
  });
}());

