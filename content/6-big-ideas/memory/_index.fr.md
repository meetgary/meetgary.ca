---
title: Un souvenir est comme un enregistrement vidéo
weight: 10

---

{{% lightbulb %}}
**Gary croit que nos souvenirs sont comme des enregistrements vidéo.** Lorsque nous nous souvenons, nous appuyons simplement sur le bouton pour rejouer la scène dans notre tête. Gary croit que les souvenirs de nos expériences troublantes sont particulièrement clairs et qu’ils perdurent. Il croit que ces souvenirs sont gravés dans notre mémoire.

Ainsi, Gary ne comprend pas pourquoi une personne qui dit la vérité pourrait **décrire une expérience différemment d’une fois à l’autre**. Il ne comprend pas qu’une personne qui dit la vérité puisse être **incapable de se souvenir des détails de ce qui est arrivé**. Il est encore plus sceptique si l’événement était particulièrement troublant.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary a beaucoup de doutes s’il pense que **l’histoire du demandeur a changé**. Il ne fait pas confiance aux demandeurs qui **se souviennent mal ou pas** de certains détails.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
Ennuis en raison de l’histoire sur le formulaire FDA
------------------------------------
{{% /red-flag %}}

{{% exclamation-highlight %}}
À l’audience, Gary a toujours une copie du formulaire FDA du demandeur. Il a souvent aussi les notes que l’agent d’IRCC a prises pendant l’entrevue du demandeur. Mais les demandeurs n’ont pas le droit d’avoir avec eux leur formulaire FDA ou les notes de l’agent au moment de l’audience. **Les demandeurs n’ont droit à aucune note pour les aider à témoigner.**
<br />
<br />
{{% /exclamation-highlight %}}

Dans son formulaire FDA, {{% name "Boniface" %}} a écrit qu’un des policiers qui l’a arrêté l’a frappé au visage, puis a craché au sol avant de l’insulter. À l’audience, Boniface a dit que le policier l’a insulté avant de cracher au sol, et l’a frappé au visage après. Gary a des doutes en raison des incohérences de son témoignage. Il pense que si l’histoire de Boniface était vraie, il se souviendrait clairement de l’ordre dans lequel les choses se sont passées.

**Qu’est-ce qui aurait aidé Boniface?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

Dans son formulaire FDA, {{% name "Annie" %}} a écrit que ses ravisseurs l’ont détenue pendant « une journée et une nuit ». À l’audience, elle a dit à Gary qu’elle a été enlevée en fin d’après-midi et relâchée le lendemain matin. Gary pense que l’histoire d’Annie a changé. Il pense que l’enlèvement n’a pas duré « une journée et une nuit ». Annie ne se souvient pas d’avoir écrit « une journée et une nuit » dans son formulaire. Elle insiste pour dire qu’il y a sûrement une erreur et qu’elle n’a pas utilisé cette phrase. Gary, exaspéré, lui montre cette phrase sur le formulaire FDA. Il a l’impression d’avoir décelé un mensonge.

Gary a décelé un problème sérieux dans l’histoire de {{% name "Graciela" %}}. À l’audience, elle affirme avoir vu une fusillade à l’heure de pointe du matin. Dans son formulaire FDA, elle a écrit qu’elle a vu cette fusillade « en revenant de l’école ». Gary lui a demandé l’heure à laquelle finissait l’école, et Graciela a confirmé que l’école se terminait en fin d’après-midi. Gary a des doutes en raison de cette incohérence.

**Qu’est-ce qui aurait aidé Annie et Graciela?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**


Dans son formulaire FDA, {{% name "Yvonne" %}} a écrit que « trois hommes dans une camionnette » sont venus au marché où elle travaillait et l’ont menacée. À l’audience, elle a dit « trois ou quatre hommes ». Quand Gary lui a posé des questions sur le véhicule, elle a dit que « c’était peut-être un Jeep ». Gary a l’impression que cette histoire est fausse. Si elle était vraie, Yvonne se souviendrait du nombre d’hommes et du véhicule qu’ils conduisaient.

**Qu’est-ce qui aurait aidé Yvonne?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% red-flag %}}
Ennuis en raison des notes de l’agent
--------------------------------------
{{% /red-flag %}}

Au moment de son arrivée au Canada, {{% name "Mustafa" %}} a dit à l’agent qui l’a interrogé qu’il prenait souvent part à des réunions de parti politique dans son pays. D’après les notes de l’agent, Mustafa a dit qu’il était allé à « une ou deux réunions par mois » au cours de la dernière année. À l’audience, Gary a demandé à Mustafa à combien de réunions il avait participé dans la dernière année. Il a répondu : « Probablement dix, peut-être douze. » Gary pense que l’histoire de Mustafa a changé.

**Qu’est-ce qui aurait aidé Mustafa?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**

Les notes de l’agente d’IRCC indiquent que {{% name "Tanzim" %}} a affirmé qu’il était « membre » d’un parti politique de l’opposition. À l’audience, Gary a demandé à voir une preuve. Tanzim a expliqué qu’il n’était pas membre, mais qu’il soutenait le parti. Gary a demandé à Tanzim d’expliquer pourquoi l’agente aurait écrit qu’il était membre alors qu’il n’était que partisan. Tanzim a répondu que l’agente devait avoir mal compris. Gary pense que l’agente n’aurait pas fait cette erreur. Il pense que Tanzim a changé son histoire quand il a réalisé qu’il devrait prouver qu’il était membre.

**Qu’est-ce qui aurait aidé Tanzim?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**


{{% red-flag %}}
Ennuis en raison de changements dans le témoignage du demandeur
------------------------------------------------------
{{% /red-flag %}}

Au début de l’audience de {{% name "Lupe" %}}, Gary lui a demandé combien de temps s’était écoulé entre le premier et le deuxième appel de menaces. Elle a répondu que le deuxième appel avait eu lieu « quelques semaines » après le premier. Plus tard au cours de l’audience, Lupe a dit qu’elle avait reçu le deuxième appel « environ un mois » après le premier. Gary pense que Lupe ment sûrement, parce que son histoire a changé.

**Qu’est-ce qui aurait aidé Lupe?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

Gary a demandé à {{% name "Samuel" %}} de décrire ce qu’il faisait pour soutenir son parti politique. Samuel a expliqué qu’il avait fondé un chapitre étudiant à son université, écrit un article dans le journal étudiant et encouragé ses camarades de classe à prendre part aux rassemblements. Plus tard, Gary a posé d’autres questions à Samuel sur ses activités politiques. Cette fois, Samuel a aussi mentionné qu’il avait distribué des dépliants à l’université. Comme il n’a pas mentionné les dépliants plus tôt, Gary pense que l’histoire a changé.

L’audience de {{% name "Kamala" %}} a duré deux jours. Le premier jour, Gary lui a demandé pourquoi elle n’avait pas avisé la police après son agression. Kamala a expliqué qu’elle avait honte. Elle ne voulait pas que sa famille et ses amis sachent ce qui lui était arrivé. Le deuxième jour, Gary a encore demandé à Kamala pourquoi elle n’avait pas avisé la police. Cette fois, elle a expliqué qu’elle avait trop honte et que la police n’aidait pas les femmes comme elle. Gary trouve que c’est louche. Il pense qu’entre les deux audiences, quelqu’un a dit à Kamala d’ajouter cette information au sujet de la police.

**Qu’est-ce qui aurait aidé Samuel et Kamala?**

* **{{< link "/12-things-that-have-helped/just-an-example" >}}**


{{% red-flag %}}
Ennuis parce que le demandeur ne sait pas quelque chose qu’il devrait savoir selon Gary
-------------------------------------------------------------------------------------------
{{% /red-flag %}}

{{% name "Jean-René" %}} a affirmé avoir pris part à une manifestation dans la capitale de son pays « au printemps » il y a plusieurs années. Gary lui a demandé le moment exact de cette manifestation. Jean-René a répondu que « c’était probablement à la fin avril ». Les rapports de la Commission sur ce pays indiquent que la manifestation a eu lieu au début de juin. Gary pense que cela révèle que Jean-René ment au sujet de sa participation à la manifestation.

**Qu’est-ce qui aurait aidé Jean-René?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Shani" %}} est venue au Canada à bord d’un bateau. Gary lui a demandé si elle se souvenait du nom du bateau. Elle a dit que non. Ainsi, Gary pense que Shani est arrivée au Canada par une autre route.

{{% name "Marco" %}} a raconté à Gary qu’il se tenait derrière le comptoir de sa boutique quand un groupe d’hommes armés à bord d’un camion est venu le menacer. Gary a demandé à Marco de décrire sa boutique. Il lui a ensuite demandé de décrire le camion à bord duquel les hommes étaient arrivés. Marco n’arrivait pas à se souvenir du camion. Gary trouve cela louche. De l’endroit où il se tenait, Marco aurait bien vu le camion pendant tout ce temps.

Au cours des semaines avant son arrestation, {{% name "Bijan" %}} a reçu la visite de représentants du gouvernement à plusieurs reprises. Gary a demandé à Bijan de décrire en détail la première visite des représentants chez lui : « Vers quelle heure sont-ils arrivés? Combien de temps sont-ils restés? Quelles étaient leurs questions? » Pour Bijan, les détails de cette première visite se mêlent aux détails de toutes les autres visites qui ont suivi. Quand il essaie de décrire cette première visite, il en parle de façon générale. Il décrit le genre de propos que tenaient les représentants, et leurs comportements typiques. Gary trouve louche que Bijan n’arrive pas à décrire la première visite en détail.

{{% name "Asmaan" %}} a été interrogée par des soldats à la frontière. Lorsque Gary lui demande de décrire les uniformes des soldats, elle ne s’en souvient pas. Elle pense que les uniformes étaient peut-être bleus. Gary détient des preuves que les uniformes étaient gris. Il pense qu’elle n’a jamais été interrogée à la frontière.

{{% name "Je-Tsun" %}} a travaillé pendant de nombreuses années pour un journal local. Gary lui a demandé de décrire le logo du journal. Je-Tsun a décrit un cimier avec un aigle, mais elle n’arrivait pas à se souvenir si la tête de l’aigle était tournée vers la droite ou la gauche. Gary pense que si elle avait vraiment vu ce logo tous les jours au travail, elle s’en souviendrait clairement.

**Qu’est-ce qui aurait aidé ces demandeurs?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Huan" %}} s’est converti à la chrétienté. Il a écrit dans son formulaire FDA qu’il va à l’église régulièrement depuis des années. Gary a posé des questions à Huan au sujet de passages de la Bible, et Huan ne pouvait pas répondre. Gary se demande si Huan s’est vraiment converti.

{{% name "Sunny" %}} s’est joint à un parti politique de l’opposition au côté de nombreux jeunes de sa région. Il a participé à des rassemblements et il a distribué des dépliants. Gary a demandé à Sunny d’expliquer la philosophie politique de son parti. Sunny a répondu que le parti était contre la corruption et qu’il soutenait les droits de sa minorité ethnique. Gary a des doutes, il pense que Sunny n’est pas vraiment membre d’un parti de l’opposition. S’il avait vraiment participé à des rassemblements régulièrement, il aurait compris plus en détail la plateforme du parti.

**Qu’est-ce qui aurait aidé Huan et Sunny?**

* **{{< link "/12-things-that-have-helped/living-their-beliefs" >}}**


Découvrir les autres Grandes Idées de Gary :

{{< siblings >}}