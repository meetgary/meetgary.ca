---
title: Un recuerdo es como una grabación de video
weight: 10

---

{{% lightbulb %}}
**Gary considera que nuestros recuerdos son como grabaciones de video.** Al recordar algo, simplemente pulsamos ‘reproducir’ y vemos la escena rodar de nuevo en la mente. Gary considera que nuestros recuerdos de experiencias perturbadoras son particularmente claros y duraderos, que quedan impresos en la memoria.

Por tanto, Gary no logra entender cómo una persona honesta podría **describir la misma experiencia de una manera distinta en dos ocasiones diferentes**. No logra entender cómo una persona honesta podría **ser incapaz de recordar los detalles de algo que les sucedió**. Duda aún más cuando lo que se describe es un evento perturbador.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary sospecha mucho cuando piensa que **la historia de una persona solicitante ha cambiado**. También desconfía de las personas solicitantes que **recuerdan mal o no recuerdan** ciertos tipos de detalles.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
Problema debido a la narración de BOC
------------------------------------
{{% /red-flag %}}

{{% exclamation-highlight %}}
Durante la audiencia, Gary siempre tiene en frente una copia del formulario Bases de la solicitud de asilo (BOC, por sus siglas en inglés) de la persona solicitante. Además, tiene las notas que tomó el oficial de IRCC durante la entrevista de la persona solicitante. Pero a las personas solicitantes no se les permite tener su formulario de BOC ni las notas del oficial en frente durante la audiencia. **Las personas solicitantes no tienen permitido ayudarse de ningún tipo de notas al testificar.**
<br />
<br />
{{% /exclamation-highlight %}}

En su BOC, {{% name "Boniface" %}} escribió que uno de los oficiales que lo detuvo lo golpeó en la cara y luego escupió en el suelo y lo insultó. Durante la audiencia, Boniface dice que el oficial primero lo insultó, luego escupió en el suelo, y luego lo golpeó en la cara. Gary tiene sospechas de esta inconsistencia. Piensa que, si la historia de Boniface fuese verdadera, este último recordaría claramente el orden en que sucedieron las cosas.

**¿Qué podría haber ayudado a Boniface?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

El formulario BOC de {{% name "Annie" %}} indica que sus secuestradores la mantuvieron en cautividad durante “un día y una noche”. Durante la audiencia, Annie le cuenta a Gary que fue secuestrada al caer la tarde y liberada la mañana siguiente. Gary piensa que la historia de Annie ha cambiado. Piensa que el secuestro que describe no duró “un día y una noche”. Annie no recuerda haber escrito “un día y una noche” en su BOC. Insiste que tiene que haber un error, pues nunca utilizó dicha frase. Gary, cada vez más frustrado, le muestra las palabras escritas en su BOC. Considera que la ha pescado mintiendo.

Gary encuentra un problema serio con la historia de {{% name "Graciela" %}}. Durante la audiencia, ella dice que vio un tiroteo durante la hora pico matutina. Pero en su BOC había escrito que había visto el tiroteo “cuando regresaba de clases a mi casa”. Gary pregunta a qué hora terminaba su día de clases y Graciela confirma que salió de clase al caer la tarde. A Gary le preocupa mucho esa inconsistencia.

**¿Qué podría haber ayudado a Annie y Graciela?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**


{{% name "Yvonne" %}} escribió en su BOC que “tres hombres en una camioneta” llegaron al mercado donde trabajaba y la amenazaron. Durante la audiencia, dice que eran “tres o cuatro hombres”. Cuando Gary le pregunta qué vehículo iban conduciendo, contesta: “¿Tal vez un Jeep?” Gary piensa que su historia no es real. Si lo fuese, Yvonne sabría cuántos hombres había y qué tipo de vehículo iban conduciendo.

**¿Qué podría haber ayudado a Yvonne?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% red-flag %}}
Problemas debidos a las notas del oficial
--------------------------------------
{{% /red-flag %}}

{{% name "Mustafa" %}} le contó al oficial que lo entrevistó al llegar a Canadá que en su país iba a menudo a reuniones de un partido político. Según las notas del oficial, Mustafa dijo haber asistido a reuniones “una o dos veces al mes” durante el año pasado. Durante la audiencia, Gary le pregunta a Mustafa a cuántas reuniones había asistido el año pasado. Contesta que “probablemente unas diez, quizá una docena”. Gary piensa que la historia de Mustafa ha cambiado.

**¿Qué podría haber ayudado a Mustafa?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**

Las notas del oficial de IRCC dicen que {{% name "Tanzim" %}} alegó ser “miembro” de un partido político de oposición. Durante la audiencia, Gary le pide pruebas de su membresía. Tanzim explica que no era miembro, solamente simpatizante. Gary le pide a Tanzim que explique por qué el oficial habría escrito que era miembro si solo era simpatizante. Tanzim dice que el oficial debe haber malentendido. Gary piensa que el oficial no habría cometido tal error. Piensa que Tanzim cambió su historia al darse cuenta de que tendría que dar prueba de su membresía en el partido.

**¿Qué podría haber ayudado a Tanzim?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**


{{% red-flag %}}
Problemas debidos a cambios en el testimonio de la persona solicitante
------------------------------------------------------
{{% /red-flag %}}

Al principio de la audiencia de {{% name "Lupe" %}}, Gary le pregunta cuánto tiempo pasó entre la primera y la segunda llamada amenazante que recibió. Ella responde que la segunda llamada entró “un par de semanas” después de la primera. Más adelante en la audiencia, Lupe dice que recibió esa llamada “más o menos un mes” después de la primera. Gary piensa que, como ha cambiado la historia, es probable que Lupe esté mintiendo.

**¿Qué podría haber ayudado a Lupe?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

Gary le pide a {{% name "Samuel" %}} que describa el tipo de cosas que había hecho para apoyar a su partido político. Samuel le cuenta a Gary que había organizado un comité estudiantil en su universidad, escrito un artículo en su periódico universitario y animado a sus compañeros de clase a asistir a mítines. Posteriormente, Gary le pregunta de nuevo a Samuel sobre sus actividades políticas. Esta vez Samuel menciona además que había distribuido panfletos en el campus. Como Samuel no había mencionado antes la distribución de panfletos, Gary piensa que ha cambiado su historia.

La audiencia de solicitud de asilo de {{% name "Kamala" %}} dura dos días. El primer día, Gary le pide que explique por qué no acudió a la policía cuando fue agredida. Kamala explica que sentía mucha vergüenza. Tampoco quería que su familia o sus amistades se enteraran de lo que le había sucedido. El segundo día, Gary le pregunta de nuevo a Kamala por qué no acudió a la policía. Esta vez explica que sentía mucha vergüenza y además que la policía no ayuda a mujeres como ella. Gary siente sospechas. Piensa que entre las dos audiencias alguien le dijo a Kamala que añadiera esa nueva información sobre la policía.

**¿Qué podría haber ayudado a Samuel y Kamala?**

* **{{< link "/12-things-that-have-helped/just-an-example" >}}**


{{% red-flag %}}
Problema porque la persona solicitante no sabe algo que Gary piensa que debería saber
-------------------------------------------------------------------------------------------
{{% /red-flag %}}

{{% name "Jean-René" %}} cuenta que participó en una manifestación grande en la capital de su país hace varios años “en primavera”. Gary le pregunta que exactamente cuándo tuvo lugar la manifestación. Jean-René responde que “sería a finales de abril”. Los informes nacionales de la Comisión muestran que la manifestación tuvo lugar a principios de junio. Gary piensa que esto demuestra que Jean-René está mintiendo sobre su participación en la manifestación.

**¿Qué podría haber ayudado a Jean-René?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Shani" %}} llegó a Canadá en barco. Gary le pregunta si recuerda el nombre del barco. Ella dice que no. Por tanto, Gary sospecha que Shani llegó a Canadá por una vía distinta.

{{% name "Marco" %}} le dice a Gary que estaba parado detrás del mostrador de su tienda cuando llegó una camioneta llena de hombres armados a amenazarlo. Gary le pregunta a Marco sobre la configuración de su tienda. Luego le pide a Marco que describa la camioneta de los hombres. Marco no es capaz de recordar nada sobre la camioneta. A Gary esto le parece muy sospechoso. Desde donde estaba parado Marco, la camioneta debió estar a la vista durante todo el período que los hombres estuvieron en la tienda.

En las semanas previas a su detención, {{% name "Bijan" %}} recibió numerosas visitas de funcionarios del gobierno. Gary le pide a Bijan que describa en detalle la primera vez que los funcionarios fueron a su casa: ¿A qué hora llegaron? ¿Cuánto tiempo se quedaron? ¿Qué preguntas le hicieron? En la mente de Bijan, los detalles de la primera visita se han confundido con los de las muchas visitas posteriores. Cuando intenta describir esa primera visita, acaba por hablar en términos generales sobre el tipo de cosas que los funcionarios dijeron e hicieron en cualquier visita. A Gary le parece sospechoso que Bijan sea incapaz de describir en detalle la primera visita.

{{% name "Asmaan" %}} fue interrogada por soldados en la frontera. Cuando Gary le pide que describa los uniformes de los soldados, no logra recordar claramente. Cree que tal vez los uniformes eran azules. Gary tiene pruebas que muestran que los uniformes eran grises. Él sospecha que nunca fue interrogada en la frontera.

{{% name "Je-Tsun" %}} trabajó durante muchos años en un periódico local. Gary le pide que describa el logotipo del periódico. Je-Tsun describe un escudo con un águila pero no recuerda si la cabeza del águila daba hacia la izquierda o hacia la derecha. Gary piensa que, si ella realmente hubiese visto el logotipo en su trabajo todos los días, se recordaría claramente cómo era.

**¿Qué podría haber ayudado a [these claimants]?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Huan" %}} se convirtió al cristianismo. Escribió en su formulario BOC que lleva varios años yendo a la iglesia regularmente. Gary le hace preguntas a Huan sobre pasajes de la biblia que Huan no es capaz de responder. Gary sospecha que Huan no se ha convertido realmente al cristianismo.

{{% name "Sunny" %}} se integró a un partido político opositor junto a mucha gente joven de su región. Asistía a mítines y distribuía panfletos. Gary le pide a Sunny que explique la filosofía política de su partido. Sunny responde que el partido se oponía a la corrupción y fomentaba los derechos de su minoría étnica. Gary sospecha que Sunny no es realmente miembro del partido opositor. Si hubiese asistido a los mítines con regularidad, conocería mejor la plataforma del partido.

**¿Qué podría haber ayudado a Huan and Sunny?**

* **{{< link "/12-things-that-have-helped/living-their-beliefs" >}}**


Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}