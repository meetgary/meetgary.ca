---
title: A memory is like a video recording
weight: 10

---

{{% lightbulb %}}
**Gary believes that our memories are like video recordings.** When we remember something, we simply push the ‘play’ button and watch the scene again in our heads. Gary believes that memories of upsetting experiences are especially clear and last. He believes that they are burned into our minds.

As a result, Gary cannot see why a truthful person would **describe the same experience differently at two different times**. He cannot understand why a truthful person would be **unable to remember the details of something that happened to them**. He is even more skeptical when the event that they are describing was upsetting.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary is very suspicious when he thinks that **a claimant’s story has changed**. He also distrusts claimants who **misremember or cannot remember** certain kinds of details.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
Trouble because of the BOC narrative
------------------------------------
{{% /red-flag %}}

{{% exclamation-highlight %}}
At the hearing, Gary always has a copy of the claimant’s Basis of Claim (BOC) form in front of him. Also, he often has the notes that the IRCC officer took at the claimant’s interview. But claimants are not allowed to have their BOC form or the officer's notes in front of them at the hearing. **Claimants are not allowed to use any notes to help them to testify.**
<br />
<br />
{{% /exclamation-highlight %}}

In his BOC, {{% name "Boniface" %}} wrote that one of the officers who arrested him hit him in the face, then spat on the ground and insulted him. In the hearing, Boniface said that the officer insulted him first, then spat on the ground, and then hit him in the face. Gary is suspicious of this inconsistency. He thinks that if Boniface's story were true, he would remember clearly the order in which these things happened.

**What might have helped Boniface?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Annie" %}}’s BOC says that her kidnappers held her captive for “a day and a night.” In the hearing, she told Gary that she was kidnapped in the late afternoon and released the next morning. Gary thinks that Annie’s story has changed. He thinks that the kidnapping that she is describing did not last “a day and a night.” Annie does not remember writing “a day and a night” in her BOC. She insists that there must be some mistake, that she never used this phrase. Gary, becoming frustrated, shows her the words in the BOC. He feels that he has caught her in a lie.

Gary has found a serious problem with {{% name "Graciela" %}}'s story. At her hearing, she said that she witnessed a shooting during the morning rush hour. But in her BOC, she wrote that she saw this shooting "on my way home from school." Gary asked what time of day her school ended, and Graciela confirmed that she left school in the late afternoon. Gary is very concerned about this inconsistency.

**What might have helped Annie and Graciela?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**


{{% name "Yvonne" %}} wrote in her BOC that “three men in a van” came to the market where she was working and threatened her. At the hearing, she said that there were “three or four men.” When Gary asked her what they were driving, she answered, “Maybe it was a Jeep?” Gary thinks that this story is not true. If it were, Yvonne would know how many men there were and what kind of vehicle they were driving.

**What might have helped Yvonne?**

* **{{< link "/12-things-that-have-helped/remembering-the-boc" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% red-flag %}}
Trouble because of the officer's notes
--------------------------------------
{{% /red-flag %}}

{{% name "Mustafa" %}} told the officer who interviewed him when he arrived in Canada that in his country he often went to political party meetings. According to the officer’s notes, Mustafa said that, over the last year, he had gone to meetings “once or twice a month.” At the hearing, Gary asked Mustafa how many meetings he had attended in the last year. He answered, “Probably about ten – maybe a dozen.” Gary thinks that Mustafa’s story has changed.

**What might have helped Mustafa?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**
* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**

The IRCC officer’s notes say that {{% name "Tanzim" %}} claimed to be a “member” of an opposition political party. At the hearing, Gary asked for proof of his membership. Tanzim explained that he was not a member, just a supporter. Gary asked Tanzim to explain why the officer would have written that he was a member if he was only a supporter. Tanzim said that the officer must have misunderstood. Gary thinks that the officer would not have made this mistake. He thinks that Tanzim changed his story when he realized that he would need to provide proof of his membership.

**What might have helped Tanzim?**

* **{{< link "/12-things-that-have-helped/knowing-the-notes" >}}**


{{% red-flag %}}
Trouble because of changes in the claimant’s testimony
------------------------------------------------------
{{% /red-flag %}}

Early in {{% name "Lupe" %}}’s hearing, Gary asked her how much time passed between the first and the second threatening phone calls she received. She answered that the second call came “a couple of weeks” after the first. Later in the hearing, Lupe said that she received this second call “about a month” after the first one. Gary thinks that, because her story is changing, Lupe is probably lying.

**What might have helped Lupe?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

Gary asked {{% name "Samuel" %}} to describe the kinds of things that he had done to help support his political party. Samuel told Gary he had organized a student chapter at his university, written an article in his university’s newspaper, and encouraged his classmates to attend rallies. Later on, Gary asked Samuel again about his political activities. This time Samuel also mentioned that he had handed out flyers on campus. Since Samuel did not mention handing out pamphlets earlier, Gary thinks that his story has changed.

{{% name "Kamala" %}}’s refugee hearing stretched over two days. On the first day, Gary asked her to explain why she did not go to the police after she was assaulted. Kamala explained that she was too ashamed. Also, she did not want her family or friends to know what had happened to her. On the second day, Gary again asked Kamala why she did not go to the police. This time she explained that she was too ashamed, and also that the police do not help women like her. Gary is suspicious. He thinks that between the two hearings, someone told Kamala to add this new information about the police.

**What might have helped Samuel and Kamala?**

* **{{< link "/12-things-that-have-helped/just-an-example" >}}**


{{% red-flag %}}
Trouble because the claimant does not know something that Gary thinks that they should know
-------------------------------------------------------------------------------------------
{{% /red-flag %}}

{{% name "Jean-René" %}} said that he took part in a big demonstration in his country’s capital city “in the spring” several years earlier. Gary asked him when exactly the demonstration took place. Jean-René answered that “it would have been at the end of April.” The Board’s country reports show that this demonstration in fact happened at the beginning of June. Gary thinks that this shows that Jean-René is lying about having taken part in the demonstration.

**What might have helped Jean-René?**

* **{{< link "/12-things-that-have-helped/just-a-guess" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Shani" %}} came to Canada on a ship. Gary asked her if she could remember the name of the ship. She could not. As a result, Gary suspects that Shani used a different route to come to Canada.

{{% name "Marco" %}} told Gary that he had been standing behind the counter in his shop when a truck full of armed men arrived to threaten him. Gary asked Marco about the lay-out of his shop. He then asked Marco to describe the men’s truck. Marco could not remember anything about the truck. Gary thinks that this is very suspicious. From where Marco was standing, the truck would have been in view the whole time that the men were in the store.

In the weeks leading up to his arrest, {{% name "Bijan" %}} was visited repeatedly by government officials. Gary asked Bijan to describe in detail the first time that the officials came to his home: What time of day did they arrive? How long did they stay? What questions did they ask? In Bijan’s mind, the details of that first visit have become blurred together with the details of the many visits that followed. When he tries to describe that first visit, he ends up talking in general terms about the kinds of things that the officials said and did whenever they would come by. Gary finds Bijan’s inability to describe the first visit in detail suspicious.

{{% name "Asmaan" %}} was questioned by soldiers at the border. When Gary asks her to describe the soldiers’ uniforms, she cannot remember them clearly. She thinks that their uniforms may have been blue. Gary has evidence that shows the uniforms would have been grey. He suspects that she was never questioned at the border.

{{% name "Je-Tsun" %}} worked for many years at a local newspaper. Gary asked her to describe the newspaper’s logo. Je-Tsun described a crest with an eagle on it, she but could not remember whether the eagle’s head was facing to the left or to the right. Gary thinks that if she had really seen this logo every day at work, she would remember clearly what it looked like.

**What might have helped these claimants?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Huan" %}} converted to Christianity. He wrote in his BOC form that he has been going to church regularly for several years. Gary asked Huan questions about Bible passages that Huan could not answer. Gary suspects that Huan has not really converted to Christianity.

{{% name "Sunny" %}} joined an opposition political party along with many of the young people from his region. He attended rallies and handed out pamphlets. Gary asked Sunny to explain his party’s political philosophy. Sunny answered that the party was opposed to corruption and that it supported the rights of his ethnic minority. Gary suspects that Sunny is not really an opposition party member. If he attended rallies regularly, he would have a more detailed understanding of the party’s platform.

**What might have helped Huan and Sunny?**

* **{{< link "/12-things-that-have-helped/living-their-beliefs" >}}**


Read about Gary's other Big Ideas:

{{< siblings >}}