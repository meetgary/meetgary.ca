---
title: Gary's Six Big Ideas
menuTitle: 6 Big Ideas
weight: 20

---

{{< lightbulb >}}
Click on the links below to learn about **Gary's Six Big Ideas**. You will learn how Gary sees the world and why these Big Ideas make him suspicious of claimants' stories.
{{< /lightbulb >}}

### Click a link below to learn about one of Gary's Big Ideas

{{< children >}}

