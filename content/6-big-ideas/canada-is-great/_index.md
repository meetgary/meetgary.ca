---
title: Canada is a safe, welcoming, and generous country
linkTitle: Canada is the best!
weight: 60

---

{{% lightbulb %}}
Gary loves Canada very much. He is very proud of his country’s reputation as a **safe, generous and welcoming country**. And he thinks that, given the chance, most people in the world would rather live here than in their own country. As a result, Gary is always concerned that a claimant may have come to Canada **simply because they want to live here**, rather than because they are in danger at home. He also has a hard time imagining **why a person would not feel safe and at ease in Canada**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary may be suspicious when he thinks that a claimant **just wants to live in Canada rather than in their own country**. At the same time, he is also suspicious when or when they say that they **do not feel safe and at ease in Canada**.
{{% /gary-suspicious-highlight %}}


{{% red-flag%}}
Preferring to live in Canada
----------------------------
{{% /red-flag%}}

At the beginning of the hearing, Gary wanted {{% name "Zoya" %}} to explain to him why she had fled her country. He asked her: “Why did you come to Canada?” Zoya answered that Canada was a beautiful and friendly country that offers her many opportunities. Now Gary suspects that Zoya has come to Canada simply because she would prefer to live here.

**What might have helped Zoya?**

* [**Explaining how they understood the questions**](/12-things-that-have-helped/the-instructions)

{{% name "Marjorie" %}}’s father wrote a detailed letter to the Board describing the danger that his daughter was facing. In the final paragraph, her father also explained that his daughter would have a much better chance at a happy and prosperous life in Canada. He implored the member to allow her to stay. This letter left Gary with the impression that Marjorie has other reasons for wanting to stay in Canada. This has made him suspicious.

**What might have helped Marjorie?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**


{{% red-flag%}}
Not feeling safe or at ease in Canada
-------------------------------------
{{% /red-flag%}}

{{% name "Boris" %}} lived through many years of persecution as a gay man. At his hearing, he explained that when he arrived in Canada, his priorities were finding an apartment and finding a job. After that, he needed time to adjust. Over the last year, he has spent most of his free time by himself. He has not shared his sexual orientation with his coworkers and has not yet tried to meet other gay men. Gary does not believe that Boris is gay. If he was, he would surely by now have taken advantage of his city’s vibrant gay community.

When {{% name "Marjani" %}} made her refugee claim, she told the officer that she had been arrested, held for three days, and interrogated by her country’s secret police. She did not tell the officer that her interrogators had sexually assaulted her. The man who was interpreting at her interview was from her country and Marjani was afraid that he might share this information with her community. But the interpreter had sworn to keep Marjani's information confidential. Gary thinks that Marjani had no reason to distrust this Canadian professional.

{{% name "Barrington" %}} made his refugee claim when he was arrested and detained by the immigration authorities for overstaying his work visa. When Gary asked him why he had not made a claim sooner, Barrington said that he felt there was no point because Canada is a racist country that would never help him. Gary finds this answer unbelievable. He cannot imagine how anyone could think of Canada this way.

**What might have helped Boris, Marjani and Barrington?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**


Read about Gary's other Big Ideas:

{{< siblings >}}	