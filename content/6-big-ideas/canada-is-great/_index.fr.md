---
title: Le Canada est un pays sécuritaire, accueillant et généreux
linkTitle: Le Canada, c’est le meilleur!
weight: 60

---

{{% lightbulb %}}
Gary aime beaucoup le Canada. Il est très fier de la réputation de son pays, **sécuritaire, généreux et accueillant**. Et il pense que, s’ils en avaient la chance, la plupart des gens voudraient vivre dans son pays plutôt que dans leur pays d’origine. Ainsi, Gary se demande toujours si les demandeurs sont venus au Canada **tout simplement parce qu’ils veulent vivre ici**, plutôt que parce qu’ils sont en danger chez eux. Il a aussi du mal à s’imaginer **qu’une personne peut ne pas se sentir en sécurité et à son aise au Canada**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary peut avoir des doutes s’il pense qu’un demandeur **veut simplement s’installer au Canada plutôt que dans son pays d’origine**. En même temps, il trouve louche si le demandeur dit qu’il **ne se sent pas en sécurité et à l’aise au Canada**.
{{% /gary-suspicious-highlight %}}


{{% red-flag%}}
Préférer vivre au Canada
----------------------------
{{% /red-flag%}}

Au début de l’audience, Gary a invité {{% name "Zoya" %}} à lui expliquer pourquoi elle avait quitté son pays. Il lui a demandé : « Pourquoi êtes-vous venue au Canada? » Zoya lui a dit que le Canada est un pays magnifique qui lui offre beaucoup de possibilités. Maintenant, Gary se demande si Zoya est venue au Canada simplement parce qu’elle préfère vivre ici.

**Qu’est-ce qui aurait aidé Zoya?**

* [**Expliquer sa compréhension de la question**](/12-things-that-have-helped/the-instructions)

Le père de {{% name "Marjorie" %}} a écrit une lettre détaillée à la Commission dans laquelle il décrit les dangers qui menaçaient sa fille. Dans le dernier paragraphe, il explique aussi que sa fille aurait beaucoup plus de chance d’avoir une vie heureuse et prospère au Canada. Il implore le commissaire de lui permettre de rester au Canada. En raison de cette lettre, Gary a l’impression que Marjorie a d’autres raisons de vouloir rester au Canada. Il a des doutes.

**Qu’est-ce qui aurait aidé Marjorie?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**


{{% red-flag%}}
Ne pas se sentir en sécurité ou à l’aise au Canada
-------------------------------------
{{% /red-flag%}}

{{% name "Boris" %}} a subi de nombreuses années de persécution parce qu’il est gay. Pendant l’audience, il a expliqué qu’à son arrivée au Canada, il avait pour priorités de trouver un appartement et de trouver un emploi. Après ça, il a eu besoin de temps pour s’habituer. Au cours de la dernière année, il a passé tous ses temps libres seul. Il n’a pas divulgué son orientation sexuelle à ses collègues au travail et il n’a pas essayé de rencontrer d’autres hommes gays. Gary ne croit pas que Boris est gay. S’il l’était vraiment, il se serait joint à la communauté gaie de sa ville. 

Quand {{% name "Marjani" %}} a fait sa demande d’asile, elle a dit à l’agent qu’elle avait été arrêtée, détenue pendant trois jours, puis interrogée par la police secrète de son pays. Elle n’a pas dit à l’agent que les interrogateurs l’avaient agressée sexuellement. L’homme qui agissait à titre d’interprète était aussi originaire du même pays, Marjani avait peur qu’il divulgue cette information à sa communauté. Mais l’interprète avait prêté serment de ne jamais divulguer les informations confidentielles de Marjani. Gary pense qu’elle n’avait aucune raison de ne pas faire confiance à ce professionnel canadien. 

{{% name "Barrington" %}} a fait sa demande d’asile quand il a été arrêté et détenu par les autorités d’immigration parce qu’il est resté au pays après la durée autorisée de son visa de travail. Gary lui a demandé pourquoi il n’avait pas fait sa demande d’asile plus tôt, et Barrington a répondu qu’il pensait que ça ne servait à rien parce que le Canada est un pays raciste qui ne lui viendrait jamais en aide. D’après Gary, cela est inconcevable. Il ne peut imaginer qu’on puisse penser cela du Canada.

**Qu’est-ce qui aurait aidé Boris, Marjani et Barrington?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**


Découvrir les autres Grandes Idées de Gary

{{< siblings >}}