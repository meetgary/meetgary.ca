---
title: Canadá es un país seguro, acogedor y generoso
linkTitle: ¡Canadá es lo mejor!
weight: 60

---

{{% lightbulb %}}
Gary siente un gran amor por Canadá. Está muy orgulloso de la reputación que tiene su país de ser un **país seguro, acogedor y generoso**. Y piensa que, de tener la oportunidad, la mayor parte del mundo preferiría vivir aquí que en su propio país. Por tanto, Gary siempre se inquieta al pensar que una persona solicitante haya venido a Canadá **simplemente porque desea vivir aquí**, y no porque corre peligro en su país. También le cuesta imaginar **por qué una persona no se sentiría a salvo y en paz en Canadá**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary puede sospechar si piensa que una persona solicitante **simplemente desea vivir en Canadá en vez de en su país**. Al mismo tiempo, también sospecha cuando una persona dice que **no se siente a salvo y en paz en Canadá**.
{{% /gary-suspicious-highlight %}}


{{% red-flag%}}
Preferir vivir en Canadá
----------------------------
{{% /red-flag%}}

Al principio de la audiencia, Gary quiere que {{% name "Zoya" %}} le explique por qué ella huyó de su país. Le pregunta: “¿Por qué viniste a Canadá?” Zoya responde que Canadá es un país hermoso y acogedor que le brinda muchas oportunidades. Ahora Gary sospecha que Zoya ha venido a Canadá simplemente porque preferiría vivir aquí.

**¿Qué podría haber ayudado a Zoya?**

* [**Explicar cómo entendieron las preguntas**](/es/12-things-that-have-helped/the-instructions)

El padre de {{% name "Marjorie" %}} escribió una carta detallada a la Comisión describiendo el peligro que corría su hija. En el último párrafo, su padre explicó asimismo que su hija tendría mejores oportunidades de tener una vida feliz y próspera en Canadá. Le suplicó al miembro de la Comisión que le permita quedarse. La carta le da a Gary la impresión de que Marjorie tiene otros motivos para querer quedarse en Canadá. Esto lo hace sospechar.

**¿Qué podría haber ayudado a Marjorie?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**


{{% red-flag%}}
No sentirse a salvo o en paz en Canadá
-------------------------------------
{{% /red-flag%}}

{{% name "Boris" %}} sufrió muchos años de persecución por ser gay. En su audiencia, explica que al llegar a Canadá sus prioridades eran encontrar un apartamento y encontrar empleo. Después, necesitó tiempo para adaptarse. Durante el año pasado, ha estado solo la mayor parte de su tiempo libre. No ha revelado su orientación sexual a sus colegas y no ha buscado conocer a otros hombres gay. Gary no cree que Boris sea gay. Si lo fuera, sin duda ya habría buscado aprovechar la vibrante comunidad gay de su ciudad.

Cuando {{% name "Marjani" %}} presentó su solicitud de asilo, le dijo al oficial que la habían arrestado, que permaneció detenida durante tres días y que fue interrogada por la policía secreta de su país. No informó al oficial que sus interrogadores la habían agredido sexualmente. El hombre que sirvió de intérprete durante su entrevista era de su país y Marjani tenía miedo de que él divulgara dicha información en su comunidad. Pero el intérprete había jurado mantener el carácter confidencial de la información de Marjani. Gary piensa que Marjani no tenía motivo para desconfiar de ese profesional canadiense.

{{% name "Barrington" %}} presentó su solicitud de asilo cuando fue arrestado y detenido por las autoridades de inmigración por permanecer en Canadá tras vencerse su visado de trabajo. Cuando Gary le pregunta que por qué no había solicitado antes, Barrington dice que sentía que no tenía sentido hacerlo pues Canadá es un país racista que nunca lo ayudaría. A Gary le cuesta creer esa respuesta. No se imagina cómo alguien podría tener esa opinión de Canadá.

**¿Qué podría haber ayudado a Boris, Marjani y Barrington?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**


Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}