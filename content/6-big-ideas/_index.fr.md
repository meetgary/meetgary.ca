---
title: Les six grandes idées de Gary
menuTitle: Six Grandes Idées
weight: 20

---

{{< lightbulb >}}
Veuillez consulter les liens ci-dessous pour découvrir **les six grandes idées de Gary**. Vous découvrirez sa façon de voir le monde, et pourquoi il lui arrive de mettre en doute les histoires des demandeurs d'asile. 
{{< /lightbulb >}}

### Veuillez consulter un des liens ci-dessous pour en apprendre davantage sur l'une des six grandes idées de Gary. 

{{< children >}}

