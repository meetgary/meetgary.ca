---
title: Once a liar, always a liar
linkTitle: Once a liar, always a liar
weight: 30

---

{{% lightbulb %}}
Gary believes that there are two kinds of people: honest people and dishonest people. A person who has tried to deceive him is a dishonest person. There is no reason for him to believe anything that a dishonest person says.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
If Gary thinks that a claimant **has tried to deceive him**, he will conclude that **their whole story is a lie**.
{{% /gary-suspicious-highlight %}}

{{% name "Ana" %}} never asked the police for help when her husband was hurting her. She knew that they would not interfere in a “family matter.” When she was writing her BOC narrative, friends gave her advice. They said that her claim would be much stronger if she said that she had asked the police for help. Ana added this to her story. Gary has heard many claims like Ana’s, and he understands that the police would not have helped her. Gary suspects that Ana made up this part of her story. As a result, he now he suspects that her whole story is a lie.

{{% name "Tariq" %}} was attacked by supporters of a rival political party. He has heard that the Board wants to see proof of his story. A man he knows creates fake newspaper articles for refugee claimants from his community. Tariq bought an article describing the attack and gave it to the Board. The article looked convincing to Tariq, but the investigators at the Board discovered that it was a fake. Now Gary thinks that Tariq’s whole story is a lie.

{{% name "Monique" %}} paid smugglers to bring her to Canada. The smugglers gave her a made-up story to tell about the route that she took. Monique wrote this story in her BOC and told it at her hearing. Gary asked Monique many questions about her trip to Canada. He asked her how long different parts of the trip lasted, and the names of the towns that she passed through. When Monique could not answer these questions, Gary decided that she was lying about how she came to Canada. Now he feels that he cannot believe anything she tells him.

**What might have helped Ana, Tariq and Monique?**

* **{{< link "/12-things-that-have-helped/admitting-a-lie" >}}**


Read about Gary's other Big Ideas:

{{< siblings >}}