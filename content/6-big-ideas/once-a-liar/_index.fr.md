---
title: Menteur un jour, menteur toujours
linkTitle: Menteur un jour, menteur toujours
weight: 30

---

{{% lightbulb %}}
Gary croit qu’il y a deux types de personnes : les personnes honnêtes et les personnes malhonnêtes. Une personne qui a essayé de lui mentir est malhonnête. Il n’y a donc aucune raison pour lui de faire confiance à ce que dit une personne malhonnête. 
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Si Gary pense qu’un demandeur **a essayé de lui mentir**, il va conclure que **toute son histoire est un mensonge**.
{{% /gary-suspicious-highlight %}}

{{% name "Ana" %}} n’a jamais demandé l’aide de la police quand son mari la frappait. Elle savait qu’ils n’interviendraient pas « dans des affaires de famille ». Au moment d’écrire son formulaire FDA, ses amis lui ont donné des conseils. Ils ont dit que sa demande serait plus convaincante si elle disait qu’elle avait contacté la police. Ana l’a ajouté à son histoire. Gary a vu beaucoup de demandes, et il comprend que la police ne l’aurait pas aidée. Gary se demande si elle a inventé cette partie de son histoire. Et en plus, il se demande si toute son histoire est un mensonge.

{{% name "Tariq" %}} a été attaqué par les partisans d’un parti politique rival. Il a entendu dire que la Commission veut des preuves qui appuient son histoire. Comme il connaît une personne qui crée de faux articles de journaux pour les demandeurs d’asile de sa communauté, il a acheté un article décrivant l’attaque et l’a donné à la Commission. L’article semblait convaincant, mais les enquêteurs ont découvert qu’il était faux. Maintenant, Gary croit que toute l’histoire de Tariq est un mensonge.

{{% name "Monique" %}} a payé des contrebandiers qui l’ont amenée jusqu’au Canada. Les contrebandiers lui ont fourni une histoire fictive pour décrire le voyage qu’elle a fait. Monique a écrit cette histoire sur le formulaire FDA et elle l’a raconté à l’audience. Gary lui a posé beaucoup de questions sur son voyage au Canada. Il lui a demandé la durée des différentes parties du trajet, et les noms des villages qu’elle a traversés. Comme Monique n’a pu répondre à ces questions, Gary a décidé qu’elle mentait au sujet du voyage qu’elle a fait pour se rendre au Canada. Maintenant, il n’a plus confiance en ce qu’elle dit. 

**Qu’est-ce qui aurait aidé Ana, Tariq et Monique?**

* **{{< link "/12-things-that-have-helped/admitting-a-lie" >}}**


Découvrir les autres Grandes Idées de Gary 

{{< siblings >}}