---
title: یکبار یک دروغگو, همیشه یک دروغگ
linkTitle: یکبار یک دروغگو, همیشه یک دروغگو
weight: 30

---

{{% lightbulb %}}
گری معتقد است که دو نوع آدم وجود دارد: انسانهای صادق و افراد نادرست. شخصی که سعی در فریب دادن او داشته است ، فرد نادرست است. هیچ دلیلی وجود ندارد که او به هر چیزی که یک شخص نادرست می گوید ایمان داشته باشد.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
اگر گری فکر کند که یک متقاضی **سعی کرده تا اورا فریب دهد**, نتیجه میگیرد که **تمام داستان آنها دروغ است**.
{{% /gary-suspicious-highlight %}}

{{% name "آنا" %}} زمانیکه شوهرش او را آزار میداد هرگز از پلیس تقاضای کمک نکرد.  او می دانست که آنها در "مسئله خانوادگی" دخالت نمی کنند.  زمانیکه داستان خود را در فرم مهاجرت مینوشت, دوستانش به او پیشنهاد کردند که تقاضای او بسیار قوی تر خواهد بود اگر اعلام کند که از پلیس هم درخواست کمک داشته است. آنا این را هم به داستانش اضافه نمود.. گری ادعاهای زیادی مانند آنا شنیده است ، و می فهمد که پلیس به او کمک نمی کرد. گری مشکوک است که آنا این بخش از داستان خود را از خودش ساخته است. در نتیجه ، او اکنون گمان می کند که کل داستان او دروغ است.

{{% name "طارق" %}} توسط هواداران یک حزب سیاسی رقیب مورد حمله قرار گرفت. او شنیده است که هیئت رسیدگی می خواهد دلیل اثبات داستان او را ببیند. مردی که او می شناسد مقالات روزنامه جعلی را برای متقاضیان پناهندگی از جامعه خود ایجاد می کند. طارق مقاله ای را برای توصیف این حمله خریداری کرد و به هیئت رسیدگی ارائه کرد. این مقاله برای طارق قانع کننده به نظر می رسید ، اما محققان هیئت کشف کردند که جعلی است. حالا گری فکر می کند که کل داستان طارق دروغ است.

{{% name "مونيك" %}} به قاچاقچیان پول پرداخت کرده تا او را به کانادا بیاورند. قاچاقچیان داستانی را برایش سر هم کردند تا از مسیری که طی کرده بود بگوید. مونیک این داستان را در فرم مهاجرت خود نوشت و آن را در مصاحبه خود گفت. گری در مورد سفرش به کانادا از مونیک سؤالهای زیادی کرد. گری از او پرسید که چه مدت بخش های مختلف سفر به طول انجامیده و نام شهرهایی که از آنها گذشت چه بوده است. هنگامی که مونیک نتوانست به این سؤالات پاسخ دهد ، گری نتیجه گرفت که او درباره نحوه ورود به کانادا دروغ می گوید. اکنون گری احساس می کند که نمی تواند هر چیزی را که به او می گوید باور کند.

**چه چیزی ممکن است به آنا ، طارق و مونیک کمک کرده باشد؟**

* **{{< link "/12-things-that-have-helped/admitting-a-lie" >}}**


درباره ایدهای بزرگ گری مطالعه کنید:

{{< siblings >}}