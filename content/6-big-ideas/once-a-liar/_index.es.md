---
title: Quien miente, siempre mentirá
linkTitle: Quien miente, siempre mentirá
weight: 30

---

{{% lightbulb %}}
Gary cree que hay dos tipos de personas: honestas y deshonestas. Una persona que ha intentado engañarlo es una persona deshonesta. No tiene motivo alguno para creer nada de lo que diga una persona deshonesta.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Si Gary piensa que una persona solicitante **ha intentado engañarlo**, concluirá que **toda su historia es mentira**.
{{% /gary-suspicious-highlight %}}

{{% name "Ana" %}} nunca acudió a la policía buscando ayuda cuando su esposo la lastimaba. Sabía que no interferirían en un “asunto familiar”. Cuando estaba redactando su narración de BOC, sus amigos la orientaron. Dijeron que su solicitud tendría más peso si decía que había acudido a la policía buscando ayuda. Ana añadió eso a su historia. Gary ha evaluado muchas solicitudes como la de Ana y sabe que la policía no la habría ayudado. Gary sospecha que Ana inventó esta parte de la historia. Por tanto, ahora sospecha que toda su historia es mentira.

{{% name "Tariq" %}} fue atacado por simpatizantes de un partido político rival. Le han dicho que la Comisión quiere ver pruebas de su historia. Un conocido elabora artículos de periódico falsos para solicitantes de asilo en su comunidad. Tariq compró un artículo que describía el ataque y lo entregó a la Comisión. A Tariq el artículo le pareció convincente pero investigadores de la Comisión descubrieron que era falso. Ahora Gary piensa que toda la historia de Tariq es mentira.

{{% name "Monique" %}} le pagó a traficantes para que la trajeran a Canadá. Ellos le dieron una historia inventada sobre la ruta que siguió. Monique escribió esa historia en su BOC y la contó en su audiencia. Gary le hizo muchas preguntas a Monique acerca de su viaje a Canadá. Preguntó por la duración de distintas partes del viaje y los nombres de los pueblos por los que pasó. Cuando Monique no supo responder dichas preguntas, Gary decidió que estaba mintiendo sobre cómo llegó a Canadá. Ahora siente que no puede creer nada de lo que ella le dice.

**¿Qué podría haber ayudado a Ana, Tariq y Monique?**

* **{{< link "/12-things-that-have-helped/admitting-a-lie" >}}**


Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}