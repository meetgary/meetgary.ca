---
title: Coincidir a la perfección
weight: 40

---

{{% lightbulb %}}
Gary espera que cada persona solicitante intente obtener evidencias para ayudar a probar su historia. Espera que, de ser posible, envíen a la Comisión documentos tales como informes policiales, informes médicos y declaraciones de testigos. Gary cree que puede aprender mucho acerca de la credibilidad de una persona solicitante examinando **hasta qué punto la información contenida en tales documentos de apoyo coincide con la historia de la persona solicitante**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary tiene sospechas cuando piensa que algo contenido en un documento **contradice** lo que la persona solicitante ha dicho. También sospecha si piensa que en el documento **falta** información importante.
{{% /gary-suspicious-highlight %}}

{{% name "Gordon" %}} presentó un informe de un psiquiatra canadiense. El psiquiatra indica que Gordon empezó a descubrir su atracción por el mismo sexo “a principios de su adolescencia”. Durante su audiencia, Gordon explica que por primera vez le gustó alguien del mismo sexo cuando tenía “16 o 17 años”. Debido a esta insconsistencia, Gary sospecha que Gordon está mintiendo sobre su orientación sexual.

{{% name "Ndidi" %}} hizo una denuncia policial en su país. El policía que la entrevistó escribió en su informe sus agresores “amenazaron con regresar”. En su BOC, Ndidi escribió que sus agresores “dijeron que regresarían a eliminarme”. Ndidi no supo explicar por qué el policía no mencionó la amenaza de muerte en su informe. Gary sospecha que Ndidi ha añadido esa amenaza para intentar dar más peso a su historia.

En su BOC, {{% name "Lijuan" %}} escribió que le fracturaron un brazo durante un ataque. También escribió que el mismo ataque la dejó con una herida en el labio y sangrando por la nariz. Lijuan presentó a la Comisión un informe médico emitido por un hospital en su país. Dicho informe muestra que fue admitida y recibió tratamiento por “un brazo fracturado”. Gary tiene sospechas porque el informe no menciona ninguna herida en la cara.

La madre de {{% name "Badma" %}} escribió una carta para que ella la presentara a la Comisión. Dicha carta describe algunos de los acosos y las amenazas que Badma sufrió de parte de su antiguo profesor. Pero Badma mantuvo le ocultó a su madre la peor parte de lo que sufrió. Por tanto, la carta solamente cuenta una pequeña parte de la historia. Al leer la carta, Gary sospecha que la madre de Badma ha descrito todos los problemas de Badma. Gary sospecha que Badma ha inventado las otras experiencias en un intento de dar mayor peso a su solicitud de asilo.

**¿Qué podría haber ayudado a Gordon, Ndidi, Lijuan y Badma?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**

Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}