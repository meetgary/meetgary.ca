---
title: La cohérence parfaite
weight: 40

---

{{% lightbulb %}}
Gary s’attend à ce que les demandeurs d’asile obtiennent des preuves pour appuyer leur histoire. Quand ils le peuvent, Gary s’attend à ce qu’ils envoient à la Commission des documents comme des rapports de police, des rapports médicaux et des témoignages. Gary croit qu’il peut en apprendre beaucoup sur la crédibilité d’un demandeur en vérifiant **à quel point l’histoire du demandeur est cohérente avec les renseignements des documents de preuve**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary a des doutes quand il a l’impression que les informations d’un document **contredisent** ce que dit le demandeur. Il a aussi des doutes s’il semble **manquer** des renseignements importants dans le document.
{{% /gary-suspicious-highlight %}}

{{% name "Gordon" %}} a soumis le rapport d’une psychiatre canadienne. La psychiatre écrit que Gordon a commencé à découvrir qu’il était attiré par les personnes du même sexe « au début de l’adolescence ». À l’audience, Gordon a expliqué qu’il a eu son premier béguin pour une personne du même sexe quand il avait « 16 ou 17 ans ». En raison de cette incohérence, Gary a l’impression que Gordon ment au sujet de son orientation sexuelle.

{{% name "Ndidi" %}} a porté plainte auprès de la police de son pays. L’agent policier qui a interrogé Ndidi a écrit dans son rapport que ses assaillants « l’avaient menacée de revenir ». Dans son formulaire FDA, Ndidi a écrit que ses assaillants « ont dit qu’ils reviendraient et qu’ils me finiraient ». Ndidi ne pouvait expliquer pourquoi l’agent n’avait pas mentionné la menace de mort dans son rapport. Gary se demande si Ndidi a ajouté cette menace parce qu’elle essaie de renforcer son histoire. 

Dans son formulaire FDA, {{% name "Lijuan" %}} a écrit que son bras a été fracturé lors d’une attaque. Elle a aussi écrit que dans cette attaque, elle a eu la lèvre coupée et que son nez saignait. Lijuan a soumis le rapport médical de l’hôpital de son pays auprès de la Commission. Le rapport montre qu’elle a été admise et soignée pour « une fracture au bras ». Gary trouve louche que le rapport ne mentionne pas les autres blessures au visage.

La mère de {{% name "Badma" %}} a écrit une lettre à l’intention de la Commission. Cette lettre décrit le harcèlement et les menaces dont Badma a fait l’objet de la part de son ancien professeur. Badma n’a pas raconté le pire à sa mère. C’est pourquoi la lettre ne décrit qu’une partie de ce qui est arrivé. À la lecture de la lettre, Gary a l’impression que la mère de Badma raconte tous les problèmes de Badma. Gary se demande si Badma a inventé les autres parties de l’histoire pour donner plus de poids à son histoire. 

**Qu’est-ce qui aurait aidé Gordon, Ndidi, Lijuan et Badma?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**

Découvrir les autres Grandes Idées de Gary

{{< siblings >}}
