---
title: A perfect match
weight: 40

---

{{% lightbulb %}}
Gary expects that claimants will try to get evidence to help prove their story. Whenever they can, he expects them to send to the Board documents like police reports, medical reports, and statements from witnesses. Gary believes that he can learn a lot about a claimant’s credibility by looking at **how closely the information in these kinds of supporting documents matches the claimant’s story**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary is suspicious when he thinks that something in a document **contradicts** what the claimant has said. He is also suspicious if he thinks that the document **is missing** some important information.
{{% /gary-suspicious-highlight %}}

{{% name "Gordon" %}} submitted a report from a Canadian psychiatrist. The psychiatrist writes that Gordon began to discover his feelings of same-sex attraction “in early adolescence.” In his hearing, Gordon explained that he had his first same-sex crush when he was “16 or 17 years old.” Because of this inconsistency, Gary suspects that Gordon is lying about his sexual orientation.

{{% name "Ndidi" %}} made a police report in her country. The police officer who interviewed Ndidi wrote in the report that her attackers “threatened to return.” In her BOC, Ndidi wrote that her attackers “said that they would come back and finish me.” Ndidi could not explain why the police officer did not mention the death threat in the report. Gary suspects that Ndidi has added this threat because she is trying to make her story stronger.

In her BOC, {{% name "Lijuan" %}} wrote that her arm was broken in an attack. She also wrote that this attack left her with a cut lip and a bleeding nose. Lijuan gave the Board a medical report from a hospital in her country. The report shows that she was admitted and treated for “a broken arm.” Gary is suspicious because the report does not mention any injuries to her face.

{{% name "Badma" %}}’s mother wrote a letter for her to give to the Board. This letter describes some of the harassment and threats that Badma experienced from her former teacher. But Badma kept the worst of what she suffered a secret from her mother. As a result, the letter only tells a small part of the story. In reading this letter, Gary suspects that Badma’s mother has described all of Badma’s troubles. Gary suspects that Badma has invented her other experiences to try to make her refugee claim stronger.

**What might have helped Gordon, Ndidi, Lijuan and Badma?**

* **{{< link "/12-things-that-have-helped/reading-the-evidence" >}}**

Read about Gary's other Big Ideas:

{{< siblings >}}
