---
title: Las personas tienen precaución y se preocupan ante todo por su propia seguridad
linkTitle: La seguridad a cualquier precio
weight: 20

---

{{% lightbulb %}}
A Gary le parece obvio que toda persona cuya vida corre peligro sienta mucho miedo, que se mantenga alerta a cualquier señal de peligro. Desde el momento en que se ve amenazada, esa persona hará todo lo posible por garantizar su seguridad y la de su familia. Nunca tomará riesgos innecesarios. Gary piensa que para las personas razonables su seguridad siempre será la prioridad más alta, incluso cuando tengan otras preocupaciones e inquietudes.

Por tanto, a Gary le cuesta creer que una persona pueda ser **valiente o imprudente u optimista o autocomplaciente o bien agobiarse** frente al peligro. También le cuesta creer que una persona pueda **valorar algo más que su propia seguridad**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
A menudo, Gary sospecha mucho cuando una persona solicitante **toma un riesgo que Gary considera insensato**. También puede sospechar cuando otras personas que aparecen en su historia se han arriesgado.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
No salir de su país antes
-----------------------
{{% /red-flag %}}

La guerrilla le advirtió a {{% name "Sonia" %}} muchas veces que saliera del país o sufriría las consecuencias. No fue hasta que intentaron asesinarla que decidió que era momento de salir. A Gary le cuesta creerlo. Piensa que si Sonia realmente se sentía amenazada se habría ido del país de inmediato.

**¿Qué podría haber ayudado a Sonia?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Manuel" %}} y su familia decidieron salir del país cuando los paramilitares de la zona intentaron reclutar a su hijo de edad adolescente. La esposa de Manuel y sus hijos partieron al día siguiente, pero Manuel se quedó unas semanas más para vender la casa de la familia y cerrar sus asuntos. Gary sospecha que Manuel realmente no temía por su seguridad. De haber temido de verdad, Manuel se habría ido del país con su familia.

El hijo de un político de peso acosó y hostigó a {{% name "Purabi" %}} durante meses. Varias semanas antes de que terminara su último año de universidad, le hizo una amenaza seria: si no accedía a casarse con él, la quemaría con ácido. Purabi decidió esconderse y dejó de ir a la universidad. Pero pidió prestadas las notas de una compañera y siguió estudiando. Volvió a la universidad a presentar su examen final. Una semana más tarde, se fue a Canadá. Gary sospecha que la historia de Purabi no es real. De serlo, sin duda ella se habría ido del país de inmediato y no se habría quedado a terminar sus estudios.


**¿Qué podría haber ayudado a Manuel y Purabi?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
No solicitar asilo de inmediato
-------------------------------------
{{% /red-flag %}}

{{% name "Ester" %}} cruzó la frontera a los Estados Unidos desde México. Evadió las autoridades de EE.UU. y llegó hasta Canadá. Gary piensa que si de verdad hubiese estado en peligro en su país habría solicitado asilo en los EE.UU., el primer país seguro donde logró entrar. Él sospecha que simplemente desea vivir en Canadá.

{{% name "Mukisa" %}} estaba estudiando en una universidad canadiense cuando su familia en su país se enteró que era gay. Por eso ya no era seguro para él regresar. Mukisa empezó a investigar qué vías jurídicas tenía. Se reunió con un abogado, se informó sobre el proceso de solicitud de asilo y empezó a recoger pruebas. Antes de que se venciera su visado de estudiante, presentó una solicitud de asilo. Gary piensa que si Mukisa de verdad tuviera miedo de regresar a su país habría presentado su solicitud mucho antes. Gary sospecha que Mukisa está solicitando asilo porque su visa está a punto de vencerse. 

**¿Qué podría haber ayudado a Ester y Mukisa?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**

{{% name "Hee-Young" %}} salió a otros países muchas veces con su esposo abusivo cuando éste viajaba por negocios. Ella siempre regresaba a su país con él porque pensaba que no tenía otra opción. Gary duda de esta historia. Piensa que si Hee-Young de verdad corría peligro con 
su esposo, habría aprovechado uno de esos viajes para escaparse. Gary piensa que tuvo suficientes oportunidades para solicitar asilo en uno de esos países.

**¿Qué podría haber ayudado a Hee-Young?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Emily" %}} nunca pensó que alguien como ella pudiera llegar a ser refugiada. Siempre pensó que los refugiados eran personas que le temían a su gobierno. Emily llegó a Canadá muchos años después huyendo de su esposo. Decidió esconderse. Con el paso del tiempo le confió su historia a un par de amistades de confianza. Estaban de acuerdo que la mejor manera de mantenerse a salvo era permanecer escondida. Gary duda que Emily realmente tuviera miedo de su esposo. Piensa que de ser así ella se habría esforzado más por averiguar si podía solicitar protección en Canadá.

**¿Qué podría haber ayudado a Emily?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Volver a su país
----------
{{% /red-flag %}}

{{% name "Désirée" %}} tuvo que huir de su país sin sus hijos. Los dejó al cuidado de su madre. Durante tres meses vivió fuera pero no aguantó la separación. Decidió arriesgarse y volver a su país. Gary piensa que si Désirée de verdad corría peligro en su país no se habría arriesgado regresando para estar con sus hijos.

Cuando los soldados vinieron a buscarlo, {{% name "Andrés" %}} huyó cruzando la frontera. Empezó a hacer su vida en un país vecino. Entonces se enteró de que su padre estaba muy enfermo. Andrés cruzó la frontera escondido para visitarlo. Gary sospecha que Andrés ya no debe temer a las autoridades de su país; si no, no se habría arriesgado regresando.

**¿Qué podría haber ayudado a Désirée y Andrés?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Otros tipos de riesgo
--------------------
{{% /red-flag %}}

{{% name "Lisa" %}} le contó a Gary que a ella y a su novia las golpearon gravemente en un ataque homofóbico. La novia de Lisa murió de sus lesiones. Años después, todavía en el mismo país, Lisa empezó a salir con otra mujer. Gary duda de la historia de Lisa. ¿Por qué correría riesgo con otra relación lesbiana sabiendo lo peligroso que era?

{{% name "Mohammed" %}} recibió advertencias de miembros de un grupo paramilitar de que debía abandonar su labor de activista sindical. Al ignorar sus advertencias, el grupo plantó una bomba que hizo volar su vehículo. La mañana antes de un importante rally del sindicato, Mohammed recibió una llamada amenazándolo de muerte si asistía. Apenas sobrevivió un intento de asesinato. Gary no se cree la historia de Mohammed. No cree que Mohammed correría el riesgo de ir al rally sabiendo que era tan peligroso.

En el país de {{% name "Constance" %}}, predicar su religión es ilegal y es objeto de castigos severos. No obstante, Constance distribuía a menudo panfletos religiosos en baños públicos. Hizo eso aún sospechando que la policía del estado la vigilaba. A Gary le cuesta creer que Constance se habría arriesgado de esa manera.

**¿Qué podría haber ayudado a Lisa, Mohammed y Constance?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Viktor" %}} fue atacado por una pandilla de jóvenes que aterrorizaban a gente de su grupo étnico. Tras dicho ataque, un amigo lo llevó al hospital, donde denunciaron el ataque a las autoridades. El oficial que entrevistó a Viktor lo insultó con una palabra racista y le dijo a Viktor que era de esperar que lo molestaran si andaba por ahí tarde causando problemas. En su audiencia, Viktor no presenta una declaración de su amigo ni un expediente médico o policial. Gary piensa que Viktor debió intentar obtener dichos documentos. Sospecha que Viktor realmente no tiene miedo de que lo envíen de regreso a su país. Si tuviese miedo, habría hecho todo lo posible por apoyar su solicitud.


**¿Qué podría haber ayudado a Viktor?**

* **{{< link "/12-things-that-have-helped/trying-hard-enough" >}}**

{{% name "Moses" %}} le cuenta a Gary que una mujer de una oficina gubernamental lo había ayudado a huir del país. Había pasado por alto las reglas y le había dado papeles que lo autorizaban a viajar. El mismo Moses no es capaz de explicar por qué ella había hecho eso. Supone que quizá él le recordaba a su hijo o a un familiar. Gary duda que esa mujer corriera tal riesgo para ayudar a Moses. No cree que alguien haría algo así por un extraño.

**¿Qué podría haber ayudado a Moses?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}
