---
title: People are cautious and care most about their own safety
linkTitle: Safety at any cost
weight: 20

---

{{% lightbulb %}}
It seems obvious to Gary that people whose lives are at risk will be very afraid. They will be alert for signs of danger. As soon as they are threatened, they will do everything they can to keep themselves and their families safe. They will never take unnecessary risks. Gary believes that reasonable people will always make safety their top priority, even when they have other cares and concerns.

As a result, Gary has a hard time believing that people can be **brave or reckless or optimistic or complacent or overwhelmed** in the face of danger. He also has a hard time believing that a person would ever **value something more than their own safety**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary is often very suspicious when a claimant **took a risk that Gary thinks is foolish**. He may also be suspicious when other people in the claimant’s story took risks.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
Not leaving home sooner
-----------------------
{{% /red-flag %}}

The guerrilla warned {{% name "Sonia" %}} many times to leave the country or else. It was not until they tried to assassinate her that she decided she had to leave. Gary finds this hard to believe. He thinks that if Sonia was really threatened, she would have fled right away.

**What might have helped Sonia?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Manuel" %}} and his family decided to leave their country after the local paramilitary tried to recruit his teenaged son. Manuel’s wife and children left the next day. But Manuel stayed behind for a few more weeks to sell his family’s home and wrap up their business. Gary suspects that Manuel was not really afraid for his own safety. If Manuel had truly been afraid, he would have fled with his family.

The son of a powerful politician stalked and harassed {{% name "Purabi" %}} for months. Several weeks before the end of her final year of university, he made a serious threat. He told her that if she did not agree to marry him, he would burn her with acid. Purabi went into hiding and stopped going to school. But she borrowed her classmate’s notes and kept studying. She went back to campus to write her final exam. The week after, she left for Canada. Gary suspects that Purabi’s story is not true. Surely she would have fled right away, and would not have stayed to finish her degree.


**What might have helped Manuel and Purabi?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Not making a refugee claim right away
-------------------------------------
{{% /red-flag %}}

{{% name "Ester" %}} crossed into the United States from Mexico. She avoided the US authorities and made her way to Canada. Gary thinks that if she was really in danger at home, she would have made a refugee claim in the United States, the first safe country she reached. He suspects that she just wants to live in Canada.

{{% name "Mukisa" %}} was studying at a Canadian university when his family back home learned that he was gay. That meant it was no longer safe for him to return home. Mukisa began to investigate his legal options. He met with a lawyer, learned about the refugee claim process, and started gathering evidence. Before his student visa expired, he filed a refugee claim. Gary thinks that if Mukisa was really afraid of returning home, he would have made his claim much sooner. He suspects that Mukisa is only making his claim because his visa was about to expire.

**What might have helped Ester and Mukisa?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**

{{% name "Hee-Young" %}} went abroad many times with her abusive husband when he traveled for business. She always returned home with him to their country. She did not think that she had any choice. Gary doubts this story. He thinks that if Hee-Young was really in danger from her husband, she would have used one of these trips as a chance to escape. He feels that she had plenty of chances to make a refugee claim in one of these other countries.

**What might have helped Hee-Young?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Emily" %}} never knew that someone like her could be a refugee. She always thought that a refugee was a person who was afraid of their government. Emily came to Canada many years ago to escape from her husband. She went into hiding. Over the years she told her story to a couple of trusted friends. They all agreed that the best way for her to stay safe was to stay hidden. Gary doubts that Emily was really afraid of her husband. He thinks that if she was, she would have tried harder to find out if she could ask for Canada’s protection.

**What might have helped Emily?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Going home
----------
{{% /red-flag %}}

{{% name "Désirée" %}} had to flee her country without her children. She left them in the care of her mother. For three months she lived abroad, but the separation was more than she could take. She decided to take her chances and return home. Gary thinks that if Désirée was really in danger in her country, she would not have put herself at risk by returning to her children.

When the soldiers came looking for him, {{% name "Andrés" %}} escaped cross the border. He began to make a life for himself in a neighbouring country. Then he learned that his father was very ill. Andrés snuck back across the border to visit him. Gary suspects that Andrés no longer fears the authorities in his country. Otherwise he would not have risked returning.

**What might have helped Désirée and Andrés?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Other kinds of risks
--------------------
{{% /red-flag %}}

{{% name "Lisa" %}} told Gary that and her girlfriend were badly beaten in a homophobic attack. Lisa’s girlfriend died of her injuries. Years later, while still living in her country, Lisa began dating another woman. Gary doubts Lisa’s story. Why would she risk having another lesbian relationship, knowing how dangerous this was?

{{% name "Mohammed" %}} was warned by members of a paramilitary group to stop his work as a union activist. When he ignored their warnings, the group planted a bomb that blew up his car. On the morning of a major union rally, Mohammed received a phone call threatening him with death if he attended. Mohammed went anyway. He narrowly survived an attempt to kill him. Gary does not believe Mohammed’s story. He does not believe Mohammed would risk going to the rally when it was obviously so dangerous.

In {{% name "Constance" %}}’s country, preaching her religion is illegal and is punished harshly. Yet Constance would often leave religious pamphlets in public washrooms. She did this even when she suspected that the state police were watching her. Gary cannot believe that Constance would have taken such a risk.

**What might have helped Lisa, Mohammed and Constance?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Viktor" %}} was attacked by a gang of young men who terrorized people from his ethnic group. After the attack, a friend took him to the hospital, where the staff reported the attack to the police. The police officer who interviewed Viktor called him a racist name. He told Viktor that he should expect hassles if he is out late causing trouble. At his hearing, Viktor did not provide a statement from his friend, or any records from the hospital or the police. Gary thinks that Viktor should have tried to get these documents. He suspects that Viktor is not really afraid of being sent home. If he were afraid, he would have done everything he could to try to support his claim.

**What might have helped Viktor?**

* **{{< link "/12-things-that-have-helped/trying-hard-enough" >}}**

{{% name "Moses" %}} told Gary that a woman at a government office helped him to escape his country. She had bent the rules and given him papers that allowed him to travel. Moses himself could not explain why she had done this. He guessed that maybe he reminded her of a son or relative. Gary doubts that this woman would have put herself at serious risk to help Moses. He does not believe that someone would do such a thing for a stranger.

**What might have helped Moses?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


Read about Gary's other Big Ideas:

{{< siblings >}}