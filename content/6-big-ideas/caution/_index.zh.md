---
title: 人们是谨慎的，最关心他们自己的安全
linkTitle: 为了安全不惜一切
weight: 20

---

{{% lightbulb %}}
对Gary来说，显而易见，生命有危险的人是会很害怕的。他们会关注任何危险的信号。一旦受到威胁，他们会尽其所能保护他们自己和家人的安全。他们从不会冒不必要的危险。Gary认为，理智的人，尽管他可能会有其他的关心和顾虑，仍是总会把自己的安全放在首位。

由此，Gary很难相信有些人在遇到危险的时候**会勇敢，或者会鲁莽，或者会乐观，或者会有自满的惰性，或者会不知所措**。同时，他也很难相信，有人会**认为其他东西比他们自己的安全更有价值**。
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
当一个申请人**冒了一个Gary认为是愚蠢的危险的**时候，Garry经常会很怀疑。当他在申请人的故事里看到其他人冒险，他也可能会觉得怀疑。
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
没有更早离开家
-----------------------
{{% /red-flag %}}

游击队警告过 {{% name "Sonia" %}} 很多次，叫她离开她的国家，否则后果自负。可是直到他们试图暗杀她的时候，她才决定离开。Gary觉得这难以置信。他认为，如果Sonia真正受到了威胁，她会马上逃离的。

**什么可能对Sonia有帮助?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Manuel" %}} 和他的家人在当地的武装民兵要收编他的十几岁的儿子的时候就决定离开他们的国家。Manuel的太太和孩子们第二天就离开了。但是，Manuel留下了几个星期，卖他家的房子，收拾他们的生意。Gary怀疑Manual不是真的害怕自己的安全。如果Manuel真的害怕，他就会和家里人一起逃离了。

一个有权势的政客的儿子纠缠和骚扰了{{% name "Purabi" %}}好几个月。在她大学最后一年结束之前的几周，他做出了一个严重的威胁。他对她说，如果她不同意和他结婚，他就用强酸烧她。Purabi躲了起来，也不再去上学了。但是她借了她同学的笔记，继续学习。她回到学校去完成了她的期末考试，一周以后，她离开了，来了加拿大。Gary怀疑的Purabi的故事不是真的。她肯定会马上离开，不会留在那里完成学位。


**什么可能对Manuel和Purabi有帮助?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
没有马上提交难民申请
-------------------------------------
{{% /red-flag %}}

{{% name "Ester" %}} 从墨西哥越境到了美国，她避开了美国官方，到了加拿大。Gary认为，如果她在家真的有危险，她就会在美国，在她到达的第一个安全的国家，申请难民。他怀疑她只是想要生活在加拿大。

{{% name "Mukisa" %}} 在加拿大的大学念书时，他在家的家人知道了他是同性恋。这就意味着回家对他来说是不安全的。Mukisa开始研究他都有些什么法律上的途径，他去见了个律师，了解了申请难民的过程，也开始收集证据。在他的学生签证到期之前，他提交了难民申请。Gary认为，如果Mukisa真是害怕回家的话，他应该会早很多时间就提出申请。他怀疑Mukisa提出申请，只是因为他的签证要到期了。

**什么可能对Ester和Mukisa有帮助?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**

{{% name "Hee-Young" %}} 曾经多次随同虐待她的丈夫到国外出差。她每次都和她的丈夫一起回国去了。她从来没认为她自己有其他的选择。Gary怀疑这个故事。他认为，如果Hee-Young真是面对她丈夫的危险，她就会利用这些出国旅行的机会逃跑。他认为她有充分的机会，在这些其他国家中的一个提出难民申请。

**什么可能对Hee-Young有帮助?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Emily" %}} 从来不知道像她这样的人可以作难民，她一直认为难民是一个害怕他们自己国家政府的人。Emily很多年前为了逃避她的丈夫来到加拿大，她躲藏了起来。这些年中，她把她的情况告诉了两个自己信任的朋友，他们也都认为保证她安全最好的办法就是继续躲起来。Gary置疑Emily是不是真的害怕她的丈夫。他认为，如果她真是害怕，那她就会更尽力地去了解是否可以请加拿大保护她。

**什么可能对Emily有帮助?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
回家
----------
{{% /red-flag %}}

{{% name "Désirée" %}} 在逃离她的国家时，没能带上她的孩子们。她把他们留给她的母亲照顾。她在国外生活了三个月，但是和孩子们的分离让她无法承受。她决定碰碰运气，回了家。Gary认为，如果Desirée在她的国家真是有危险的话，她是不会把自己放在危险的境地，回去和孩子们在一起的。

在士兵们来找他的时候， {{% name "Andrés" %}} 越过边境逃走了。他开始在邻近的国家安顿下来。当他得知他的父亲病重，Andres就偷偷越回了边境去看望他。Gary怀疑Andres已经不再害怕他的国家的政府，否则他就不会冒险回去。

**什么可能对Désirée和Andrés有帮助?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
其他类型的危险
--------------------
{{% /red-flag %}}

{{% name "Lisa" %}} 告诉Gary，她和她的女朋友曾经被仇视同性恋的人殴打得非常严重。Lisa的女朋友，因伤而死去了。多年之后，Lisa仍然住在她的国家，又开始和另外一个女人约会。Gary怀疑Lisa的故事。既然她知道是这么危险，那她为什么又要冒险再有另一个女同性恋关系呢？

{{% name "Mohammed" %}} 被武装民兵组织的成员们警告过，叫他停止作为工会活动家的工作。当他无视他们的警告时，这个组织就放置了一个炸弹，炸了他的车。在一个重要的工会大型集会即将进行的早上，Mohammed收到了一个电话，恐吓他，如果他参加，就会死。Mohammed仍然去了，免强避免被杀。但是Gary不相信Mohammed的故事。他不相信Mohammed会在这样显而易见的危险下，仍然冒险去参加集会。

在 {{% name "Constance" %}}的国家，传播她的宗教是非法的，会受到严厉的制裁。可是Constance经常把宗教的宣传单留在公共厕所里。即使在她怀疑自己被国家警察监视的时候，她仍然这么做。Gary不能相信Constance会去冒这样的危险。

**什么可能对Lisa, Mohammed和Constance有帮助?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

对{{% name "Viktor" %}} 所属民族的人进行伤害和骚扰的一帮年轻人，袭击了Viktor。被袭击了之后，一个朋友把他送去了医院，医院的工作人员把袭击报告了警察。和Viktor面谈的警官用了一个种族歧视的字眼称呼他，他对Viktor说，如果他很晚出去找麻烦，就应该预期会遇到骚扰。在他的听证会上，Viktor没有提供他的那个朋友所写的声明书，也没有医院或者警察的记录材料。Gary认为，Viktor应该尝试去拿到这些文件。他怀疑Viktor不是真的害怕被送回国去。如果他害怕，他就会尽一切所能为他自己的申请寻求支持。

**什么可能对Viktor有帮助?**

* **{{< link "/12-things-that-have-helped/trying-hard-enough" >}}**

{{% name "Moses" %}} 告诉Gary，政府里的一个女工作人员帮他逃离了他的国家。她通融了规定，给了他让他能够旅行的文件。Moses自己也不能解释她为什么会这么做。他猜测，可能是他让她联想起她自己的儿子或者亲戚。Gary质疑这个女人会为了帮助Moses而把自己置于很危险的境地，他不相信有人会为一个陌生人做这样的事情。

**什么可能的对Moses有帮助?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


阅读Gary其他的“大的看法”:

{{< siblings >}}