---
title: Les gens sont prudents et leur sécurité est ce qui leur tient le plus à cœur
linkTitle: La sécurité avant tout
weight: 20

---

{{% lightbulb %}}
Pour Gary, il semble évident que les personnes dont la vie est en danger ont très peur. Elles sont à l’affût des dangers. Dès qu’elles sont menacées, elles feront tout ce qu’elles peuvent pour être en sécurité, tant pour elles-mêmes que pour leur famille. Elles ne prendront jamais de risques inutiles. Gary croit que pour les personnes responsables, la sécurité est la principale priorité, même si elles ont d’autres charges ou préoccupations.

C’est pourquoi Gary a du mal à comprendre que les personnes puissent être **courageuses, téméraires, optimistes, complaisantes ou dépassées** face au danger. Il a aussi du mal à croire qu’une personne puisse **accorder plus d’importance à autre chose qu’à sa propre sécurité**.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary a des doutes lorsqu’un demandeur **a pris des risques qui lui semblent naïfs**. Il a aussi des doutes si une autre personne dans l’histoire du demandeur a pris des risques.
{{% /gary-suspicious-highlight %}}


{{% red-flag %}}
Ne pas quitter la maison plus tôt
-----------------------
{{% /red-flag %}}

La guérilla a averti {{% name "Sonia" %}} plusieurs fois, lui intimant de quitter le pays, sinon. C’est seulement quand ils ont essayé de l’assassiner qu’elle a décidé de partir. Gary a du mal à y croire. Il pense que si Sonia avait réellement été menacée, elle serait partie tout de suite. 

**Qu’est-ce qui aurait aidé Sonia?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Manuel" %}} et sa famille ont décidé de quitter leur pays quand les paramilitaires locaux ont essayé de recruter son fils adolescent. L’épouse et les enfants de Manuel sont partis le lendemain. Mais Manuel est resté pendant quelques semaines pour vendre la maison familiale et mettre fin aux activités de son entreprise. Gary pense que Manuel n’avait pas vraiment peur pour sa sécurité. S’il avait vraiment eu peur, il serait parti avec sa famille. 

Le fils d’un politicien influent a suivi et harcelé {{% name "Purabi" %}} pendant des mois. Plusieurs semaines avant la fin de sa dernière année à l’université, il a proféré des menaces sérieuses. Il a dit que si elle refusait de l’épouser, il la brûlerait avec de l’acide. Purabi s’est cachée et elle a arrêté d’aller à ses cours. Elle a emprunté les notes de ses camarades de classe et a continué à étudier. Elle est retournée à l’université pour faire ses examens de fin d’année. Elle est partie pour le Canada la semaine suivante. Gary a des doutes. Si c’était vrai, Purabi serait sûrement partie tout de suite et ne serait pas restée pour terminer ses études.


**Qu’est-ce qui aurait aidé Manuel et Purabi?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Ne pas présenter sa demande d’asile tout de suite
-------------------------------------
{{% /red-flag %}}

{{% name "Ester" %}} a quitté le Mexique pour aller aux États-Unis. Elle a évité les autorités américaines et s’est rendue jusqu’au Canada. Gary pense que si elle était vraiment en danger chez elle, elle aurait fait sa demande d’asile aux États-Unis, le premier pays sécuritaire où elle a pu se rendre. Il se demande si elle ne veut pas tout simplement vivre au Canada. 

{{% name "Mukisa" %}} étudiait dans une université canadienne quand sa famille, dans son pays d’origine, a appris qu’il était gay. C’est pourquoi il n’est pas sécuritaire pour lui de retourner chez lui. Mukisa a commencé à faire des recherches sur les possibilités juridiques. Il a rencontré un avocat, s’est informé sur le processus de demande d’asile et a commencé à recueillir des preuves. Il a présenté sa demande d’asile avant l’échéance de son visa d’étudiant. Gary pense que si Mukisa avait vraiment peur de retourner chez lui, il aurait présenté sa demande bien avant. Il se demande si Mukisa présente sa demande d’asile parce que son visa tirait à sa fin. 

**Qu’est-ce qui aurait aidé Ester et Mukisa?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**

{{% name "Hee-Young" %}} est allée à l’étranger souvent avec son mari violent alors qu’il voyageait pour le travail. Elle retournait toujours avec lui à la maison, dans son pays. Elle ne pensait pas avoir d’autres options. Gary trouve cette histoire louche. Il pense que si Hee-Young était vraiment en danger, elle se serait échappée lors d’un de ces voyages. Il a l’impression qu’elle a eu beaucoup d’autres chances de faire une demande d’asile dans ces autres pays. 

**Qu’est-ce qui aurait aidé Hee-Young?**

* **{{< link "/12-things-that-have-helped/shaped-by-experiences" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Emily" %}} ne savait pas qu’une personne comme elle pouvait être une réfugiée. Elle pensait qu’un réfugié est une personne qui a peur du gouvernement de son pays. Emily est venue au Canada il y a de nombreuses années pour échapper à son mari. Elle cherchait à se cacher. Au cours des années, elle a raconté son histoire à quelques amis proches. Ils étaient tous d’accord : pour être en sécurité, elle devait se cacher. Gary doute qu’Emily ait eu vraiment peur de son mari. Il pense que si c’était vraiment le cas, elle aurait essayé d’obtenir plus d’information sur la protection du Canada. 

**Qu’est-ce qui aurait aidé Emily?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Retourner à la maison
----------
{{% /red-flag %}}

{{% name "Désirée" %}} a dû s’enfuir de son pays sans ses enfants. Elle les a laissés avec sa mère. Elle a habité à l’étranger pendant trois mois, mais la séparation était trop difficile. Elle a décidé de prendre le risque et de retourner à la maison. Gary pense que si Désirée était vraiment en danger dans son pays, elle n’aurait jamais pris le risque d’aller voir ses enfants.

Quand les soldats se sont mis à sa recherche, {{% name "Andrés" %}} s’est enfui en traversant la frontière. Il a recommencé sa vie dans un pays voisin. Quand il a appris que son père était très malade, Andrés a retraversé la frontière pour se rendre à son chevet. Gary doute qu’Andrés ait encore peur des autorités de son pays. Autrement il n’aurait jamais pris le risque de retourner dans son pays.

**Qu’est-ce qui aurait aidé Désirée et Andrés?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**


{{% red-flag %}}
Autres risques
--------------------
{{% /red-flag %}}

{{% name "Lisa" %}} a raconté à Gary qu’elle et sa partenaire avaient été gravement battues lors d’une attaque homophobe. Sa partenaire est décédée des suites de ses blessures. Quelques années plus tard, alors qu’elle habitait encore dans son pays, Lisa a commencé à fréquenter une autre femme. Gary a des doutes. Pourquoi prendrait-elle le risque d’avoir une autre relation homosexuelle, sachant à quel point s’était dangereux?

{{% name "Mohammed" %}} a reçu des avertissements des membres d’un groupe paramilitaire au sujet de ses activités syndicalistes. Comme il a ignoré leurs avertissements, le groupe a mis une bombe qui a fait exploser sa voiture. Le matin d’un rassemblement syndicaliste important, Mohammed a reçu des menaces de mort par téléphone l’intimant de ne pas se rendre au rassemblement. Mohammed est allé quand même. Il a survécu de justesse à l’attentat contre lui. Gary ne croit pas l’histoire de Mohammed. Il pense que Mohammed n’aurait pas pris le risque d’aller au rassemblement si c’était vraiment dangereux. 

Dans le pays de {{% name "Constance" %}}, prêcher sa religion est illégal et puni sévèrement. Pourtant, Constance laissait souvent des dépliants religieux dans les toilettes publiques. Elle a continué à le faire, même si elle se doutait que la police d’État la surveillait. Gary n’arrive pas à croire que Constance ait pris ce risque.

**Qu’est-ce qui aurait aidé Lisa, Mohammed et Constance?**

* **{{< link "/12-things-that-have-helped/why-take-the-risk" >}}**
* **{{< link "/12-things-that-have-helped/social-science" >}}**

{{% name "Viktor" %}} a été attaqué par un gang de jeunes hommes qui terrorisaient les personnes de son groupe ethnique. Après l’attaque, un ami l’a accompagné à l’hôpital où le personnel a appelé la police pour rapporter l’événement. L’officier de police qui a interrogé Viktor l’a interpelé par un nom raciste. Il a dit à Viktor de s’attendre à avoir des problèmes s’il sort tard le soir et qu’il fait du grabuge. À l’audience, Viktor n’a fourni ni déclaration de la part de son ami, ni rapports de l’hôpital ou de la police. Gary est d’avis que Viktor aurait dû essayer d’obtenir ces documents. Il pense que Viktor n’a pas vraiment peur de retourner chez lui. S’il avait vraiment peur, il aurait tout fait pour obtenir tout ce qui peut soutenir sa demande d’asile. 

**Qu’est-ce qui aurait aidé Viktor?**

* **{{< link "/12-things-that-have-helped/trying-hard-enough" >}}**

{{% name "Moses" %}} a dit à Gary qu’une femme dans un bureau gouvernemental l’a aidé à s’enfuir de son pays. Elle avait contourné les règles pour lui donner les documents nécessaires lui permettant de voyager. Même Moses n’arrive pas à expliquer ce qui l’a poussé à agir ainsi. Il devine que peut-être il lui rappelait un fils ou un homme de sa famille. Gary doute que cette femme ait accepté de prendre un tel risque pour aider Moses. Il ne croit pas que quelqu’un poserait un tel geste pour un étranger.

**Qu’est-ce qui aurait aidé Moses?**

* **{{< link "/12-things-that-have-helped/social-science" >}}**


Découvrir les autres Grandes Idées de Gary

{{< siblings >}}