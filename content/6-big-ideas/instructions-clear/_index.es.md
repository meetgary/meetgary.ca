---
title: Nuestras expectativas eran claras
weight: 50

---

{{% lightbulb %}}
Gary entiende qué tipo de información las personas solicitantes deben incluir en sus formularios de solicitud de asilo. Piensa que **las instrucciones indicadas en los formularios** también le explican esto claramente a las personas solicitantes. Gary también piensa que **sus preguntas** son sencillas y directas. Cuando hace una pregunta, piensa que a las personas solicitantes no les debería costar entender lo que quiere decir.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary sospecha mucho cuando una persona solicitante ha **interpretado las instrucciones de otra manera** o **malentendido sus preguntas**.
{{% /gary-suspicious-highlight %}}

{{% red-flag %}}
Redacción de la historia
-----------------
{{% /red-flag %}}

El formulario BOC indica a las personas solicitantes que, al redactar su historia, deben “incluir todo lo que sea importante”. {{% name "Svetlana" %}} explicó en el formulario BOC que su esposo la lastimaba a diario durante años. Describió en detalle dos ocasiones tan graves que tuvo que ir al hospital. Durante la audiencia, Svetlana también le cuenta a Gary de una tercera vez que tuvo que ir al hospital a causa del abuso de su esposo. Como solamente había mencionado dos visitas hospitalarias en su formulario BOC, Gary piensa que está inventando esta tercera visita a fin de dar más peso a su historia.

{{% name "Jamal" %}} había sufrido persecución toda su vida por ser gay. Durante su audiencia, Jamal le cuenta a Gary sobre un ataque en el que le fracturaron un dedo. Gary piensa que un ataque de tal gravedad es claramente algo de suficiente “importancia” como para incluirlo en el formulario BOC. Como Jamal no lo mencionó de manera específica, Gary concluye que lo ha inventado.

En su narración de BOC, {{% name "Araceli" %}} describió las llamadas telefónicas anónimas que recibió cuando empezó a investigar a un político corrupto. Escribió que la persona que llamó la amenazó y le advirtió que abandonara el asunto. Durante su audiencia da más detalles: la persona que llamó amenazó matarla si no abandonaba su investigación. Gary piensa que el hecho de recibir una amenaza de muerte es una información muy importante. Como no incluyó ese detalle en su narración escrita, Gary sospecha que Araceli lo ha añadido durante la audiencia para dar mayor peso a su solicitud.

{{% name "Lester" %}} y su esposa e hijos tuvieron que huir de su país tras un golpe militar. Durante su audiencia, Lester explica que su hermano también se vio obligado a esconderse. En su formulario BOC, Lester no mencionó los problemas de su hermano, solamente los propios. Ahora Gary tiene sospechas porque en el formulario BOC se pide explicar lo que le sucedió “a usted y a su familia”, así como a “personas en una situación similar a la suya”. Gary piensa que Lester está inventando los problemas de su hermano en un intento de dar peso a su solicitud.

**¿Qué podría haber ayudado a Svetlana, Jamal, Araceli y Lester?**

* **{{< link "/12-things-that-have-helped/the-instructions" >}}**


{{% red-flag %}}
Proporcionar detalles biográficos
------------------------------
{{% /red-flag %}}

{{% name "Mamun" %}} se escondió en la casa de su tía. Vivió con ella un par de semanas antes de huir del país. En el formulario de IRCC se pedía que Mamun diera una lista de sus direcciones pasadas. Mamun no incluyó la dirección de su tía. Dio su propia dirección como el domicilio donde estaba viviendo hasta el día en que salió del país. Gary sospecha. Como Mamun no indicó la dirección de su tía, Gary sospecha que Mamun nunca se escondió y que Mamun estuvo viviendo en su casa hasta que se fue del país.

Una semana antes de huir de su país, {{% name "Pia" %}} tuvo que renunciar a su trabajo porque le daba mucho miedo salir de su casa. En el formulario IRCC se pedía que Pia diera su historial laboral, donde indicó que estuvo empleada hasta el día en que salió del país. Como Pia no indicó que estuvo desempleada esa última semana, Gary sospecha que Pia estuvo trabajando hasta el día que se fue. Sospecha que nunca se vio obligada a renunciar a su trabajo.

**¿Qué podría haber ayudado a Mamun y Pia?**

* [**Explicar cómo entendieron las instrucciones**](/es/12-things-that-have-helped/the-instructions)


{{% red-flag %}}
Entender las preguntas de Gary
------------------------------
{{% /red-flag %}}

Gary le pregunta a {{% name Angela %}}: “¿Por qué piensas que tu padre te sigue buscando?” Angela explica que su padre la está buscando porque está muy enojado con ella. Gary pregunta de nuevo: “Muy bien, pero ¿por qué piensas que te sigue buscando?” Ella responde: “Porque siente mucha vergüenza de mí y lo he hecho enojar mucho”. Gary piensa que Angela está evadiendo su pregunta.

{{% name "Farzad" %}} fue detenido dos veces. La segunda vez, la policía le quitó su carnet de identidad. Durante la audiencia, Gary le pregunta a Farzad: “¿La policía te quitó tu carnet de identidad?” Gary preguntaba sobre la primera detención. Pero Farzad piensa que Gary se refería a la segunda detención. Farzad contesta, “Sí, al momento de llegar a la estación”. Ahora Gary piensa que la historia de Farzad ha cambiado. Piensa que ha pescado a Farzad mintiendo.

**¿Qué podría haber ayudado a Angela y Farzad?**

* [**Explicar cómo entendieron las preguntas**](/es/12-things-that-have-helped/the-instructions)


Leer sobre otras Grandes Ideas de Gary:

{{< siblings >}}