---
title: Nos attentes étaient claires
weight: 50

---

{{% lightbulb %}}
Gary comprend le type d’information que devraient fournir les demandeurs d’asile dans leurs formulaires de demande. Il pense que **les instructions sur les formulaires** sont claires. Gary pense aussi que **ses questions** sont simples et évidentes. Quand il pose une question, il pense que les demandeurs ne devraient pas avoir de mal à comprendre ce qu’il veut dire. 
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary a des doutes quand un demandeur **interprète les instructions différemment** ou qu’il **comprend mal ses questions**.
{{% /gary-suspicious-highlight %}}

{{% red-flag %}}
Écrire l’histoire
-----------------
{{% /red-flag %}}

Le formulaire FDA invite les demandeurs, alors qu’ils écrivent leur histoire, à « écrire tout ce qui est important pour votre demande d’asile ». {{% name "Svetlana" %}} a expliqué sur le formulaire FDA que son mari la frappait presque tous les jours pendant des années. Elle a décrit en détail deux instances où elle était tellement mal en point qu’elle a dû se rendre à l’hôpital. À l’audience, Svetlana a aussi raconté à Gary un troisième incident qui l’a forcé à aller à l’hôpital en raison de l’abus de son mari. Comme elle n’a mentionné que deux visites à l’hôpital dans son formulaire FDA, Gary pense qu’elle a inventé cette troisième visite pour renforcer sa demande. 

{{% name "Jamal" %}} a souffert de persécution toute sa vie parce qu’il est gay. À l’audience, Jamal a raconté à Gary une attaque où il s’est fait briser un doigt. Gary pense que Jamal aurait dû inclure une attaque aussi « importante » sur le formulaire FDA. Comme il ne l’a pas écrit, Gary conclut que Jamal l’a inventé.

Dans l’histoire qu’elle a présentée sur le formulaire FDA, {{% name "Araceli" %}} décrit les appels téléphoniques anonymes qu’elle recevait quand elle a commencé à faire des recherches sur un politicien corrompu. Elle écrit que l’interlocuteur la menaçait et l’a averti d’arrêter ses recherches. À l’audience elle a fourni plus de détail : la personne à l’autre bout du fil a dit qu’il la tuerait si elle ne mettait pas fin à son enquête. Gary pense qu’une menace de mort est une pièce d’information très importante. Comme elle ne l’a pas mentionné dans son histoire à l’écrit, Gary se demande si Araceli a ajouté cela pour donner plus de poids à sa demande d’asile.

{{% name "Lester" %}}, sa femme et ses enfants ont dû quitter leur pays après un coup d’État militaire. À l’audience, Lester a expliqué que son frère avait aussi été obligé de s’enfuir. Lester n’avait pas mentionné les problèmes de son frère dans son formulaire FDA. Gary a des doutes, car le formulaire invite Lester à expliquer ce qui est arrivé à « vous et votre famille », de même qu’aux « personnes qui sont dans une situation semblable ». Gary se demande si Lester a inventé les problèmes de son frère pour renforcer sa demande d’asile.

**Qu’est-ce qui aurait aidé Svetlana, Jamal, Araceli et Lester?**

* **{{< link "/12-things-that-have-helped/the-instructions" >}}**


{{% red-flag %}}
Donner des détails biographiques
------------------------------
{{% /red-flag %}}

{{% name "Mamun" %}} s’est caché dans la maison de sa tante. Il a habité avec elle pendant quelques semaines avant de quitter son pays. Sur le formulaire d’IRCC, Mamum devait fournir toutes ses adresses passées. Il n’a pas indiqué l’adresse de sa tante. Il a seulement indiqué son adresse domiciliaire comme sa seule adresse jusqu’à son départ du pays. Gary a des doutes. Parce que Mamun n’a pas indiqué l’adresse de sa tante sur le formulaire, Gary pense qu’il ne s’est pas caché du tout. Il pense que Mamun vivait chez lui jusqu’à son départ du pays.

Une semaine avant de s’enfuir de son pays, {{% name "Pia" %}} a été obligée de remettre sa démission, car elle avait trop peur de sortir de la maison. Dans le formulaire d’IRCC, Pia devait décrire ses emplois passés. Elle a indiqué qu’elle était employée jusqu’au jour de son départ du pays. Parce qu’elle n’a pas écrit qu’elle était sans emploi pendant la dernière semaine, Gary se demande si Pia travaillait jusqu’au jour de son départ. Il pense qu’elle n’a jamais été obligée de démissionner.

**Qu’est-ce qui aurait aidé Mamun et Pia?**

* [**Expliquer sa compréhension des instructions**](/fr/12-things-that-have-helped/the-instructions)


{{% red-flag %}}
Comprendre les questions de Gary
------------------------------
{{% /red-flag %}}

Gary a demandé à {{% name Angela %}} : « Pourquoi pensez-vous que votre père vous cherche encore? » Angela a expliqué que son père était à sa recherche parce qu’il était très en colère contre elle. Gary a posé la même question : « Oui, mais pourquoi pensez-vous qu’il est encore à votre recherche? » Elle a répondu : « Parce qu’il a très honte de moi et qu’il est en colère par ma faute. » Gary pense qu’Angela évite sa question.

{{% name "Farzad" %}} a été arrêté deux fois. Lors de la deuxième arrestation, la police a confisqué ses pièces d’identité. À l’audience, Gary a demandé à Farzad : « La police a-t-elle pris vos pièces d’identité? » Gary posait la question par rapport à la première arrestation. Mais Farzad pensait qu’il parlait de la deuxième arrestation, c’est pourquoi il a répondu : « Oui, dès que nous sommes arrivés au poste. » Maintenant, Gary pense que l’histoire de Farzad a changé. Il pense que Farzad ment.

**Qu’est-ce qui aurait aidé Angela et Farzad?**

* [**Expliquer sa compréhension de la question**](/fr/12-things-that-have-helped/the-instructions)


Découvrir les autres Grandes Idées de Gary :

{{< siblings >}}