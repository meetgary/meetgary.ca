---
title: 我们的预期是清楚的
weight: 50

---

{{% lightbulb %}}
Gary明白申请人在他们的难民申请表格上都应该包括些什么信息内容。他认为，**表格上的指示**也已经把这些都向申请人讲清楚了。Gary也认为**他提的问题**是简单和直接的。在他提一个问题的时候，他认为申请人不应该有困难理解他的意思。
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
一个申请人如果**对指示的有不同的理解**或者**误解了他的问题**，Gary就会很怀疑。
{{% /gary-suspicious-highlight %}}

{{% red-flag %}}
写故事
-----------------
{{% /red-flag %}}

申请难民基本材料表 (BOC) 上告诉申请人，在写他们的故事时，要“包含所有重要的内容”。 {{% name "Svetlana" %}} 在BOC表格上解释了，她的丈夫在很多年中几乎每天都会伤害她。她详细地描述了，有两次他伤她非常厉害的时候，她得要去医院。在听证会上，Svetlana又和Gary讲到了她有第三次因为丈夫的虐待而去医院。因为在她的BOC表格里，她只提到去过医院两次，Gary认为她是为了把自己的故事编得更强，而编造了第三次。

{{% name "Jamal" %}} 一生都因为他是一个同性恋者而遭受迫害。在他的听证会上, 他向Gary讲述，有一次他被袭击，手指被打断了。Gary认为，这么严重的袭击明显是一个够“重要”的事情，应该包括在他的BOC表格里。因为Jamel没有具体写到这件事，Gary结论，这件事是他编造的。

在她的BOC故事描述里，{{% name "Araceli" %}} 描述了在她开始调查一个腐败的政客的时候收到的那些匿名电话。她写道，打电话的人恐吓她，警告她让她停止这件事。在她的听证会上，她讲了更多的细节：打电话的人恐吓她，如果她不停止调查就杀了她。Gary认为，她收到了死亡威胁的事实，是一个很重要的信息。因为她没有把这个细节在她所写的故事描述里包含进去，Gary怀疑Araceli是为了让她的申请理由更强，而在她的听证会上加了这个内容。

{{% name "Lester" %}} 和他的太太和孩子在军事政变以后就不得不逃离他们的国家。在他的听证会上，Lester解释说，他的兄弟也被迫要去躲藏起来。在他的BOC表格上，Lester没有提到他兄弟的困难，只讲了他自己的。Gary现在就有了怀疑，因为在BOC表格中，要求Lester讲述关于“你和你的家人”以及“类似情形中的其他人”所发生的事情。Gary认为Lester为了让他自己的申请更强，而编造了他兄弟的困难。

**什么可能对Svetlana, Jamal, Araceli和Lester有帮助?**

* **{{< link "/12-things-that-have-helped/the-instructions" >}}**


{{% red-flag %}}
提供个人经历信息
------------------------------
{{% /red-flag %}}

{{% name "Mamun" %}} 到他阿姨的家里躲起来了。他和她住了两个星期后才逃离了他的国家。IRCC表格让Mamum提供他过去的地址，Mamum没有包括他阿姨的地址，他把自己家的地址写成直到离开他的国家最后一天都一直住的地址。Gary怀疑了。因为Mamum没有写上他阿姨的地址，Gary怀疑Mamum从来也没有去躲藏起来。Gary怀疑Mamum直到离开他的国家之前，都一直住在自己家里。

在她逃离她的国家一周以前, {{% name "Pia" %}} 不得不辞了工作，因为她太害怕离开家了。 IRCC表格要求Pia提供工作历史信息。她写成，直到离开她的国家那一天，她都仍然在职。因为她没有写她在最后一周是无业状态，Gary怀疑，直到她离开，Pia都一直在工作。他怀疑她根本就没有被迫辞掉工作。

**什么可能对Mamun和Pia有帮助?**

* [**解释他们对指示是如何理解的**](/zh/12-things-that-have-helped/the-instructions)


{{% red-flag %}}
理解Gary的问题
------------------------------
{{% /red-flag %}}

Gary问 {{% name Angela %}}, “你为什么认为你的爸爸仍然在找你？”。 Angela解释说，她的爸爸在找她，是因为他对她非常气愤。Gary又问了一遍，“是的，可是你为什么认为他仍然在找你呢？” 她回答说，“他因为我而感到非常的耻辱，我让他非常气愤。” Gary认为，Angela在回避他的问题。

{{% name "Farzad" %}} 被逮捕过两次。在他第二次被逮捕的时候，警察把他的身份证件拿走了。在听证会上，Gary问Farzed“警察有没有把你的身份证件拿走？” Gary正在问第一次被捕时候的事，可是Farzed以为Gary在问他第二次被捕时候的事。 Farzed回答说“对，我们一到警察局就拿走了。” Gary认为Farzed的故事变了，他认为他抓住Farzed说谎了。

**什么可能对Angela和Farzad有帮助?**

* [**解释他们是如何理解问题的**](/zh/12-things-that-have-helped/the-instructions)


阅读Gary其他的“大的看法”:

{{< siblings >}}