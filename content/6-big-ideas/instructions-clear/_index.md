---
title: Our expectations were clear
weight: 50

---

{{% lightbulb %}}
Gary understands what kind of information claimants should include on their refugee claim forms. He thinks that **the instructions on the forms** make this clear to claimants as well.  Gary also thinks that **his questions** are simple and straightforward. When he asks a question, he thinks that claimants should have no trouble understanding what he means.
{{% /lightbulb %}}

{{% gary-suspicious-highlight %}}
Gary is very suspicious when a claimant has **interpreted the instructions differently** or **misunderstood his questions**.
{{% /gary-suspicious-highlight %}}

{{% red-flag %}}
Writing the story
-----------------
{{% /red-flag %}}

The BOC form tells claimants, in writing their story, to “include everything that is important.” {{% name "Svetlana" %}} explained on the BOC form that her husband hurt her almost daily for years. She described in detail two times when he hurt her so badly that she had to go to the hospital. At the hearing, Svetlana also told Gary about a third time when she had to go to the hospital because of her husband’s abuse. Since she had only mentioned two hospital visits on her BOC form, Gary thinks that she is making up this third visit in order to make her story stronger.

{{% name "Jamal" %}} had suffered a lifetime of persecution as a gay man. At his hearing, Jamal told Gary about an attack during which his finger was broken. Gary thinks that an attack this severe was clearly “important” enough to include on the BOC form. Since Jamal did not specifically mention it, Gary concludes that he has invented it.

In her BOC narrative, {{% name "Araceli" %}} described the anonymous telephone calls that she received when she started investigating a corrupt politician. She wrote that the caller threatened her and warned her to drop the matter. In her hearing, she gave more details: the caller threatened to kill her if she did not stop her investigation. Gary thinks that the fact that she received a death threat is very important information. Since she did not include this detail in her written narrative, Gary suspects that Araceli has added it at the hearing to make her claim stronger.

{{% name "Lester" %}} and his wife and children had to flee their country after a military coup. In his hearing, Lester explained that his brother had also been forced into hiding. Lester did not mention his brother’s troubles in his BOC form, only his own. Now Gary is suspicious because the BOC form asked Lester to explain what happened to “you and your family,” as well as to “persons in situations similar to yours.” Gary thinks that Lester is making up his brother’s troubles to try to make his claim stronger.

**What might have helped Svetlana, Jamal, Araceli and Lester?**

* **{{< link "/12-things-that-have-helped/the-instructions" >}}**


{{% red-flag %}}
Giving biographical details
------------------------------
{{% /red-flag %}}

{{% name "Mamun" %}} went into hiding at his aunt’s house. He lived with her for a couple of weeks before fleeing his country. The IRCC form asked Mamun to list his past addresses. Mamun did not include his aunt’s address. He gave his own home address as the address where he was living until the day that he left the country. Gary is suspicious. Because Mamun did not list his aunt’s address, Gary suspects that Mamun never went into hiding. He suspects that Mamun was living at home until he left his country.

A week before she fled her country, {{% name "Pia" %}} had to quit her job because she was too afraid to leave her house. The IRCC form asked Pia to give her employment history. She listed herself as employed until the day that she left the country. Because she did not list that she was unemployed for that last week, Gary suspects that Pia was working until the day that she left. He suspects that she was never forced to quit her job.

**What might have helped Mamun and Pia?**

* [**Explaining how they understood the instructions**](/12-things-that-have-helped/the-instructions)


{{% red-flag %}}
Understanding Gary's questions
------------------------------
{{% /red-flag %}}

Gary asked {{% name Angela %}}, “Why do you think that your father is still looking for you?” Angela explained that her father was looking for her because he was very angry with her. Gary asked again, “Yes, but why do you think that he is still looking for you?” She responded, “Because he is very ashamed of me and I have made him very angry.” Gary thinks that Angela is avoiding his question.

{{% name "Farzad" %}} was arrested twice. During the second arrest, the police took away his ID. In the hearing, Gary asked Farzad, “Did the police take your ID?” Gary was asking about the first arrest. But Farzad thought that Gary was asking about the second arrest. Farzad answered, “Yes, as soon as we got to the station.” Now Gary thinks that Farzad’s story has changed. He thinks that he has caught Farzad in a lie.

**What might have helped Angela and Farzad?**

* [**Explaining how they understood the questions**](/12-things-that-have-helped/the-instructions)


Read about Gary's other Big Ideas:

{{< siblings >}}