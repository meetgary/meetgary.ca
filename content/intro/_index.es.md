---
title: Conocer a Gary
menuTitle: Introducción
weight: 10

---

<span class="img-center" margin-bottom="10px"><img class="inline" src="/images/GaryBear-face-basic.png" /></span>
<br/>
Gary tiene un trabajo importante. Es miembro de la Comisión de Refugiados. Él decide cuáles solicitantes de asilo necesitan la protección de Canadá.

Gary trabaja muy duro y se esfuerza por hacer un buen trabajo. **Jamás querría enviar a una persona refugiada a su país donde corre peligro.**

<span class="img-center"><img class="inline" src="/images/GaryBear-face-suspicious.png" /></span>
<br/>
Pero Gary se preocupa. **Lo que más le preocupa es que una persona mentirosa lo engañe.** Ante todo, Gary desea asegurarse de que una persona que sólo finge ser refugiada no logre quedarse en Canadá.

Gary sospecha a menudo de las historias que cuentan las personas que solicitan asilo. **Muchas de sus sospechas provienen de sus Seis Grandes Ideas sobre cómo funciona el mundo.**


Cómo utilizar este sitio web
-----------------------

**Este sitio web no presta asesoramiento legal.** No explica ni qué hacer ni qué decir en su audiencia de solicitud de asilo. No obstante, puede serle útil considerar la información aquí presentada y consultar al respecto con su representante legal, si tiene uno.

En este sitio web se presupone que usted conoce los aspectos básicos del proceso de solicitud de asilo en Canadá.

{{% button href="/other-sources" %}}Aprender más sobre el proceso de solicitud de asilo{{% /button %}}

<br />
En este sitio web, conocerá a **más de 50 solicitantes**. Aunque sus historias son inventadas, éstas se basan en las experiencias de muchas personas reales que han pasado por el sistema de asilo canadiense. Estas historias le ayudarán a entender cómo piensa Gary.


{{% lightbulb %}}
Las Seis Grandes Ideas de Gary
--------------------

Aquí podrá aprender sobre las [**Seis Grandes Ideas de Gary**](/es/6-big-ideas). ¿Cómo ve Gary el mundo? ¿Qué lo hace sospechar y por qué?

{{% button href="/6-big-ideas" %}}Aprender sobre Seis Grandes Ideas de Gary{{% /button %}}

{{% /lightbulb %}}

{{% gears %}}
12 cosas que han ayudado
--------------------------

Cuando Gary cree que una persona solicitante está mintiendo, a veces nada logrará que cambie de parecer. Pero a veces las personas solicitantes pueden ayudarlo a entender que dicen la verdad. Aquí podrá aprender sobre [**12 cosas que personas solicitantes han dicho o hecho que han ayudado**](/es/12-things-that-have-helped) a calmar las sospechas de Gary. O, si no lograron persuadir a Gary, tal vez hayan ayudado a que la División de Apelaciones para Refugiados o el Tribunal Federal revocara su decisión. 

{{% button href="/12-things-that-have-helped" %}}Aprender sobre 12 cosas que han ayudado {{% /button %}}
{{% /gears %}}
No desanimarse
----------

Gary no es una persona real. Lo hemos inventado a fin de ayudar a que usted se prepare para algunos de los razonamientos más desfavorables de la Comisión de Refugiados. No obstante, tiene motivos de sobra para esperar resultados positivos. Algunos miembros de la Comisión se parecen mucho a Gary. Otros sólo un poco. **Muchos no piensan como Gary en absoluto.** Es importante tener presente que la Comisión de Refugiados da asilo a la mayoría de las personas que lo solicitan.

Gary no se basa en ningún miembro de la Comisión llamado Gary, ni pasado ni presente.