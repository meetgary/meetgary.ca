---
title: Voici Gary
menuTitle: Introduction
weight: 10

---

<span class="img-center" margin-bottom="10px"><img class="inline" src="/images/GaryBear-face-basic.png" /></span>
<br/>
Gary fait un travail important. Il est membre de la Commission du statut de réfugié. Il détermine qui a besoin de la protection du Canada. 

Gary travaille très fort et il veut bien faire son travail. **Il ne voudrait jamais mettre un réfugié en danger.**

<span class="img-center"><img class="inline" src="/images/GaryBear-face-suspicious.png" /></span>
<br/>
Mais Gary est préoccupé. **Ce qui le préoccupe le plus, c’est qu’une personne pourrait le duper.** Avant tout, Gary veut s’assurer qu’une personne qui fait semblant d’être un réfugié ne restera pas au Canada.

Gary met souvent en doute les histoires des demandeurs d’asile. **Bon nombre de ses doutes viennent de ses Six Grandes Idées sur le monde et ce qui s’y passe.**

Pour consulter ce site
-----------------------

**Ce site Web n’offre pas de conseil juridique.** Vous n’y trouverez pas des conseils sur ce que vous devez faire, ou ce que vous devez dire lors de votre audience. Mais les informations qui s’y trouvent pourraient vous être utiles; elles pourraient éclairer votre réflexion et vous pourriez en parler avec votre représentant juridique, si vous en avez un.

Ce site Web tient pour acquis que vous êtes déjà familiarisé avec le processus canadien de demande d’asile.  

{{% button href="/other-sources" %}}Les procédures de la demande d’asile{{% /button %}}

<br />
Sur ce site, vous allez faire la connaissance de **plus de 50 demandeurs d’asile**. Leurs histoires sont fictives, mais elles sont inspirées d’histoires vraies de personnes qui sont passées par le système de demande d’asile canadien. Ces histoires vous aideront à mieux comprendre ce que pense Gary.


{{% lightbulb %}}
Les Six Grandes Idées de Gary
--------------------

Vous découvrirez les [**Six Grandes Idées de Gary**](/fr/6-big-ideas). Comment perçoit-il le monde? Qu’est-ce qui le fait douter et pourquoi?

{{% button href="/6-big-ideas" %}}Découvrir les Six Grandes Idées de Gary{{% /button %}}

{{% /lightbulb %}}

{{% gears %}}
12 choses qui ont aidé
--------------------------

Si Gary pense qu’un demandeur d’asile ment, il se peut que rien ne le fasse changer d’idée. Mais parfois les demandeurs peuvent l’aider à comprendre qu’ils disent la vérité. Ici vous découvrirez [**douze choses que les demandeurs ont dit ou fait qui ont fait une différence**](/fr/12-things-that-have-helped) et ont atténué les doutes de Gary. Ou, si elles n’ont pas convaincu Gary, elles ont aidé la Section d’appel des réfugiés ou la Cour fédérale à annuler une décision. 

{{% button href="/12-things-that-have-helped" %}}Découvrir 12 choses qui ont aidé{{% /button %}}
{{% /gears %}}
Prenez courage
----------


Gary n’est pas une vraie personne. Nous l’avons inventé pour vous aider à vous préparer à faire face aux pires raisonnements de la Commission du statut de réfugié. Mais prenez courage. Certains membres de la Commission sont beaucoup comme Gary. D’autres sont un peu comme Gary. **Bon nombre d’entre eux ne pensent pas comme Gary du tout.** Gardez en tête que la Commission du statut de réfugié accorde l’asile à la plupart des demandeurs.

Nous n’avons pas créé le personnage de Gary d’après un membre présent ou passé de la Commission du nom de Gary.