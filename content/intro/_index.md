---
title: Meet Gary
menuTitle: Introduction
weight: 10

---

<span class="img-center" margin-bottom="10px"><img class="inline" src="/images/GaryBear-face-basic.png" /></span>
<br/>
Gary has an important job. He is a member of the Refugee Board. He decides which refugee claimants need Canada’s protection.

Gary works very hard and he wants to do his job well. **He would never want to send a refugee home to danger.**

<span class="img-center"><img class="inline" src="/images/GaryBear-face-suspicious.png" /></span>
<br/>
But Gary worries. **What worries him most is that a liar might trick him.** Above all, Gary wants to make sure that a person who is just pretending to be a refugee does not get to stay in Canada.

Gary is often suspicious of the stories that he hears from refugee claimants. **Many of his suspicions come from his Six Big Ideas about how the world works.**


How to use this website
-----------------------

**This website does not give legal advice.** It does not tell you what to do or what to say at your refugee hearing. But the information on this site could be useful for you to think about, and to discuss with your legal representative, if you have one.

This website assumes that you know the basics about how the Canadian refugee claim process works.

{{% button href="/other-sources" %}}Learn more about the refugee claim process{{% /button %}}

<br />
On this website, you will meet **more than 50 claimants**. Their stories are made up. But they are based on the experiences of many real people who have come through the Canadian refugee system. These stories will help you to understand how Gary thinks.


{{% lightbulb %}}
Gary's Six Big Ideas
--------------------

You will learn about Gary’s [**Six Big Ideas**](/6-big-ideas). How does he see the world? What makes him suspicious and why?

{{% button href="/6-big-ideas" %}}Learn about Gary's Six Big Ideas{{% /button %}}

{{% /lightbulb %}}

{{% gears %}}
12 Things That Have Helped
--------------------------

When Gary thinks that a claimant is lying, sometimes nothing will change his mind. But sometimes claimants can help him to understand that they are telling the truth. Here you will learn about [**twelve things that claimants have said or done that have helped**](/12-things-that-have-helped) to calm Gary's suspicions. Or, if they did not persuade Gary, they may have helped the Refugee Appeal Division or the Federal Court to overturn his decision. 

{{% button href="/12-things-that-have-helped" %}}Learn about 12 Things that have helped{{% /button %}}
{{% /gears %}}
Take Heart
----------


Gary is not a real person. We made him up to help you to prepare for some of the Refugee Board's worst reasoning. But you have every reason to hope for the best. Some members of the Board are a lot like Gary. Some members are only a little like Gary. **Many do not think like Gary at all.** Keep in mind that the Refugee Board gives protection to most of the claimants who ask for it.

We did not base Gary on any past or present Board members named Gary.