---
title: Admitting a lie
weight: 110

---

{{% gears %}}
Sometimes claimants try to deceive the Board. But then they change their minds. They decide to **come forward of their own free will** at the start of their hearing. They admit that they have lied and try to explain why they did it.
{{% /gears %}}

{{% exclamation-highlight %}}
In such cases, Gary **is not always sympathetic**. Sometimes he concludes that the claimant is a dishonest person and cannot be trusted. Sometimes he may even think that the claimant is only admitting a lie to try to win his sympathy. But other times, this very honest act does help to convince Gary that the claimant is an honest person. He believes them when they explain they made a bad mistake because they were desperate and afraid.
{{% /exclamation-highlight %}}

{{% story name="Ana" href="/6-big-ideas/once-a-liar" /%}}, {{% story name="Tariq" href="/6-big-ideas/once-a-liar" /%}}, and {{% story name="Monique" href="/6-big-ideas/once-a-liar" /%}} tried to deceive the Board. They could have admitted this to Gary at the start of their hearings. The outcome could not have been worse than what happened when Gary found out about it himself. If they admitted that they had lied, Gary might have been sympathetic. They would at least have had the chanced to explain why they did it: because they were terrified of losing their cases and being sent home.

{{< go-on "/6-big-ideas/perfect-match" >}}