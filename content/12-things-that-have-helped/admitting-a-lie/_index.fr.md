---
title: Admettre un mensonge
weight: 110

---

{{% gears %}}
Parfois, les demandeurs essaient de mentir à la Commission. Mais ils changent d’idée. Ils décident **d’avouer de leur propre gré** au début de l’audience. Ils avouent avoir menti et ils font de leur mieux pour expliquer ce qui les a poussés à mentir. 
{{% /gears %}}

{{% exclamation-highlight %}}
Dans ces cas, Gary **n’est pas toujours compatissant**. Parfois, il en conclut que la personne est malhonnête et qu’on ne peut lui faire confiance. Quelquefois, il a l’impression que la personne avoue son mensonge dans le but de susciter plus de compassion. Mais parfois, ce geste très honnête aide à convaincre Gary que la personne est honnête. Il la croit quand elle explique qu’elle a fait cette erreur parce qu’elle était désespérée ou qu’elle avait peur.
{{% /exclamation-highlight %}}

{{% story name="Ana" href="/6-big-ideas/once-a-liar" /%}}, {{% story name="Tariq" href="/6-big-ideas/once-a-liar" /%}} et {{% story name="Monique" href="/6-big-ideas/once-a-liar" /%}} ont essayé de mentir à la Commission. Ils auraient pu avouer au début de l’audience. Ça aurait sans doute été moins pire que lorsque Gary s’en est rendu compte tout seul. S’ils avaient avoué leur mensonge, Gary aurait peut-être eu plus de compassion. Ils auraient au moins eu la chance d’expliquer qu’ils étaient terrifiés à l’idée d’un refus et de devoir retourner dans leur pays. 

{{< go-on "/6-big-ideas/perfect-match" >}}