---
title: 承认说谎
weight: 110

---

{{% gears %}}
有些时候，申请人试图欺骗委员会，但是之后他们改变了主意。在他们的听证会一开始，他们决定**自愿坦白**。他们承认他们自己以前说了谎，也尽量解释他们为什么那么做。
{{% /gears %}}

{{% exclamation-highlight %}}
在这些情况下，Gary**不是每次都有同情心的**。有些时候，他的结论是申请人是一个不诚实的人，不可以相信。有些时候，他还会认为申请人承认说谎只是为了获取他的同情。但是在另一些时候，这个非常诚实的举动，可以帮助说服Gary，申请人是一个诚实的人。他相信他们的解释，他们犯了一个错误，是因为他们急迫，他们害怕。
{{% /exclamation-highlight %}}

{{% story name="Ana" href="/6-big-ideas/once-a-liar" /%}}, {{% story name="Tariq" href="/6-big-ideas/once-a-liar" /%}}，和 {{% story name="Monique" href="/6-big-ideas/once-a-liar" /%}} 试图欺骗委员会。他们是可以在他们的听证会开始的时候向Gary承认的。那样的结果不会比Gary自己发现了这件事更坏。如果他们承认他们自己说谎了，Gary可能会同情他们的。至少他们会有一个机会解释他们为什么这么做：因为他们非常害怕申请会失败，会被送回家。

{{< go-on "/6-big-ideas/perfect-match" >}}
