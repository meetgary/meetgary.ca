---
title: Admitir una mentira
weight: 110

---

{{% gears %}}
A veces las personas solicitantes intentan engañar a la Comisión. Pero luego pueden cambiar de parecer y decidir **manifestarse por su propia y libre voluntad** al principio de su audiencia. Admiten que han mentido e intentan explicar por qué lo hicieron.
{{% /gears %}}

{{% exclamation-highlight %}}
En dichos casos, Gary **no siempre se muestra compasivo**. A veces concluye que la persona solicitante es deshonesta y que no se puede confiar en ella. A veces llega a pensar que la persona solicitante está admitiendo que mintió solamente para ganarse su compasión. Pero otras veces, ese acto honesto sí ayuda a convencer a Gary de que la persona solicitante es honesta. Gary les cree cuando explican que cometieron un grave error porque sentían desespero y temor.
{{% /exclamation-highlight %}}

{{% story name="Ana" href="/6-big-ideas/once-a-liar" /%}}, {{% story name="Tariq" href="/6-big-ideas/once-a-liar" /%}}, y {{% story name="Monique" href="/6-big-ideas/once-a-liar" /%}} intentaron engañar a la Comisión. Podrían haberlo admitido frente a Gary al principio de sus audiencias. El resultado no podría haber sido peor que lo que pasó cuando Gary se enteró por su propia cuenta. Si hubiesen admitido que habían mentido, quizás Gary se habría mostrado compasivo. Al menos habrían tenido la oportunidad de explicar por qué lo hicieron: porque sentían terror de perder el caso y que les regresaran a su país.

{{< go-on "/6-big-ideas/perfect-match" >}}