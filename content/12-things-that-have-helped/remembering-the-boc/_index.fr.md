---
title: Se souvenir de l’histoire sur le formulaire FDA
weight: 10

---

{{% gears %}}

Gary trouve ça louche quand l’histoire d’un demandeur change. Pourtant les histoires changent souvent quand on essaie de se souvenir de la même expérience à deux moments différents. Les demandeurs risquent de raconter la même histoire de façon différente, ou ils peuvent se souvenir de détails différents. Même si les demandeurs **se souviennent bien de ce qui est arrivé**, Gary pourrait ne pas les croire. 

Gary a tendance à croire les histoires cohérentes et uniformes. C’est pourquoi les demandeurs ont intérêt à **se souvenir de ce qu’ils ont écrit dans leur formulaire de fondement de la demande d’asile (FDA)**.
{{% /gears %}}

{{% story name="Boniface" href="/6-big-ideas/memory" /%}} ne s’est jamais souvenu précisément de l’ordre des événements quand le policier l’a frappé. Dans son formulaire FDA, il a écrit : « Un des policiers m'a frappé au visage, puis a craché au sol avant de m’insulter. » Il aurait tout aussi bien pu écrire que le policier l’avait insulté avant de cracher au sol et de le frapper au visage. Boniface se souvient très bien que ces trois choses sont arrivées, mais il ne sait pas dans quel ordre. Avant l’audience, Boniface aurait pu lire son FDA de près pour s’assurer de décrire l’événement de la même manière à Gary.

{{< go-back "/6-big-ideas/memory#Boniface" >}}

Gary était contrarié parce que {{% story name="Annie" href="/6-big-ideas/memory" /%}} n’arrivait pas à se souvenir de ce qu’elle avait écrit dans son formulaire FDA. Une de ses amies de l’église l’avait aidée à rédiger son histoire. C’est cette amie qui avait écrit « une journée et une nuit ». Sur le coup, Annie n’avait pas vu de problème avec ces mots. Même si Annie avait relu son FDA avant l’audience, elle n’aurait peut-être pas remarqué ce qui clochait. Mais elle aurait au moins reconnu ces mots au moment où Gary avait posé des questions. Elle aurait pu éviter de contrarier Gary en disant qu’elle n’avait jamais écrit cela.

Comme {{% story name="Graciela" href="/6-big-ideas/memory" /%}} ne parlait ni français ni anglais, un bénévole de la communauté l’a aidée à rédiger son histoire dans le formulaire FDA. Il l’a relu pour elle à voix haute dans sa langue, et tout lui semblait parfait. Mais il ne lui a jamais donné de copie du texte dans sa langue. Bien qu’elle avait appris un peu d’anglais avant l’audience, elle n’avait pas relu l’histoire avant l’audience. Si elle l’avait relu, elle aurait peut-être remarqué l’erreur. Elle avait dit au bénévole qu’elle avait vu la fusillade « en se rendant à l’école », mais il a écrit « en revenant de l’école ». Si elle avait remarqué cette erreur, elle l’aurait corrigée.

{{% exclamation-highlight %}}
Vous pouvez corriger les erreurs sur le formulaire FDA ou y ajouter de l’information manquante. Voyez comment faire :

* [Comment **modifier le formulaire FDA**](https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes)
{{% /exclamation-highlight %}}

{{< go-back "/6-big-ideas/memory#Graciela" >}}

L’audience d’{{% story name="Yvonne" href="/6-big-ideas/memory" /%}} a eu lieu près de deux ans après sa demande d’asile. Elle ne se souvenait plus très bien des hommes dans la camionnette. C’était une expérience qu’elle essayait d’oublier. Elle aurait pu lire son formulaire FDA avant l’audience pour se rappeler des détails.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}