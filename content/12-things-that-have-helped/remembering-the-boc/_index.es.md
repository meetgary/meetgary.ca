---
title: Recordar la historia de Bases de la solicitud de asilo
weight: 10

---

{{% gears %}}

Gary sospecha cuando la historia de una persona solicitante cambia. Aún así, las historias cambian a menudo cuando las personas intentan recordar la misma experiencia en dos ocasiones distintas. Es posible que cuenten la historia de otra manera, o que recuerden detalles distintos.
Incluso cuando una persona solicitante **recuerda bien lo que le pasó**, es posible que Gary no le crea.

Gary es más susceptible de creer una historia coherente. Es por eso que ayuda si la persona solicitante **recuerda lo que escribió en su formulario de Bases de la solicitud (Basis of Claim, o BOC)**.
{{% /gears %}}

{{% story name="Boniface" href="/6-big-ideas/memory" /%}} nunca recordó claramente el orden en que sucedieron las cosas cuando lo golpeó el policía. En su BOC, Boniface escribió que el oficial “lo golpeó en la cara, luego escupió en el suelo y lo insultó”. Fácilmente podía haber escrito que el oficial “lo insultó, luego escupió en el suelo y lo golpeó en la cara”. Boniface recuerda con seguridad que las tres cosas sucedieron, pero no en qué orden. Antes de la audiencia, Boniface podría haber leído su historia de BOC detenidamente a fin de asegurarse que iba a poder describirle el acontecimiento a Gary de la misma forma.

{{< go-back "/6-big-ideas/memory#Boniface" >}}

Gary se sintió frustrado al ver que {{% story name="Annie" href="/6-big-ideas/memory" /%}} no lograba recordar lo que tenía escrito en su propio formulario de BOC. Pero una amiga de la iglesia la había ayudado a escribir su declaración. Fue esa amiga la que escribió “un día y una noche”. En aquel momento Annie no vio ningún problema con la frase. Si Annie hubiese leído su historia de BOC otra vez antes de la audiencia, tal vez tampoco hubiese visto ningún problema. Pero al menos hubiese reconocido la frase cuando Gary preguntó al respecto. De esa manera habría podido evitar que Gary se sintiera frustrado con su insistencia de que no la había escrito.

Como {{% story name="Graciela" href="/6-big-ideas/memory" /%}} no hablaba inglés, un voluntario de su comunidad la ayudó a escribir su historia de BOC. Se lo leyó en su propio idioma y a ella le pareció bien. Pero nunca le dio una copia en su propio idioma. Aunque Graciela ya había aprendido algo de inglés cuando llegó el día de su audiencia, no repasó la historia de nuevo antes de su audiencia. Si lo hubiese hecho, tal vez habría encontrado el error. Ella le dijo al voluntario que había visto el tiroteo “de camino a clases” pero él escribió “cuando regresaba de clases a mi casa”. Si ella se hubiese dado cuenta de ese error, lo hubiese corregido.

{{% exclamation-highlight %}}
Se permite corregir errores en cualquier sección del formulario BOC o añadir información que falta. Para aprender cómo:

* [Aprender más sobre  **el proceso para cambiar el formulario BOC**](https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes)
{{% /exclamation-highlight %}}

{{< go-back "/6-big-ideas/memory#Graciela" >}}

La audiencia de {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} tuvo lugar casi dos años después de solicitar asilo. Sus recuerdos sobre los hombres de la camioneta se habían empezado a borrar. Intentaba no pensar mucho en esa experiencia. Leer su historia de BOC antes de la audiencia la habría ayudado a recordar los detalles.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}