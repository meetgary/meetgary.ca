---
title: Remembering the Basis of Claim story
weight: 10

---

{{% gears %}}

Gary is suspicious whenever a claimant’s story changes. Yet people’s stories often change when they try to remember the same experience at two different times. They may tell the same story in a different way, or they may remember different details. Even if claimants **remember well what happened to them**, Gary may not believe them. 

Gary is more likely to believe a story that is consistent. That is why it helps if claimants **remember what they wrote in their Basis of Claim (BOC) form**.
{{% /gears %}}

{{% story name="Boniface" href="/6-big-ideas/memory" /%}} never had a clear memory of the order of things when he was hit by the police officer. He wrote in the BOC that the officer “him hit him in the face, then spat on the ground and insulted him.” He could just as easily have written that the officer “insulted him, then spat on the ground and hit him in the face.” Boniface remembers for sure that all three things happened, but not the order. Before the hearing, Boniface could have read his BOC story carefully to make sure that he would described this event to Gary in the same way.

{{< go-back "/6-big-ideas/memory#Boniface" >}}

Gary was frustrated because {{% story name="Annie" href="/6-big-ideas/memory" /%}} could not remember what was written on her own BOC form. But a friend from her church had helped her to write her story. “A day and a night” were this woman’s words. At the time Annie had not seen any problem with this phrase. If Annie had reread her BOC story before the hearing, she still might not have seen a problem. But she would at least have recognized the phrase when Gary asked about it. She could have avoided making him frustrated by insisting that she had not written it.

Since {{% story name="Graciela" href="/6-big-ideas/memory" /%}} spoke no English, a community volunteer helped her to write her BOC story. He read it back to her in her own language and it sounded fine to her. But he never gave her a copy in her own language. Although she had learned some English by the time of her hearing, she did not look over the story again before her hearing. If she had, she might have caught the mistake. She told the worker that she saw the shooting “on my way to school” but he wrote “on my way home from school.” If she had caught this mistake, she would have corrected it.

{{% exclamation-highlight %}}
You can correct a mistake anywhere on the BOC form or add information that is missing. Learn how:

* [Learn about **how to amend the BOC form**](https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes)
{{% /exclamation-highlight %}}

{{< go-back "/6-big-ideas/memory#Graciela" >}}

{{% story name="Yvonne" href="/6-big-ideas/memory" /%}}'s hearing took place almost two years after she made her claim. Her memory of the men in the van had started to fade. It was an experience that she tried hard not to think about. Reading over her BOC story before the hearing would have reminded her of the details.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}