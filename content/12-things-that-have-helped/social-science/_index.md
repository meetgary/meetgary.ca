---
title: Using social science evidence
weight: 120

---

{{% gears %}}
Researchers in the social sciences have pointed out [**problems with Gary’s Big Idea about memory**](#research-about-the-limits-of-memory) and [**problems with Gary’s Big Idea about caution**](#research-about-how-people-respond-to-danger). Below you will find links to **articles that summarize this research**.

These articles can be very useful in a hearing. A claimant's representative can use the research to challenge Gary’s thinking. Challenging Gary directly is a lot harder for an unrepresented claimant. But when claimants send these articles to the Board, they become **part of the claimant’s evidence**. If Gary rejects the claim, and the claimant fights this decision, these articles will be part of the package that goes to the Refugee Appeal Division and possibly on to the Federal Court. At the Refugee Appeal Division, or in Federal Court, these articles may **help get Gary’s decision overturned**. This is because the Appeal member or the Court judge can use them to point out what is wrong with Gary’s decision.
{{% /gears %}}

{{% exclamation-highlight %}}
If you want these articles to be part of the evidence in your claim, you must print them out and send them to the Board **at least 10 days before your hearing**. {{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}Learn more about submitting documents to the Board{{% /button %}} 
{{% /exclamation-highlight %}}

Research about the limits of memory
-----------------------------------

Decades of research has shown that Gary’s Big Idea about memory is wrong. Memories are not like video recordings.

{{% button href="/files/Memory_paper.pdf" %}}Read more about the limits of memory{{% /button %}}

Memories are incomplete, especially for upsetting events. Memories can change, whether the event is upsetting or not. And often how we remember an event will depend on the kinds of questions that we are asked about it. 

Here are some points that the research makes very clear:

* People very often have poor memory for the **timing, sequence, frequency, or duration of events**. Like {{% story name="Boniface" href="/6-big-ideas/memory" /%}}, {{% story name="Mustafa" href="/6-big-ideas/memory" /%}}, {{% story name="Lupe" href="/6-big-ideas/memory" /%}} and {{% story name="Jean-René" href="/6-big-ideas/memory" /%}}, we can remember that events happened, without remembering well when they happened, in what order, for how long or how often.
* Like {{% story name="Je-Tsun" href="/6-big-ideas/memory" /%}}, we very often cannot remember well or describe accurately **objects that we see everyday**.
* Like {{% story name="Shani" href="/6-big-ideas/memory" /%}}, we often have trouble remembering **names**.


* When we try to remember similar events, we often lose the details of each one. Like {{% story name="Bijan" href="/6-big-ideas/memory" /%}} we recall the essence of what happened. But our memories of the details of the events **become blurred together**.

* When we recall an event, we will not remember **things that we did not notice** at the time. Gary was suspicious because {{% story name="Marco" href="/6-big-ideas/memory" /%}} and {{% story name="Asmaan" href="/6-big-ideas/memory" /%}} could not remember details like the make of a truck or the colour of soldiers’ uniforms. Marco would have seen the truck, and Asmaan would have seen the uniforms. But they have no memory of them because they were focussed on other things, like the guns that the men were carrying.

* Over time, our memories **fade and change**. They sometimes becoming confused with memories of other events. This can happen even for upsetting memories like {{% story name="Yvonne" href="/6-big-ideas/memory" /%}}’s memory of the men who threatened her.

{{< go-back "/6-big-ideas/memory" >}}
{{< go-on "/6-big-ideas/caution" >}}

Research about how people respond to danger
-------------------------------------------

Decades of research has also uncovered problems with Gary’s Big Idea that people avoid danger at all costs.

{{% button href="/files/Cameron_2008-Risk_Theory_and_Subjective_Fear.pdf" %}}Read more about how people respond to dangerous situations{{% /button %}}

There are all kinds of reasons why people overlook or downplay dangers. There are all kinds of reasons why we decide to take risks. Sometimes we value something more than our own safety. Here are some more points that the research makes clear:

* Like {{% story name="Sonia" href="/6-big-ideas/caution" /%}}, we can get used to danger **when we live with it every day**. The more familiar a risk is, the easier it is for us to push it to the backs of our minds.

* Like {{% story name="Désirée" href="/6-big-ideas/caution" /%}}, risking her life to return to her children, we often convince ourselves that it will be safe enough **when we want to do something very badly**.

* When people like {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} live for a long time with abuse, they can start to feel that they have **no control over their own lives**. They may come to believe that there is simply nothing that they can do to keep themselves safe.

* Gary has been trained to rely on experts. He thinks that if {{% story name="Emily" href="/6-big-ideas/caution" /%}} was really in danger, she would have paid a lawyer for advice. But that is often not what people do in dangerous situations. Many people rely on **what they hear from friends, family and the people they know and trust**.

* **Safety does not always come first.** People will put their lives at risk to give themselves a better chance at starting over in a new country. They will do it to support a cause that they believe in. They will do it to preserve their dignity, integrity, or reputation. They will even do it for a chance at love and happiness. People have their own priorities, like we saw in the stories of {{% story name="Manuel" href="/6-big-ideas/caution" /%}}, {{% story name="Purabi" href="/6-big-ideas/caution" /%}}, {{% story name="Andrés" href="/6-big-ideas/caution" /%}}, {{% story name="Lisa" href="/6-big-ideas/caution" /%}}, {{% story name="Mohammed" href="/6-big-ideas/caution" /%}}, {{% story name="Constance" href="/6-big-ideas/caution" /%}} and the woman who helped {{% story name="Moses" href="/6-big-ideas/caution" /%}}.

{{< go-back "/6-big-ideas/caution" >}}
{{< go-on "/6-big-ideas/once-a-liar" >}}