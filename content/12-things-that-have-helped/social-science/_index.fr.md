---
title: Les sciences sociales comme preuve
weight: 120

---

{{% gears %}}
Les chercheurs en sciences sociales ont mis en lumière [**les problèmes associés aux Grandes Idées de Gary au sujet de la mémoire**](#la-recherche-sur-les-limites-de-la-mémoire) et [**les problèmes associés aux Grandes Idées de Gary sur la prudence**](#les-études-sur-notre-réponse-face-au-danger
). Ci-après vous trouverez des liens aux **articles qui résument cette recherche**.

Ces articles peuvent être très utiles pour l’audience. Le représentant du demandeur peut se servir de la recherche pour contester le raisonnement de Gary. Il est beaucoup plus difficile pour un demandeur qui n’est pas représenté de contester le raisonnement de Gary directement. Mais lorsqu’un demandeur fait parvenir ces articles à la Commission, ceux-ci **sont inclus dans les preuves du demandeur**. Si Gary rejette une demande et le demandeur conteste la décision, ces articles feront partie du dossier envoyé à la Section d’appel des réfugiés et possiblement à la Cour fédérale. À la Section d’appel des réfugiés, ou à la Cour fédérale, ces articles peuvent **contribuer à infirmer la décision de Gary**. En effet, le commissaire de la Section d’appel, ou le juge de la Cour, pourrait s’appuyer sur ces articles pour révéler ce qui ne va pas dans la décision de Gary.
{{% /gears %}}

{{% exclamation-highlight %}}
Si vous voulez inclure ces articles dans les preuves pour votre demande, vous devez les imprimer et les envoyer à la Commission **au moins 10 jours avant l’audience**.  {{% button href="https://irb-cisr.gc.ca/fr/presenter-demande-asile/Pages/index3.aspx" %}}Pour savoir comment soumettre des documents à la Commission, c'est par ici.{{% /button %}} 

{{% /exclamation-highlight %}}

La recherche sur les limites de la mémoire
-----------------------------------

Depuis quelques décennies, les études révèlent que la Grande Idée de Gary au sujet de la mémoire est erronée. Les souvenirs ne sont pas comme des enregistrements vidéos. 

{{% button href="/files/Memory_paper.pdf" %}}En apprendre plus sur les limites de la mémoire {{% /button %}}

Les souvenirs sont incomplets, surtout pour les événements troublants. Les souvenirs peuvent changer, que l’événement soit troublant ou non. Et souvent, notre façon de nous rappeler un événement dépend du genre de questions posées. 

Voici certains éléments que les études révèlent très clairement :

* Les personnes ont souvent une faible mémoire en ce qui a trait **à la chronologie, au calendrier, à la fréquence ou à la durée des événements**. Tout comme {{% story name="Boniface" href="/6-big-ideas/memory" /%}}, {{% story name="Mustafa" href="/6-big-ideas/memory" /%}}, {{% story name="Lupe" href="/6-big-ideas/memory" /%}} et {{% story name="Jean-René" href="/6-big-ideas/memory" /%}}, nous nous souvenons des événements, sans nous rappeler des moments exacts, de l’ordre, de la durée ou de la fréquence de ceux-ci.
* Tout comme {{% story name="Je-Tsun" href="/6-big-ideas/memory" /%}}, nous avons souvent du mal à nous souvenir ou à décrire de façon précise **les objets que nous voyons tous les jours**.
* Tout comme {{% story name="Shani" href="/6-big-ideas/memory" /%}}, nous avons souvent du mal à nous souvenir des **prénoms**.


* Quand nous essayons de nous rappeler d’événements semblables, il arrive que les détails de chacun nous échappent. Tout comme {{% story name="Bijan" href="/6-big-ideas/memory" /%}}, nous nous souvenons de l’essentiel de ce qui est arrivé. Mais notre mémoire pour les détails des événements **est floue**.

* Quand nous nous remémorons un événement, nous ne pouvons nous souvenir **des choses que nous n’avons pas remarquées** sur le coup. Gary a des doutes parce que {{% story name="Marco" href="/6-big-ideas/memory" /%}} et {{% story name="Asmaan" href="/6-big-ideas/memory" /%}} ne se souviennent pas des détails, comme la marque du véhicule ou la couleur de l’uniforme des soldats. Marco a vu le camion, et Asmaan a vu les uniformes. Mais ils ne les ont pas en mémoire parce que sur le coup, ils étaient concentrés sur autre chose, comme les armes à feu de ces hommes peut-être.

* Avec le temps, nos souvenirs **s’estompent et changent**. Ils s’entremêlent parfois avec des souvenirs d’autres événements. C’est aussi le cas pour les souvenirs troublants, comme pour le souvenir qu’a {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} des hommes qui l’ont menacée.

{{< go-back "/6-big-ideas/memory" >}}
{{< go-on "/6-big-ideas/caution" >}}

Les études sur notre réponse face au danger
-------------------------------------------

Les décennies de recherche ont également révélé des problèmes quant à la Grande Idée de Gary qui suppose que les gens évitent le danger à tout prix. 

{{% button href="/files/Cameron_2008-Risk_Theory_and_Subjective_Fear.pdf" %}}Lire davantage sur la réaction face aux situations dangereuses{{% /button %}}

Les gens ignorent ou minimisent les dangers pour toutes sortes de raisons. De même, nous décidons de prendre des risques pour toutes sortes de raisons. Il nous arrive d’accorder plus d’importance à certaines choses qu’à notre sécurité. Voici d’autres aspects que la recherche révèle :

* Comme {{% story name="Sonia" href="/6-big-ideas/caution" /%}}, nous pouvons nous habituer au danger **quand il fait partie du quotidien**. Plus le risque est familier, plus il est facile d’en faire abstraction.

* Comme {{% story name="Désirée" href="/6-big-ideas/caution" /%}}, qui a risqué sa vie pour rejoindre ses enfants, nous nous convainquons que c’est assez sécuritaire **quand nous voulons vraiment quelque chose**.

* Les personnes comme {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} qui vivent dans l’abus pendant longtemps peuvent avoir l’impression qu’elles **n’ont aucun contrôle sur leur vie**. Elles peuvent croire qu’il n’y a rien à faire pour se protéger.

* En raison de sa formation, Gary se fie aux experts. Il pense que si {{% story name="Emily" href="/6-big-ideas/caution" /%}} était vraiment en danger, elle aurait payé un avocat pour ses conseils. Mais c’est souvent ce que les personnes en danger ne font pas. De nombreuses personnes s’appuient sur **ce que disent leurs amis, leur famille et les personnes qu’elles connaissent et auxquelles elles font confiance**.

* **La sécurité n’est pas toujours la priorité.** Certaines personnes mettront leur vie en danger pour avoir la chance de repartir à zéro dans un autre pays. Elles le font pour soutenir une cause qui leur tient à cœur. Elles le font pour préserver leur dignité, leur intégrité ou leur réputation. Elles le font même pour avoir la chance de trouver l’amour et le bonheur. Tout le monde a ses propres priorités, comme nous l’avons vu dans les histoires de {{% story name="Manuel" href="/6-big-ideas/caution" /%}}, {{% story name="Purabi" href="/6-big-ideas/caution" /%}}, {{% story name="Andrés" href="/6-big-ideas/caution" /%}}, {{% story name="Lisa" href="/6-big-ideas/caution" /%}}, {{% story name="Mohammed" href="/6-big-ideas/caution" /%}}, {{% story name="Constance" href="/6-big-ideas/caution" /%}} et la femme qui a aidé {{% story name="Moses" href="/6-big-ideas/caution" /%}}.

{{< go-back "/6-big-ideas/caution" >}}
{{< go-on "/6-big-ideas/once-a-liar" >}}