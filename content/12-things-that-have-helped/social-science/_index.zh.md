---
title: 运用社会科学的证据
weight: 120

---

{{% gears %}}
社会科学的研究人员指出了 [**Gary关于记忆的“大的看法”的问题**](#关于记忆的局限性的研究) 和 [**Gary的关于警惕性的“大的看法”的问题**](#关于人们如何应对危险的研究). 下方你可以找到**总结这个研究的一些文章**的链接。

这些文章在听证会上可以很有用。申请人的法律代表可以使用这些研究挑战Gary的想法。直接挑战Gary，对于一个没有法律代表的申请人来说会困难得多。但是，如果申请人把这些文章送交委员会，那它们就成为**申请人的证据的一部分**。如果Gary拒绝了申请，申请人要反对这个决定，这些文件将作为证据材料的一部分，送交难民上诉委员会，还有可能到联邦法院。在难民上诉委员会或联邦法院，这些文章可能**对撤销Gary的决定有帮助**。这是因为，上诉委员会委员或联邦法院法官可以用它们来指出Gary的决定为什么是错的。
{{% /gears %}}

{{% exclamation-highlight %}}
如果你想要把这些文章作为你申请的证据的一部分，你必须把它们打印出来，**最晚在你的听证会十天之前**，呈交委员会。

{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}了解更多关于呈交文件给委员会的内容{{% /button %}} 


{{% /exclamation-highlight %}}

关于记忆的局限性的研究
-----------------------------------

数十年的研究显示，Gary关于记忆的“大的看法”是错误的。记忆不是像录像那样的。

{{% button href="/files/Memory_paper.pdf" %}}阅读更多有关记忆的局限性的内容{{% /button %}}

记忆是不完整的，尤其是令人难过的记忆。记忆是会改变的，不管事件令人难过与否 。经常，我们如何回忆一个事件是由向我们问了哪些有关它的问题而决定的。

这里是研究中讲得很清楚的一些观点:

* 人们经常不能够记好**时间，顺序，频率或事件的长短**。就像 {{% story name="Boniface" href="/6-big-ideas/memory" /%}}, {{% story name="Mustafa" href="/6-big-ideas/memory" /%}}, {{% story name="Lupe" href="/6-big-ideas/memory" /%}} 和 {{% story name="Jean-René" href="/6-big-ideas/memory" /%}}，我们能够记得这件事情发生过，但是不能够很好地记得它们是什么时间发生的，什么顺序，持续多久，或者有多经常。
* 就像{{% story name="Je-Tsun" href="/6-big-ideas/memory" /%}}，我们经常记不好，或者不能够准确地描述**我们每天都见到的东西**。
* 就像 {{% story name="Shani" href="/6-big-ideas/memory" /%}}，我们经常记**名字**有困难。


* 当我们试图回忆类似的一些事件，我们经常忘记具体每一个事件的细节。就像{{% story name="Bijan" href="/6-big-ideas/memory" /%}}，我们可以记起发生的事情的实质，但是我们把这些事件的具体细节的记忆**模糊在了一起**。

* 当我们回忆一个事件，我们不会记得当时**我们没有注意到的事物**。Gary产生了怀疑， 是因为 {{% story name="Marco" href="/6-big-ideas/memory" /%}} 和 {{% story name="Asmaan" href="/6-big-ideas/memory" /%}} 没有记得一些细节，比如说卡车的型号，或是士兵制服的颜色。Marco看见过那辆卡车，Asmaan看见过制服。但是，他们没有这些事物的记忆，是因为他们当时的注意力在别的东西上，比如说那些男人拿着的枪。

* 经过时间，我们的记忆**会淡漠，会改变**。有些时候，它们会和其他事件的记忆混在一起。即使是{{% story name="Yvonne" href="/6-big-ideas/memory" /%}}被那些男人恐吓的难过的回忆，也可能发生这些情况。

{{< go-back "/6-big-ideas/memory" >}}
{{< go-on "/6-big-ideas/caution" >}}

关于人们如何应对危险的研究
-------------------------------------------

数十年的研究也发现了，Gary的人们会不惜一切避免危险的“大的看法”存在的问题。

{{% button href="/files/Cameron_2008-Risk_Theory_and_Subjective_Fear.pdf" %}}阅读更多有关人们如何应对危险的情况的内容{{% /button %}}

人们为什么会无视或者轻视危险，有各种各样的原因。我们为什么会决定冒险，有各种各样的原因。有些时候，我们认为某些东西比我们自己的安全更有价值。
这里有研究中讲得很清楚的更多观点:

* 就像 {{% story name="Sonia" href="/6-big-ideas/caution" /%}}，如果我们**每天都生活在危险**里，我们就会习惯它。危险对我们来说越是熟悉，就越容易让我们把它置之脑后。

* 就像 {{% story name="Désirée" href="/6-big-ideas/caution" /%}}，冒着生命危险去回到她的孩子们身边，**如果我们非常想做一件事的时候**，我们经常会说服自己它是够安全的。

* 像 {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} 一样在折磨中生活了很长时间的人，他们会开始觉得**他们对自己的生活无法掌控**。他们可能会产生这样的想法，自己做的任何事都不能保证自己的安全。

* Gary所受过的训练，令他会依靠专家。他认为，如果 {{% story name="Emily" href="/6-big-ideas/caution" /%}} 真的是有危险，她会付费获取律师的建议。但是，在危险的情况下，人们经常不这么做。很多人会依靠**他们从朋友，家人，他们所认识和信任的人那里听到的信息**。

* **安全不是永远在第一位的**。人们会冒生命危险，给他们自己一个更好的在新的国家重新开始的机会。会冒生命危险，去支持一个他们相信的事业。会冒生命危险，去维护他们自己的尊严，气节或名誉。他们甚至愿意为了爱情和幸福的机会，冒生命危险。人们做事有他们自己的主次，就像我们在 {{% story name="Manuel" href="/6-big-ideas/caution" /%}}, {{% story name="Purabi" href="/6-big-ideas/caution" /%}}, {{% story name="Andrés" href="/6-big-ideas/caution" /%}}, {{% story name="Lisa" href="/6-big-ideas/caution" /%}}, {{% story name="Mohammed" href="/6-big-ideas/caution" /%}}, {{% story name="Constance" href="/6-big-ideas/caution" /%}} 和那个帮助{{% story name="Moses" href="/6-big-ideas/caution" /%}}的女人的这些故事里看到的。

{{< go-back "/6-big-ideas/caution" >}}
{{< go-on "/6-big-ideas/once-a-liar" >}}