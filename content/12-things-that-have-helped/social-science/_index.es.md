---
title: Usar pruebas sociológicas
weight: 120

---

{{% gears %}}
Estudios en sociología han señalado [**problemas con la Gran Idea de Gary sobre la memoria**](#estudios-sobre-los-límites-de-la-memoria) y [**problemas con la Gran Idea de Gary sobre la precaución**](#estudios-sobre-cómo-las-personas-responden-al-peligro). A continuación, encontrará enlaces a **artículos que resumen dichos estudios**.

Los siguientes artículos pueden ser de gran utilidad en una audiencia. Quien represente a la persona solicitante puede hacer uso de estos estudios para cuestionar el razonamiento de Gary. Cuestionar a Gary directamente es mucho más difícil para solicitantes sin representación. Pero cuando una persona solicitante envía dichos artículos a la Comisión, los mismos son **parte de las pruebas de la persona solicitante**. Si Gary rechaza la solicitud, y la persona solicitante apela la decisión, estos artículos se integran al paquete que se envía a la División de Apelaciones para Refugiados y posiblemente al Tribunal Federal. Allí, en la División de Apelaciones para Refugiados, o en el Tribunal Federal, estos artículos pueden **ayudar a que se revoque la decisión de Gary**. Esto se debe a que un miembro de la División de Apelaciones o un juez federal puede usar los artículos para señalar lo que está incorrecto en la decisión de Gary.
{{% /gears %}}

{{% exclamation-highlight %}}
Si desea integrar estos artículos como parte de las pruebas presentadas con su solicitud, debe imprimirlos y enviarlos a la Comisión **al menos 10 días antes de su audiencia**. {{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}Información sobre cómo presentar documentos a la Comisión{{% /button %}} 
{{% /exclamation-highlight %}}

Estudios sobre los límites de la memoria
-----------------------------------

Décadas de estudios han demostrado que la Gran Idea de Gary sobre la memoria es incorrecta. Los recuerdos no son como grabaciones de video.

{{% button href="/files/Memory_paper.pdf" %}}Leer más sobre los límites de la memoria{{% /button %}}

La memoria es incompleta, sobre todo cuando se trata de eventos difíciles. Nuestros recuerdos de un evento pueden cambiar, sea éste difícil o no. Y a menudo nuestra manera de recordar un evento dependerá del tipo de preguntas que nos hagan al respecto.

A continuación, se presentan algunos puntos que el estudio señala muy claramente:

* Las personas a menudo tienen mala memoria cuando se trata de **cronología, secuencia, frecuencia o duración de los eventos**. Como {{% story name="Boniface" href="/6-big-ideas/memory" /%}}, {{% story name="Mustafa" href="/6-big-ideas/memory" /%}}, {{% story name="Lupe" href="/6-big-ideas/memory" /%}} y {{% story name="Jean-René" href="/6-big-ideas/memory" /%}}, podemos recordar que sucedieron eventos sin recordar bien cuándo sucedieron, en qué orden, su duración o su frecuencia. 
* Como a {{% story name="Je-Tsun" href="/6-big-ideas/memory" /%}}, a menudo no podemos recordar bien o describir de manera exacta **los objetos que vemos todos los días**.
* Como a {{% story name="Shani" href="/6-big-ideas/memory" /%}}, a menudo nos cuesta recordar **nombres**.


* Cuando intentamos recordar eventos similares, a menudo perdemos los detalles de cada uno. Como {{% story name="Bijan" href="/6-big-ideas/memory" /%}}, recordamos la esencia de lo que sucedió. Pero nuestros recuerdos de los eventos **se confunden y se mezclan**.

* Cuando recordamos un evento, no recordaremos **las cosas que no notamos** en ese momento. Gary sospechó porque {{% story name="Marco" href="/6-big-ideas/memory" /%}} y {{% story name="Asmaan" href="/6-big-ideas/memory" /%}} no recordaban detalles tales como el modelo de la camioneta o el color de los uniformes de los soldados. Marco debió ver la camioneta, y Asmaan debió ver los uniformes. Pero no tienen memoria de ellos porque estaban concentrados en otras cosas, como las armas que llevaban los hombres.

* Con el paso del tiempo, nuestros recuerdos **se desvanecen y cambian**. A veces se confunden con recuerdos de otros eventos. Esto puede suceder incluso cuando se trata de eventos difíciles como los recuerdos de {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} de los hombres que la amenazaron.

{{< go-back "/6-big-ideas/memory" >}}
{{< go-on "/6-big-ideas/caution" >}}

Estudios sobre cómo las personas responden al peligro
-------------------------------------------

Décadas de estudios revelan igualmente problemas con la Gran Idea de Gary sobre cómo las personas evitan el peligro a toda costa.

{{% button href="/files/Cameron_2008-Risk_Theory_and_Subjective_Fear.pdf" %}}Leer más sobre cómo las personas responden a situaciones de peligro{{% /button %}}

Las personas tienen toda clase de motivos para pasar por alto o dar menos importancia a los peligros. Hay muchas razones por las que decidimos tomar riesgos. A veces hay algo que valoramos más nuestra propia seguridad. A continuación, se presentan otros puntos que el estudio señala claramente:

* Como el caso de {{% story name="Sonia" href="/6-big-ideas/caution" /%}}, es posible acostumbrarse al peligro **cuando se vive con él todos los días**. Mientras más familiar es el riesgo, más fácil será sacárselo de la cabeza.

* Como {{% story name="Désirée" href="/6-big-ideas/caution" /%}}, que arriesgó su vida al regresar a estar con sus hijos, a menudo nos convencemos de que gozaremos de suficiente seguridad **cuando hay algo que deseamos hacer a como dé lugar**.

* Cuando personas como {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} viven durante mucho tiempo en una situación de abuso, pueden empezar a sentir que **no tienen control sobre sus propias vidas**. Pueden empezar a creer que no hay nada que puedan hacer para mantenerse a salvo.

* Gary ha sido formado para confiar en especialistas. Él piensa que si {{% story name="Emily" href="/6-big-ideas/caution" /%}} hubiese estado realmente en peligro, habría contratado a un abogado para recibir asesoramiento. Pero a menudo eso no es lo que hacen las personas en situaciones de peligro. Muchas personas confían en **lo que les dicen sus amigos, familiares y personas de confianza**.

* **La seguridad no es siempre lo más importante.** Las personas ponen en peligro su vida en busca de una mejor oportunidad de empezar de cero en un nuevo país. Lo hacen para apoyar una causa en la que creen. Lo hacen para preservar su dignidad, integridad o reputación. Lo harán incluso por una oportunidad de conseguir amor y felicidad. Las personas tienen sus propias prioridades, como se ve en las historias de {{% story name="Manuel" href="/6-big-ideas/caution" /%}}, {{% story name="Purabi" href="/6-big-ideas/caution" /%}}, {{% story name="Andrés" href="/6-big-ideas/caution" /%}}, {{% story name="Lisa" href="/6-big-ideas/caution" /%}}, {{% story name="Mohammed" href="/6-big-ideas/caution" /%}}, {{% story name="Constance" href="/6-big-ideas/caution" /%}} y la mujer que ayudó a {{% story name="Moses" href="/6-big-ideas/caution" /%}}.

{{< go-back "/6-big-ideas/caution" >}}
{{< go-on "/6-big-ideas/once-a-liar" >}}