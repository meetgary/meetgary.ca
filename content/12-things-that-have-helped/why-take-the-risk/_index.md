---
title: Explaining the decision to take a risk
weight: 70

---

{{% gears %}}
Gary does not have a very strong imagination. He cannot easily imagine himself ‘standing in a claimant’s shoes’. Sometimes he thinks that a claimant’s decision to take a risk was too foolish to be believed. Often this is because he pictures the claimant facing a simple choice between safety and danger. Why would anyone choose danger?

Some claimants have been able to help Gary by explaining in detail **what they were thinking when they decided to take a risk**. This helped Gary to see that the choice that they had to make was more complicated than it seemed. Some claimants have helped Gary to understand **the factors that they were weighing**. Claimants have also helped Gary to imagine **how they thought about the danger**.
{{% /gears %}}

{{% story name="Sonia" href="/6-big-ideas/caution" /%}} told Gary in detail about the thoughts that went through her mind after she started to receive death threats. While she did take the threats seriously, she continued to hope that the situation would blow over. She allowed herself to believe that it would get better.

{{< go-back "/6-big-ideas/caution#Sonia" >}}


{{% story name="Manuel" href="/6-big-ideas/caution" /%}} explained that he was willing to take to take risks to give his children a chance at a good future. By taking a few days to wrap up the business, he could keep his family from being destitute in a new country. 

{{% story name="Purabi" href="/6-big-ideas/caution" /%}} helped Gary to understand why it was so important for her to finish her degree, even though she was in danger. Having a degree would give her a much better chance of supporting herself in Canada. But also, it was a victory over a man who wanted to ruin her life. By forcing her to flee, he had already robbed her of so much: her home, her friends, her family. She had worked hard for this degree. She would not let him take it away from her.

{{< go-back "/6-big-ideas/caution#Purabi" >}}

{{% story name="Ester" href="/6-big-ideas/caution" /%}} and {{% story name="Mukisa" href="/6-big-ideas/caution" /%}} told Gary about how they weighed the advantages and disadvantages of making a refugee claim sooner. 

Ester explained that she thought that Canada was a much safer option than the United States. She thought that she was more likely to be sent home if she made her claim in the United States. She believed that she had a better chance of being accepted as a refugee in Canada. 

Mukisa explained the advantages of waiting to file his refugee claim. He had a valid student visa, so he did not feel that he was in danger of being sent home while he prepared his claim. He knew that he might be returned home if he did not do a good job preparing his claim. He felt that it would be dangerous to make his claim too soon, before he had the evidence that he needed.

{{< go-back "/6-big-ideas/caution#Mukisa" >}}

{{% story name="Désirée" href="/6-big-ideas/caution" /%}} helped Gary to understand how she was able to convince herself that it was safe enough to return home to her children. Looking back, she knows that she was fooling herself. But she wanted her children so badly that she came up with ways to justify why it would be safe enough. She told herself that the state police probably had more important people to persecute. She told herself that they probably felt that she had learned her lesson. Maybe they would not bother with her anymore. She told herself these things and made herself believe them.

{{% story name="Andrés" href="/6-big-ideas/caution" /%}} told Gary that he wanted very badly to see his father again before he died. As his father’s eldest child and only son, he was supposed to care for him in his old age. But Andrés had to flee the country to save himself, and he left that responsibility to his mother and sisters. He felt a great deal of shame about this. He could not live with himself if fear prevented him from being with his father in his final days.

{{< go-back "/6-big-ideas/caution#Andrés" >}}

{{% story name="Lisa" href="/6-big-ideas/caution" /%}} explained to Gary how she dealt with her fears when she found herself falling in love again. At first, she had wondered herself whether the risks were worth it. There was the risk to her own safety and also the risk of once again losing someone she loved. But as she fell more deeply in love, her heart took over. She told herself that there was not much point in living without love. She also believed that her murdered partner would want her to be strong and brave and have a chance at being happy again.

There are words that {{% story name="Mohammed" href="/6-big-ideas/caution" /%}} has said to himself every day for a long time: “If today is my day to die, then I will die today.” He said this to himself after he got the phone call warning him not to attend the union rally. He said it again as he put on his coat and got in his car. He explained to Gary that these words have always given him the courage that he needed. That day they gave him the courage that he needed to go to the rally.

{{% story name="Constance" href="/6-big-ideas/caution" /%}} explained to Gary that she values her life, but she values her relationship with God more. She believes that it is her duty to tell others about her religion, that this is what God expects her do to. She felt that public washrooms were the place where her pamphlets had the best chance of reaching a lot of people. Certainly she was afraid, but she trusted that her life was in God’s hands. She knew that she would be rewarded in the afterlife.

{{< go-back "/6-big-ideas/caution#Constance" >}}