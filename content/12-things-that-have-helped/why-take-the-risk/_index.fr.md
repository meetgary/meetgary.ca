---
title: Expliquer la décision de prendre un risque
weight: 70

---

{{% gears %}}
Gary n’a pas beaucoup d’imagination. Il a du mal à s’imaginer à la place d’un demandeur. Parfois, il pense que la décision de prendre un risque d’un demandeur était tellement absurde qu’il est incapable de le croire. C’est souvent parce qu’il s’imagine que le demandeur n’avait qu’un choix simple à faire entre la sécurité et le danger. Pourquoi quelqu’un choisirait-il le danger? 

Certains demandeurs ont aidé Gary à comprendre en expliquant en détail **ce qu’il pensait au moment de prendre le risque**. Cela a permis à Gary de comprendre que le choix à faire était beaucoup plus compliqué qu’en apparence. Certains demandeurs ont aidé Gary à voir **les facteurs qui pesaient dans la balance**. D’autres demandeurs ont aussi aidé Gary à comprendre **leur perception du danger**.
{{% /gears %}}

{{% story name="Sonia" href="/6-big-ideas/caution" /%}} a expliqué à Gary en détail toutes ses pensées quand elle a commencé à recevoir des menaces de mort. Même si elle prenait les menaces au sérieux, elle continuait de croire que la situation passerait d’elle-même. Elle voulait croire que tout irait mieux. 

{{< go-back "/6-big-ideas/caution#Sonia" >}}


{{% story name="Manuel" href="/6-big-ideas/caution" /%}} a expliqué qu’il était prêt à prendre le risque pour donner un meilleur avenir à ses enfants. En prenant quelques jours pour fermer son entreprise, il voulait s’assurer de donner une meilleure situation à sa famille dans un nouveau pays. 

{{% story name="Purabi" href="/6-big-ideas/caution" /%}} a aidé Gary à comprendre pourquoi il était important pour elle de terminer ses études, malgré le danger. Son diplôme lui donne de meilleures possibilités pour gagner sa vie au Canada. Mais c’est aussi une victoire face à l’homme qui voulait gâcher sa vie. En la forçant en exil, il l’avait déjà privée de sa maison, ses amis, sa famille. Elle avait travaillé fort pour son diplôme, elle ne voulait pas perdre ça aussi.  

{{< go-back "/6-big-ideas/caution#Purabi" >}}

{{% story name="Ester" href="/6-big-ideas/caution" /%}} et {{% story name="Mukisa" href="/6-big-ideas/caution" /%}} ont raconté à Gary comment ils avaient pensé aux avantages et aux désavantages de présenter une demande d’asile plus tôt. 

Ester a expliqué son impression que le Canada était beaucoup plus sécuritaire que les États-Unis. D’après elle, il était beaucoup plus probable qu’elle soit déportée si elle faisait sa demande aux États-Unis. Elle croyait avoir plus de chance d’être acceptée comme réfugiée au Canada. 

Mukisa a expliqué les avantages d’attendre avant de présenter sa demande d’asile. Il avait un visa d’étudiant valide, c’est pourquoi il ne se sentait pas en danger pendant qu’il préparait sa demande d’asile. Il savait qu’il risquait d’être déporté s’il ne présentait pas un bon dossier complet. D’après lui, c’était risqué de faire une demande trop tôt, avant d’avoir toutes les preuves dont il avait besoin. 

{{< go-back "/6-big-ideas/caution#Mukisa" >}}

{{% story name="Désirée" href="/6-big-ideas/caution" /%}} a aidé Gary à comprendre comment elle avait réussi à se convaincre que c’était assez sécuritaire pour retourner voir ses enfants. Quand elle y pense, elle sait que c’était une illusion. Mais elle voulait ses enfants plus que tout et elle s’est convaincue que c’était assez sécuritaire. Elle s’est dit que la police d’État avait probablement d’autres personnes plus importantes à persécuter. Elle s’est dit que le service de police devait penser qu’elle avait appris sa leçon, que peut-être il la laisserait tranquille. Elle s’est répété ses choses jusqu’à ce qu’elle y croit. 

{{% story name="Andrés" href="/6-big-ideas/caution" /%}} a expliqué à Gary qu’il voulait vraiment voir son père avant qu’il meure. Comme il était l’aîné et le seul garçon, c’était sa responsabilité de prendre soin de son père. Mais Andrés avait dû fuir le pays pour sa sécurité en laissant cette responsabilité à sa mère et à ses sœurs. Il avait honte. Il s’en serait voulu toute sa vie s’il ne s'était pas rendu au chevet de son père dans les derniers jours. 

{{< go-back "/6-big-ideas/caution#Andrés" >}}

{{% story name="Lisa" href="/6-big-ideas/caution" /%}} a expliqué à Gary sa façon d’aborder ses peurs quand elle a commencé à tomber en amour. Au début, elle s’était demandé si les risques en valaient la chandelle. C’était risqué pour sa sécurité, mais aussi parce qu’elle avait peur de perdre la personne dont elle était amoureuse encore une fois. Mais elle est tombée de plus en plus amoureuse, et puis son cœur l’a guidé. Elle s’est dit qu’une vie sans amour ne valait pas la peine d’être vécue. Elle croyait aussi que sa partenaire assassinée aurait voulu qu’elle soit forte et brave, et qu’elle ait la chance d’être heureuse.

Il y a des choses que {{% story name="Mohammed" href="/6-big-ideas/caution" /%}} se répète tous les jours depuis longtemps : « Si c’est mon tour aujourd’hui, c’est mon tour. » C’est ce qu’il s’est dit le jour où il a reçu l’appel qui l’intimait de ne pas se rendre au rassemblement. Il l’a répété encore quand il a mis son manteau et s’est assis dans sa voiture. Il a expliqué à Gary que ces mots lui avaient toujours donné le courage dont il avait besoin. Ce jour-là, ces mots lui ont donné le courage d’aller au rassemblement. 

{{% story name="Constance" href="/6-big-ideas/caution" /%}} a expliqué à Gary que sa vie était importante, mais que sa relation avec Dieu l’était encore plus. Elle croit que c’est son devoir de parler de sa religion avec les autres et que c’est ce que Dieu lui demande. Elle avait l’impression qu’elle pouvait atteindre beaucoup de personnes en laissant les dépliants dans les toilettes publiques. Elle avait peur, mais elle faisait confiance à Dieu. Elle savait qu’elle serait récompensée dans l’Au-delà.

{{< go-back "/6-big-ideas/caution#Constance" >}}