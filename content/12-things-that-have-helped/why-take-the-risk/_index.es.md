---
title: Explicar la decisión de asumir un riesgo
weight: 70

---

{{% gears %}}
Gary no tiene una imaginación muy potente. Le cuesta imaginarse a sí mismo ‘en el lugar de una persona solicitante’. A veces piensa que la decisión de una persona solicitante de asumir un riesgo es demasiado insensata como para creer que es verdad. A menudo esto es porque se imagina que la persona solicitante enfrenta una elección sencilla entre la seguridad y el peligro. ¿Por qué razón elegiría alguien el peligro?

Algunas personas solicitantes han ayudado a Gary explicándole en detalle **lo que estaban pensando cuando decidieron asumir un riesgo**. Esto ayudó a Gary a ver que su elección era más complicada de lo que parecía. Algunas personas solicitantes han ayudado a Gary a entender **los factores que estaban considerando**. Las personas solicitantes también han ayudado a Gary a imaginar **su forma de pensar con respecto al peligro**.
{{% /gears %}}

{{% story name="Sonia" href="/6-big-ideas/caution" /%}} le describió a Gary en detalle los pensamientos que empezó a tener desde que empezaron a llegarle amenazas de muerte. Aunque se tomaba las amenazas en serio, pensaba que la situación se iba a disipar. Se permitió a sí misma creer que iba a mejorar.

{{< go-back "/6-big-ideas/caution#Sonia" >}}


{{% story name="Manuel" href="/6-big-ideas/caution" /%}} explicó que estaba dispuesto a asumir riesgos para brindar a sus hijos la oportunidad de un buen futuro. Dedicar algunos días a cerrar su negocio le permitiría evitar que su familia pasara necesidades en otro país. 

{{% story name="Purabi" href="/6-big-ideas/caution" /%}} ayudó a Gary a entender por qué ella consideraba importante terminar sus estudios, a pesar de correr peligro. Obtener su título le daría mejores oportunidades de ganarse la vida en Canadá. Pero también sería una victoria sobre el hombre que quería arruinarle la vida. Al forzarla a huir, ya le había robado mucho: su hogar, sus amigos, su familia. Ella había trabajado duro por ese título. No iba a dejar que él le quitara eso también.

{{< go-back "/6-big-ideas/caution#Purabi" >}}

{{% story name="Ester" href="/6-big-ideas/caution" /%}} y {{% story name="Mukisa" href="/6-big-ideas/caution" /%}} le contaron a Gary de cómo habían considerado las ventajas y desventajas de presentar una solicitud de asilo antes.

Ester explicó que pensó que Canadá sería una opción mucho más segura que los Estados Unidos. Pensaba que era más factible que la regresaran a su país si pedía asilo en los EE.UU. Veía mayor probabilidad de que la aceptaran como refugiada en Canadá. 

Mukisa explicó las ventajas de esperar a presentar su solicitud de asilo. Tenía un visado de estudiante válido, así que sentía que no corría riesgo de que lo regresaran a su país mientras preparaba su solicitud. Sabía que podían regresarlo a su país si no la preparaba bien. Sentía que era peligroso presentarla demasiado pronto antes de obtener las pruebas necesarias.

{{< go-back "/6-big-ideas/caution#Mukisa" >}}

{{% story name="Désirée" href="/6-big-ideas/caution" /%}} ayudó a Gary a entender cómo se convenció de que iba a ser lo suficientemente seguro regresar a casa para estar con sus hijos. Ahora, mirando atrás, sabe que se estaba engañando a sí misma. Pero quería estar con sus hijos a toda costa así que se inventó maneras de justificar por qué iba a ser seguro. Se dijo que la policía estatal probablemente tenía gente más importante que perseguir. Se dijo que probablemente pensaban que ella ya había aprendido su lección. Tal vez ya no se interesarían por ella. Ella se decía todo eso y se obligó a creer que era verdad.

{{% story name="Andrés" href="/6-big-ideas/caution" /%}} le dijo a Gary quería ver a su padre a toda costa antes de su muerte. Como su hijo mayor y único hijo varón, era su deber cuidarlo en su vejez. Pero Andrés tuvo que huir del país para salvarse y le dejó esa responsabilidad a su madre y sus hermanas. Es algo que le hacía sentir mucha vergüenza. No podría vivir consigo mismo si el miedo le impedía estar con su padre en sus últimos días.

{{< go-back "/6-big-ideas/caution#Andrés" >}}

{{% story name="Lisa" href="/6-big-ideas/caution" /%}} le explicó a Gary cómo afrontó sus miedos cuando se dio cuenta que se estaba enamorando otra vez. Al principio se preguntaba si valía la pena correr esos riesgos. Estaba el riesgo que corría su propia seguridad y además el de perder una vez más a alguien que amaba. Pero al seguirse enamorando su corazón tomó el control. Se dijo que vivir sin amor no tenía ningún sentido. También sentía que su pareja, a quien asesinaron, querría que fuese fuerte y valiente y buscara la oportunidad de volver a ser feliz.

Hay palabras que {{% story name="Mohammed" href="/6-big-ideas/caution" /%}} se repite todos los días desde hace mucho tiempo: “Si me toca morirme hoy, pues hoy me muero”. Eso es lo que se dijo después de colgar la llamada que recibió advirtiéndole que no asistiera al rally del sindicato. Se lo dijo otra vez cuando se puso su abrigo y se montó en su carro. Él le explicó a Gary que esas palabras siempre le dieron la valentía que necesitaba. Ese día le dieron el valor que necesitaba para ir al rally.

{{% story name="Constance" href="/6-big-ideas/caution" /%}} le explicó a Gary que valora su vida pero que valora aún más su relación con Dios. Ella considera su deber hacer conocer su religión a otras personas, que eso es lo que Dios espera de ella. Sentía que en los baños públicos era más probable que sus panfletos alcanzaran un gran número de personas. Sin duda sentía temor pero confiaba que su vida estaba en manos de Dios. Sabía que tendría su recompensa en el más allá. 

{{< go-back "/6-big-ideas/caution#Constance" >}}