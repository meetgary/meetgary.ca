---
title: Making every effort to get evidence
weight: 30

---

{{% gears %}}
Often claimants do not realize how important it is to try to get evidence to back up their stories. Sometimes they think that the fact that they are telling the truth will be obvious. Other times, they do not see any point in trying because they feel sure that they will not succeed. But if there is **any chance of getting evidence**, Gary will expect the claimant **at least to have made the effort**.
{{% /gears %}}

{{% story name="Viktor" href="/6-big-ideas/caution" /%}} believed that trying to get proof of his story would be hopeless. The friend who took him to the hospital had his own troubles and would not want to write a letter. He suspected that the hospital and the police would not cooperate with his requests. But if he had written to his friend, to the hospital, and to the police, he could have sent proof of this to the Board. For instance, he could have made copies of his letters and the postage receipts. Even if none of them ever wrote back, Viktor could have shown Gary that he had done everything he could to try to support his claim.

{{< go-back "/6-big-ideas/caution#Viktor" >}}