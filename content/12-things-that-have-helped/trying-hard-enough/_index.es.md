---
title: Hacer todo lo posible por obtener pruebas
weight: 30

---

{{% gears %}}
A menudo las personas solicitantes no se dan cuenta lo importante que es intentar obtener pruebas en apoyo de sus historias. A veces piensan que el hecho de decir la verdad será evidente. Otras veces, no ven razón alguna en intentarlo porque tienen claro que no lo lograrán. Pero si hay **alguna posibilidad de obtener pruebas**, Gary espera que la persona solicitante **al menos haya hecho el esfuerzo**.
{{% /gears %}}

{{% story name="Viktor" href="/6-big-ideas/caution" /%}} creyó que era inútil intentar obtener pruebas en apoyo de su historia. El amigo que lo llevó al hospital tenía sus propios problemas y no querría escribir una carta. Viktor sospechaba que el hospital y la policía no cooperarían con sus pedidos. Pero si le hubiese escrito al amigo, al hospital y a la policía, podría haber enviado prueba a la Comisión. Por ejemplo, podría haber hecho copias de sus cartas y de las facturas de los gastos de envío. Aún si nadie le hubiese respondido, Viktor podría haberle mostrado a Gary que había hecho todo lo posible por respaldar su solicitud.

{{< go-back "/6-big-ideas/caution#Viktor" >}}