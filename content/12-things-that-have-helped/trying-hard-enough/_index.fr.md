---
title: Faire tous les efforts pour rassembler les preuves
weight: 30

---

{{% gears %}}
Souvent les demandeurs ne réalisent pas à quel point il est important d’essayer d’obtenir des preuves pour appuyer leur histoire. Parfois, ils pensent qu’il est évident qu’ils disent la vérité. Parfois, ils ont l’impression que ça ne vaut pas la peine, car ils sont persuadés qu’ils n’y arriveront pas. Mais s’il y a **ne serait-ce qu’une chance d’obtenir les preuves**, Gary s’attend à ce que le demandeur **ait au moins fait l’effort d’essayer**.
{{% /gears %}}

{{% story name="Viktor" href="/6-big-ideas/caution" /%}} croyait qu’il était impossible de trouver des preuves pour appuyer son histoire. L’ami qui l’avait accompagné à l’hôpital avait ses propres problèmes et allait sûrement refuser d’écrire la lettre. Il s’attendait à ce que l’hôpital et la police ne collaborent pas et ne l’aident pas avec sa demande. Mais s’il avait écrit à son ami, ou à l’hôpital ou à la police, il aurait au moins pu donner ces preuves à la Commission. Par exemple, il aurait pu faire des copies de ses lettres et fournir l’avis de réception du service postal. Même s’ils n’avaient pas répondu, Viktor aurait pu montrer à Gary qu’il avait tout essayé pour obtenir des preuves. 

{{< go-back "/6-big-ideas/caution#Viktor" >}}