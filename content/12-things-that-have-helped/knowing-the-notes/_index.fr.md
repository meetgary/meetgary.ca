---
title: Savoir ce qu’a écrit l’agent d’IRCC dans ses notes
linkTitle: Savoir ce qu’a écrit l’agent d’IRCC
weight: 20

---

{{% gears %}}
Les agents d’IRCC qui font les interrogatoires auprès des demandeurs d’asile sont censés écrire ce que disent les demandeurs. Gary compare ce que disent les demandeurs à l’audience avec ce qu’a écrit l’agent d’IRCC. Les demandeurs peuvent se préparer à mieux raconter leur histoire s’ils savent **ce qu’a écrit l’agent dans ses notes** pendant l’interrogatoire. 
{{% /gears %}}

Quand {{% story name="Mustafa" href="/6-big-ideas/memory" /%}} a parlé avec l’agent d’IRCC, il a fait une approximation du nombre de rassemblements du parti auxquels il avait participé. Il a fait une approximation différente quand Gary lui a posé la même question. S’il avait relu les notes de l’agent, il se serait souvenu de ce qu’il avait dit la première fois. Il aurait pu répondre la même chose. 

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

Si {{% story name="Tanzim" href="/6-big-ideas/memory" /%}} avait lu les notes de l’agente, il aurait remarqué que l’agente avait écrit qu’il était membre du parti et qu’il s’agissait d’une erreur. Tanzim aurait pu informer Gary au sujet de l’erreur au début de l’audience. Cela aurait sans doute été mieux que de laisser Gary trouver l’erreur de lui-même. 

{{% exclamation-highlight %}}
**Avertissement :** Les demandeurs prennent un risque quand ils décident de signaler un problème à Gary. S’ils ne disent rien, il se peut que Gary ne s’en rende pas compte. Mais le fait de signaler le problème à Gary démontre que le demandeur n’essaie pas de le cacher. Souvent Gary trouve cela moins louche que s’il découvre le problème lui-même. 

{{< go-back "/6-big-ideas/memory#Tanzim" >}}