---
title: Knowing what the IRCC officer wrote in their notes
linkTitle: Knowing what the IRCC officer wrote
weight: 20

---

{{% gears %}}
The IRCC officers who interview claimants are supposed to write down what the claimants say. Gary will compare what claimants say in their hearings to what the officer wrote in their notes. Claimants may be better able to tell their story at their hearing if they know **what the officer wrote in their notes** during their first interview. 
{{% /gears %}}

When {{% story name="Mustafa" href="/6-big-ideas/memory" /%}} first spoke with the IRCC officer, he made a guess about how many party meetings he had attended. He made a different guess when Gary asked the same question. If he had read the officer’s notes, he would have been reminded of what he had said before. Then he could have been more consistent.

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

If {{% story name="Tanzim" href="/6-big-ideas/memory" /%}} had read the officer’s notes, he might have noticed that the officer had made a mistake when she wrote that Tanzim was a party member. Tanzim could have told Gary about the mistake at the start of the hearing. That might have been better than letting Gary find the problem.

{{% exclamation-highlight %}}
**Warning:** Claimants take a risk when they choose to bring a problem to Gary’s attention.  If they say nothing, there is always the chance that Gary will not notice it. But telling him about a problem shows Gary that the claimant is not trying to hide it. This often makes him less suspicious than when he discovers a problem himself. 

{{< go-back "/6-big-ideas/memory#Tanzim" >}}