---
title: Saber lo que el oficial de IRCC escribió en sus notas
linkTitle: Saber lo que el oficial de IRCC anotó
weight: 20

---

{{% gears %}}
Los oficiales de IRCC que entrevistan a personas solicitantes deben anotar lo que ellas dicen. Gary compara lo que las personas solicitantes dicen en la audiencia con lo que el oficial ha escrito en sus notas. Las personas solicitantes pueden estar mejor preparadas para contar su historia en la audiencia si saben **lo que el oficial escribió en sus notas** durante la primera entrevista. 
{{% /gears %}}

La primera vez que {{% story name="Mustafa" href="/6-big-ideas/memory" /%}} habló con el oficial de IRCC, dio una conjetura sobre el número de reuniones del partido a las que había asistido. Dio una conjetura distinta cuando Gary le hizo la misma pregunta. Si Mustafa hubiese leído las notas del oficial, habría recordado lo que había dicho antes y, así, podría haber sido más consistente.

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

Si {{% story name="Tanzim" href="/6-big-ideas/memory" /%}} hubiese leído las notas del oficial, podría haberse dado cuenta del error del oficial, que escribió que Tanzim era miembro del partido. Tanzim podría haber advertido a Gary de dicho error al principio de la audiencia. Tal vez eso hubiera resultado mejor que dejar que Gary encontrara el problema por su cuenta.

{{% exclamation-highlight %}}
**Advertencia:** Las personas solicitantes asumen un riesgo cuando deciden dirigir la atención de Gary hacia un problema. Si no dicen nada, siempre existe la posibilidad de que Gary no lo note. Pero al señalarle un problema, la persona solicitante le demuestra a Gary que su intención no es ocultarlo. A menudo Gary sospecha menos en esos casos que cuando descubre un problema por su cuenta.

{{< go-back "/6-big-ideas/memory#Tanzim" >}}