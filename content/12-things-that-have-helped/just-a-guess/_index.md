---
title: Making clear when an answer is only an estimate or a guess
linkTitle: Making clear that it is just a guess
weight: 50

---

{{% gears %}}
Claimants may feel pressured to give Gary information that they do not have. Gary might ask the same question several times, or he might press a claimant for an answer. If a claimant does not know the answer, they might estimate or guess. Often this happens when Gary asks questions about time or quantities: When? For how long? How often? How many?


Claimants run a risk when they estimate or guess. Their estimate or guess may be wrong. And if they give a different estimate later, it may seem to Gary that they are changing their story. But they also run a risk if they simply insist that they do not know the answer. Gary might feel strongly that an answer to the question is important. He may find that they are being evasive.

Some claimants have avoided this problem by making very clear to Gary that **they do not know the answer**. They explain that the information they are giving is **only a guess or an estimate**. They make clear that they **are not confident that their estimate or guess is accurate**.
{{% /gears %}}

{{% story name="Mustafa" href="/6-big-ideas/memory" /%}} never had a very precise idea of how often he went to meetings of his political party. He gave one estimate to the officer and a different estimate to Gary. Mustafa could have explained that his answers were only rough guesses. Then Gary might have been less suspicious.

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

{{% story name="Lupe" href="/6-big-ideas/memory" /%}} did not have a clear sense of how much time had passed between the two threatening phone calls. “A couple of weeks” and “about a month” were both just estimates. If Lupe had explained that her answers were only rough guesses, Gary might have been more inclined to believe her.

{{< go-back "/6-big-ideas/memory#Lupe" >}}


{{% story name="Jean-René" href="/6-big-ideas/memory" /%}} guessed that the demonstration “would have happened at the end of April.” Before he said that, he could have explained that he remembered that it took place in the spring, but he did not remember the month. Gary might have insisted that he be more precise. Then Jean-René could have said that, if he had to guess, it might have been around the end of April.

{{< go-back "/6-big-ideas/memory#Jean-René" >}}

By the time of her hearing, {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} could no longer remember the make of the vehicle. “Maybe it was a Jeep?” was just a guess. Rather than offering a guess right away, Yvonne could have said that she did not know the answer to Gary’s question. She might have explained to Gary that she tries very hard not to think about this event. There are things about it now that she cannot clearly remember. Then, if Gary insisted, she could have offered the guess of a Jeep, while making clear that she really could not remember.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}