---
title: Aclarar cuando una respuesta es solamente una aproximación o una conjetura
linkTitle: Aclarar cuando es solamente una suposición
weight: 50

---

{{% gears %}}
Las personas solicitantes pueden sentirse presionadas para dar a Gary información que no poseen. Gary puede hacer la misma pregunta varias veces, o presionar a la persona solicitante pidiendo una respuesta. Cuando una persona solicitante no sabe la respuesta, es posible que den una aproximación o una conjetura. Esto sucede a menudo cuando Gary hace preguntas sobre períodos de tiempo o cantidades: ¿Cuándo? ¿Durante cuánto tiempo? ¿Con qué frecuencia? ¿Cuántos?

Las personas solicitantes corren un riesgo dando una aproximación o una conjetura, pues la misma puede ser errada. Y si posteriormente dan una aproximación distinta, Gary puede pensar que están cambiando la historia. Pero también corren un riesgo si simplemente insisten en que no saben la respuesta. Gary puede estar convencido de que es importante responder esa pregunta. Puede pensar que están evadiendo la pregunta.

Algunas personas solicitantes han evitado este problema dejándole muy claro a Gary que **no saben la respuesta**. Explican que la información que proporcionan es **solamente una conjetura o una aproximación**. Dejan claro que **no confían en que su aproximación o conjetura sea certera**.
{{% /gears %}}

{{% story name="Mustafa" href="/6-big-ideas/memory" /%}} nunca tuvo una idea muy precisa de la frecuencia con la que se realizaban las reuniones de su partido político. Dio una aproximación al oficial y otra estimación a Gary. Mustafa pudo haber explicado que sus respuestas eran sólo conjeturas. Así Gary habría tenido menos sospechas.

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

{{% story name="Lupe" href="/6-big-ideas/memory" /%}} no tenía claro cuánto tiempo había pasado entre las dos llamadas amenazadoras. Tanto “un par de semanas” como “más o menos un mes” eran simples aproximaciones. Si Lupe hubiese explicado que sus respuestas eran sólo conjeturas, Gary habría estado más dispuesto a creerle.

{{< go-back "/6-big-ideas/memory#Lupe" >}}


{{% story name="Jean-René" href="/6-big-ideas/memory" /%}} aproximó que la manifestación “habría sido a finales de abril”. Antes de decir eso, pudo haber explicado que recordaba que tuvo lugar en primavera pero que no recordaba en qué mes. Gary quizás habría insistido pidiendo mayor precisión. Entonces Jean-René podría haber dicho que, si tuviera que aproximar, podría haber sucedido a finales de abril.

{{< go-back "/6-big-ideas/memory#Jean-René" >}}

Al momento de su audiencia, {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} ya no recordaba la marca del vehículo. “¿Tal vez era un Jeep?” fue una simple conjetura. En vez de ofrecer una conjetura de inmediato, Yvonne pudo haber dicho que no sabía la respuesta a la pregunta de Gary. Pudo haberle explicado a Gary que intentaba en lo posible no pensar en ese suceso, que hay detalles que ahora no logra recordar. De esa manera, si Gary insistía, hubiese dicho “un Jeep” como conjetura, dejando claro que en realidad no lograba recordar.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}