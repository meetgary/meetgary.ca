---
title: التوضيح عندما تكون الإجابة مجرد تقدير أو تخمين
linkTitle: توضيح أن الإجابة مجرد تخمين
weight: 50

---

{{% gears %}}
قد يشعر المتقدمون بأنهم مضغوطون لإعطاء غاري معلومات ليست بحوزتهم. قد يطرح غاري نفس السؤال عدة مرات، أو قد يضغط على المتقدم للحصول على إجابة. إذا كان المتقدمون لا يعرفون الإجابة، فقد يقدرون أو يخمنون. غالبًا ما يحدث هذا عندما يطرح غاري أسئلة حول الوقت أو الكميات: متى؟ إلى متى؟ كم مرة؟ كم العدد؟ 

يخاطر المتقدمون عندما يقدرون أو يخمنون. قد يكون تقديرهم أو تخمينهم خطأ. وإذا أعطوا تقديرًا مختلفًا لاحقًا، فقد يبدو لغاري أنهم يغيرون قصتهم. لكنهم يخاطرون أيضًا إذا أصروا ببساطة على أنهم لا يعرفون الإجابة. قد يشعر غاري بقوة بأن الإجابة على السؤال مهمة. قد يجد أنهم يراوغون.

تجنب بعض المتقدمين هذه المشكلة من خلال توضيحهم لغاري أنهم **لا يعرفون الإجابة**. ويشرحون أن المعلومات التي يقدمونها **هي مجرد تخمين أو تقدير**. يوضحون أنهم **غير واثقين من أن تقديرهم أو تخمينهم دقيق**.
{{% /gears %}}

لم يكن لدى {{% story name="مصطفى" href="/6-big-ideas/memory" /%}} فكرة دقيقة للغاية عن عدد المرات التي ذهب فيها إلى اجتماعات حزبه السياسي. أعطى تقديراً معيناً للضابط وتقديراً مختلفاً لغاري. كان بإمكان مصطفى أن يشرح أن إجاباته كانت مجرد تخمينات تقريبية. لربما كان غاري أقل اشتباهاً.

{{< go-back "/6-big-ideas/memory#مصطفى" >}}

لم يكن لدى {{% story name="لوب" href="/6-big-ideas/memory" /%}} فكرة واضحة عن مقدار الوقت الذي انقضى بين مكالمتي التهديد الهاتفيتين. فقد كان كل من "أسبوعين" و "حوالي شهر" مجرد تقديرات. لو أن لوب كانت قد أوضحت أن إجاباتها كانت مجرد تخمينات تقريبية، لربما كان غاري أكثر ميلًا لتصديقها. 

{{< go-back "/6-big-ideas/memory#لوب" >}}

خمن {{% story name="جان-رينيه" href="/6-big-ideas/memory" /%}} بأن المظاهرة " لا بد قد حدثت في نهاية أبريل". قبل أن يقول ذلك، كان بإمكانه أن يشرح أنه يتذكر أنها حدثت في الربيع، لكنه لا يتذكر الشهر. لربما أصر غاري على أن يكون أكثر دقة. عندها كان بإمكان جان-رينيه أن يقول إنه إذا كان عليه أن يخمن، فلربما كانت حوالي نهاية أبريل.

{{< go-back "/6-big-ideas/memory#جان-رينيه" >}}

لحين جلسة استماعها، لم تعد {{% story name="إيفون" href="/6-big-ideas/memory" /%}} تتذكر ماركة صنع السيارة. "ربما كانت سيارة جيب؟" كان مجرد تخمين. بدلاً من تقديم تخمين على الفور، كان بإمكان إيفون أن تقول بأنها لا تعرف الإجابة على سؤال غاري. كان بإمكانها أن توضح لغاري بأنها تحاول جاهدة ألا تفكر في هذا الحدث. هناك أشياء حوله لا تستطيع تذكرها بوضوح الآن. حينها، إذا أصر غاري، لكان بإمكانها تقديم تخمين سيارة جيب، مع توضيح أنها لا تستطيع حقًا أن تتذكر.

{{< go-back "/6-big-ideas/memory#إيفون" >}}
