---
title: Expliquer clairement qu’il s’agit d’une approximation ou d’une hypothèse
linkTitle: Expliquer clairement qu’il s’agit d’une hypothèse
weight: 50

---

{{% gears %}}
Les demandeurs peuvent se sentir obligés de donner des informations qu’ils n’ont pas. Il arrive que Gary pose la même question plus d’une fois, ou qu’il insiste pour une réponse. Si les demandeurs ne connaissent pas la réponse, ils peuvent fournir une approximation ou hypothèse. Cela se produit souvent quand Gary pose des questions sur le temps ou les quantités : Quand? Pendant combien de temps? À quelle fréquence? Combien?

Les demandeurs prennent un risque quand ils formulent une approximation ou une hypothèse. Il se peut que leur approximation ou hypothèse soit erronée. Et s’ils offrent une approximation différente plus tard, Gary risque d’avoir l’impression qu’ils changent leur histoire. Mais c’est aussi risqué d’affirmer qu’ils ne savent pas tout simplement. Si Gary est d’avis qu’une réponse est importante, il pourrait avoir l’impression qu’ils veulent contourner la question.

Certains demandeurs ont évité ce problème en informant Gary très clairement **qu’ils n’ont pas de réponse**. Ils expliquent que les renseignements qu’ils donnent ne sont que **des approximations ou des hypothèses**. Ils ajoutent clairement que **d’après eux, leurs approximations ou hypothèses ne sont pas exactes**.
{{% /gears %}}

{{% story name="Mustafa" href="/6-big-ideas/memory" /%}} n’a jamais vraiment su à quelle fréquence il se rendait aux réunions de son parti politique. Il a offert une approximation à l’agent, et une autre à Gary. Mustafa aurait pu expliquer que ses réponses ne sont que des approximations sans précision. Gary aurait trouvé cela moins louche. 

{{< go-back "/6-big-ideas/memory#Mustafa" >}}

{{% story name="Lupe" href="/6-big-ideas/memory" /%}} ne savait pas précisément combien de temps s’était écoulé entre les deux appels de menace. « Quelques semaines » et « environ un mois » sont des approximations. Si Lupe avait expliqué que ses réponses n’étaient que des approximations, Gary l’aurait peut-être crue.

{{< go-back "/6-big-ideas/memory#Lupe" >}}


{{% story name="Jean-René" href="/6-big-ideas/memory" /%}} a estimé que la manifestation « était probablement à la fin avril ». Avant de répondre, il aurait pu dire qu’il se souvenait que c’était au printemps, mais qu’il ne se rappelait pas le mois précis. Gary aurait pu insister pour obtenir une réponse plus précise. Jean-René aurait alors pu dire que, s’il devait essayer de deviner, il dirait que c’était vers la fin du mois d’avril. 

{{< go-back "/6-big-ideas/memory#Jean-René" >}}

Au moment de son audience, {{% story name="Yvonne" href="/6-big-ideas/memory" /%}} ne se souvenait plus de la marque du véhicule. « C’était peut-être un Jeep » n’était qu’une hypothèse. Au lieu de formuler cette hypothèse tout de suite, Yvonne aurait pu dire qu’elle ne savait pas. Elle aurait pu expliquer à Gary qu’elle fait de son mieux pour ne plus penser à cet événement. Il y a plusieurs choses dont elle ne se souvient plus. Puis, si Gary avait insisté, elle aurait pu présenter l’hypothèse du Jeep tout en précisant qu’elle ne se souvenait plus.

{{< go-back "/6-big-ideas/memory#Yvonne" >}}