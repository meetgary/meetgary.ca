---
title: Explaining how they understood the instructions or the questions
linkTitle: Explaining how they understood the questions
weight: 90

---

{{% gears %}}
Gary is very familiar with the forms that the Board uses. To Gary, the instructions on the forms seem very clear. He cannot imagine why a claimant would understand them differently. Claimants have sometimes been able to help Gary see **why they interpreted the instructions the way that they did**.

Gary also thinks that he asks clear and straightforward questions. Claimants have sometimes been able to help Gary to understand **why they found his question confusing**.
{{% /gears %}}

{{% story name="Svetlana" href="/6-big-ideas/instructions-clear" /%}}, {{% story name="Jamal" href="/6-big-ideas/instructions-clear" /%}} and {{% story name="Araceli" href="/6-big-ideas/instructions-clear" /%}} all read the instruction on the Basis of Claim form that tells them to “include everything that is important.” But they understood this instruction differently from the way that Gary does.

Svetlana explained to Gary that she gave two examples of the ways that her husband hurt her. She thought that these two examples would be enough to show the Board why she was afraid to go home. And if the Board member did not believe her, what was the use of giving more examples? 

Jamal explained that when he read the instruction to “include everything that is important,” he thought this meant that he should provide a summary of his important experiences. After all, he could not possibly describe every time that he had been harassed, threatened, or attacked. 

Araceli wrote on the form that she had been threatened. She explained to Gary that she thought that the fact that she was threatened was what was important. She knew she would have a chance to tell him more details at the hearing. 

The BOC form asks claimants to discuss any danger facing “you and your family.” {{% story name="Lester" href="/6-big-ideas/instructions-clear" /%}} understood this to mean his immediate family. He explained to Gary that he was focused on telling his own story. It was easy to miss the reference further down in the instructions to “persons in situations similar to yours.”

{{< go-back "/6-big-ideas/instructions-clear#Lester" >}}

{{% story name="Mamun" href="/6-big-ideas/instructions-clear" /%}} ran into trouble because of how he filled out the part of the form that asked him to list his past addresses. Mamun did not include his aunt’s address because the form asked him to list all the addresses where he had “lived.” He felt that he was only staying temporarily at his aunt’s house, not living there. All his belongings and his furniture were still at his home. His mail was still being delivered to his home. His telephone and internet service were still connected. Mamun explained to Gary that he still thought of his home as the place where he “lived,” even though he was forced to stay elsewhere.

{{% story name="Pia" href="/6-big-ideas/instructions-clear" /%}} had to explain to Gary that she had simply not paid precise enough attention to the dates when she was filling out the form. She had not realized how important it would be to give the exact date she left her job.

{{< go-back "/6-big-ideas/instructions-clear#Pia" >}}

Gary asked {{% story name="Angela" href="/6-big-ideas/instructions-clear" /%}} “Why do you think that your father is still looking for you?” He wanted to know how she knew that her father was still looking for her. But Angela understood his question differently. She thought that he was asking her to tell him about her father’s motivation. She started explaining her father’s reasons for wanting to find her. When Angela realized what Gary meant, she explained to him what she thought he had been asking.

{{% story name="Farzad" href="/6-big-ideas/instructions-clear" /%}} got confused because Gary was asking questions that went back and forth between his first arrest and his second arrest. When he realized what had happened, he explained to Gary that he must have misunderstood. He thought Gary’s question was about the second arrest. Gary did not seem to accept this explanation. Gary thought that his questions had been very clear. But Gary remembered that Farzad had raised the issue. After the hearing, Gary listened to the recording of his questions and Farzad’s answers. Then he understood why Farzad had been confused.

{{< go-on "/6-big-ideas/canada-is-great#Farzad" >}}

When Gary asked {{% story name="Zoya" href="/6-big-ideas/canada-is-great" /%}}, “Why did you come to Canada?” he wanted her to explain why she had left her country. She thought that he was asking her why she fled to Canada and not to some other country. When Zoya realized what Gary meant, she explained to him how she had understood his question. Then she explained why she had left her country.

{{< go-back "/6-big-ideas/canada-is-great" >}}