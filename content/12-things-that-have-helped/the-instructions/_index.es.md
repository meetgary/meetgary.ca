---
title: Explicar cómo entendieron las instrucciones o las preguntas
linkTitle: Explicar cómo entendieron las preguntas
weight: 90

---

{{% gears %}}
Gary conoce bien los formularios que usa la Comisión. Desde el punto de vista de Gary, las instrucciones dadas en los formularios son muy claras. No se imagina cómo una persona solicitante las entendería de otra manera. Las personas solicitantes a veces han podido ayudar a Gary a ver **por qué interpretaron las instrucciones de esa manera**.

Gary también piensa que hace preguntas claras y directas. Las personas solicitantes a veces han podido ayudar a Gary a entender **por qué les pareció confusa su pregunta**.
{{% /gears %}}

{{% story name="Svetlana" href="/6-big-ideas/instructions-clear" /%}}, {{% story name="Jamal" href="/6-big-ideas/instructions-clear" /%}} y {{% story name="Araceli" href="/6-big-ideas/instructions-clear" /%}} leyeron la instrucción dada en el formulario de Bases de la solicitud que indica que se debe “incluir todo lo que sea importante”. Pero entendieron dicha instrucción de manera distinta a la de Gary.

Svetlana le explicó a Gary que dio dos ejemplos de las formas en las que su esposo la lastimaba. Pensó que esos dos ejemplos serían suficiente para mostrar a la Comisión por qué le daba miedo volver a casa. Y si el miembro de la Comisión no le iba a creer, ¿de qué iba a servir dar más ejemplos?

Jamal explicó que cuando leyó la instrucción “incluir todo lo que sea importante”, pensó que se refería a dar un resumen de sus experiencias importantes. Después de todo, no era posible describir cada una de las veces que había sido acosado, amenazado o atacado. 

Araceli escribió en el formulario que había sido amenazada. Le explicó a Gary que pensaba que el hecho de haber sido amenazada es lo que era importante. Sabía que tendría la oportunidad de explicarle más detalles durante la audiencia.

El formulario de BOC pide a las personas solicitantes describir todos los peligros que enfrentan “usted y su familia”. Para {{% story name="Lester" href="/6-big-ideas/instructions-clear" /%}} esto significaba su familia inmediata. Le explicó a Gary que se había concentrado en contar su propia historia. Fue fácil pasar por alto la referencia a “personas en situaciones similares a la suya” más adelante en el formulario.

{{< go-back "/6-big-ideas/instructions-clear#Lester" >}}

{{% story name="Mamun" href="/6-big-ideas/instructions-clear" /%}} se metió en problemas por la manera en que llenó una parte del formulario donde se le pedía indicar sus direcciones anteriores. Mamun no incluyó la dirección de su tía porque en el formulario se le pedía indicar las direcciones donde había “vivido”. Él sentía que estaba quedándose en la casa de su tía solamente por un tiempo, no viviendo con ella. Aún tenía todas sus pertenencias y muebles en su casa. Su correo todavía llegaba a su casa. Su servicio de teléfono y de internet seguían conectados. Mamun le explicó a Gary que aún consideraba su casa como el sitio donde “vivía”, a pesar de que se vio forzado a quedarse en otro sitio.

{{% story name="Pia" href="/6-big-ideas/instructions-clear" /%}} tuvo que explicarle a Gary que simplemente no había prestado suficiente atención a las fechas al momento de llenar el formulario. No se había dado cuenta de lo importante que era dar la fecha exacta en que había renunciado a su empleo.

{{< go-back "/6-big-ideas/instructions-clear#Pia" >}}

Gary le preguntó a {{% story name="Angela" href="/6-big-ideas/instructions-clear" /%}} “¿Por qué piensas que tu padre te sigue buscando?” Él quería saber cómo ella sabía que su padre la seguía buscando. Pero Angela entendió la pregunta de otra manera. Ella pensó que Gary le estaba pidiendo contar los motivos de su padre. Empezó a explicar los motivos que tenía su padre para querer encontrarla. Una vez que Angela entendió lo que Gary quería decir, ella le explicó lo que había pensado que él preguntaba.

{{% story name="Farzad" href="/6-big-ideas/instructions-clear" /%}} se confundió porque Gary hacía preguntas que alternaban entre su primera detención y la segunda. Cuando se dio cuenta lo que había pasado, le explicó a Gary que debió haber un malentendido. El pensó que la pregunta de Gary se trataba de la segunda detención. Gary no pareció aceptar dicha explicación. Gary pensaba que sus preguntas habían sido muy claras. Pero Gary recordó que Farzad había planteado el problema. Después de la audiencia, Gary escuchó la grabación de sus propias preguntas y las respuestas de Farzad. Entonces entendió por qué Farzad se había confundido.

{{< go-on "/6-big-ideas/canada-is-great#Farzad" >}}

Cuando Gary le preguntó a {{% story name="Zoya" href="/6-big-ideas/canada-is-great" /%}}, “¿Por qué viniste a Canadá?” quería que explicara por qué ella se había ido de su país. Ella pensó que el preguntaba por qué había huido a Canadá en vez de a otro país. Cuando Zoya entendió lo que Gary quería decir, le explicó cómo había entendido la pregunta. Entonces explicó por qué se había ido de su país.

{{< go-back "/6-big-ideas/canada-is-great" >}}