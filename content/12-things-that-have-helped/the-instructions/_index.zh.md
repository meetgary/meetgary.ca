---
title: 解释他们是如何理解指示或问题的
linkTitle: 解释他们是如何理解问题的
weight: 90

---

{{% gears %}}
Gary对委员会使用的表格非常熟悉。对Gary来说，表格上面的指示看上去非常清楚。他不能想象为什么一个申请人会对它们有不同的理解。申请人有些时候可以帮助Gary了解**为什么他们会把那些指示理解成他们理解的那样**。

Gary还认为，他问的问题是清楚和直接的。申请人有些时候能够帮助Gary理解**为什么他们认为他的问题令人困惑**。
{{% /gears %}}

{{% story name="Svetlana" href="/6-big-ideas/instructions-clear" /%}}, {{% story name="Jamal" href="/6-big-ideas/instructions-clear" /%}} 和 {{% story name="Araceli" href="/6-big-ideas/instructions-clear" /%}} 都读了申请难民基本材料表上面的指示，告诉他们要“包括所有重要的内容”。但是他们对这个指示的理解和Gary的不同。

Svetlana解释给Gary说，她提供了两个她丈夫种种方式伤害她的例子。她觉得这两个例子就足以向委员会说明为什么她害怕回家去。如果委员会委员不相信她，举更多的例子又有什么用呢? 

Jamal解释说，当他读到指示“包含所有重要的内容”，他想这个意思是，他应该提供他的重要经历的摘要总结。毕竟，把他经历的每一次骚扰，恐吓，袭击，都描述下来是不可能。 

Araceli在她的表格里写了她被恐吓过。她向Gary解释，她认为，她被恐吓过的事实是重要的。她知道她会有机会在听证会上，向他讲更多的细节。

BOC表格要求申请人描述“你和你的家人”面对的危险。 {{% story name="Lester" href="/6-big-ideas/instructions-clear" /%}} 理解这个意思是指他的直系亲属。他向Gary解释，他在着重讲他自己的故事。后面的指示中所提到的“和你有类似经历的人”是容易漏掉的。

{{< go-back "/6-big-ideas/instructions-clear#Lester" >}}

{{% story name="Mamun" href="/6-big-ideas/instructions-clear" /%}} 遇到问题是因为他在表格中要求他填写过去的地址时所写的内容。Mamum没有把他阿姨的地址包括进去，是因为表格要求他填写曾经“居住”过的所有地址。他觉得他只是暂时待在她的阿姨家，不是居住在那里。他所有的物品和家具仍然在他自己家里。他的邮件仍然是送到他自己家里。他的电话和网络服务仍然是联着的。Mamum向Gary解释，尽管他被迫待在别的地方，他仍然觉得他自己的家是他“居住”的地方。

{{% story name="Pia" href="/6-big-ideas/instructions-clear" /%}} 需要向Gary解释，她在填写表格的时候，对日期没有足够准确地注意。她没有意识到，提供她离开工作的准确日期有多么重要。

{{< go-back "/6-big-ideas/instructions-clear#Pia" >}}

Gary问 {{% story name="Angela" href="/6-big-ideas/instructions-clear" /%}} “你为什么认为你的爸爸仍然在找你？” 他想知道，她是怎么知道她的爸爸还在找她的。但是Angela对他的问题理解得不一样。她以为，他是让她告诉他，她爸爸的动机。她就开始解释她爸爸要找她的原因。当Angela明白了Gary的意思，她向他解释，她之前以为他在问什么。

{{% story name="Farzad" href="/6-big-ideas/instructions-clear" /%}} 之所以会混乱，是因为Gary的问题在他第一次被捕和第二次被捕之间切换。当他意识到发生了什么的时候，他向Gary解释，他肯定是误解了。他以为Gary的问题是在说第二次逮捕。Gary似乎并不能接受这种解释。Gary认为他的问题很清楚。但是Gary记得Farzad指出了这个问题。在听证会之后，Gary听了他的问题和Farzad的回答的录音。于是他明白Farzad为什么混乱了。

{{< go-on "/6-big-ideas/canada-is-great#Farzad" >}}

当Gary问{{% story name="Zoya" href="/6-big-ideas/canada-is-great" /%}}，“你为什么来加拿大？”他是想让她解释她为什么离开了她的国家。她以为他是在问，她为什么逃来加拿大，而不是去别的国家。当Zoya理解了Gary的意思，她向他解释，她之前是怎么理解他的问题的。然后她解释了她为什么离开了她的国家。

{{< go-back "/6-big-ideas/canada-is-great" >}}