---
title: Expliquer sa façon de comprendre les instructions ou les questions
linkTitle: Expliquer sa compréhension des questions
weight: 90

---

{{% gears %}}
Gary connaît très bien les formulaires utilisés par la Commission. D’après lui, les instructions sur les formulaires sont très claires. Il ne comprend pas pourquoi un demandeur pourrait comprendre différemment. Les demandeurs ont parfois été en mesure d’aider Gary à comprendre **pourquoi ils avaient interprété les instructions d’une façon ou d’une autre**.

Gary pense aussi qu’il pose des questions claires et évidentes. Certains demandeurs ont réussi à aider Gary à comprendre **pourquoi ses questions semaient la confusion**.
{{% /gears %}}

{{% story name="Svetlana" href="/6-big-ideas/instructions-clear" /%}}, {{% story name="Jamal" href="/6-big-ideas/instructions-clear" /%}} et {{% story name="Araceli" href="/6-big-ideas/instructions-clear" /%}} ont tous lu les instructions sur le formulaire FDA qui leur demandent d’inclure « tout ce qui est important ». Mais ils les ont interprétées de façon différente. 

Svetlana a expliqué à Gary qu’elle avait fourni deux exemples pour décrire les façons qu’avait son mari de lui faire mal. Elle pensait que ces deux exemples étaient suffisants pour démontrer à la Commission pourquoi elle avait peur de retourner chez elle. Et si le commissaire ne la croyait pas, à quoi bon fournir plus d’exemples? 

Jamal a expliqué que quand il a lu qu’il fallait inclure « tout ce qui est important », il a compris qu’il devait présenter un résumé de ses expériences importantes. Après tout, il était impossible de décrire toutes les fois où il a été harcelé, menacé ou agressé. 

Araceli a écrit sur le formulaire qu’elle avait été menacée. Elle a expliqué à Gary qu’elle pensait que ces menaces étaient justement ce qui était important. Elle savait qu’elle aurait la chance de lui en dire plus à l’audience. 

Sur le formulaire FDA, on invite les demandeurs à décrire les dangers auxquels « vous et votre famille » faites face. {{% story name="Lester" href="/6-big-ideas/instructions-clear" /%}} a compris qu’il s’agissait de sa famille immédiate. Il a expliqué à Gary qu’il avait mis l’accent sur son histoire à lui. L’indication au bas des instructions relative aux « personnes dans une situation semblable à la vôtre » était facile à manquer. 

{{< go-back "/6-big-ideas/instructions-clear#Lester" >}}

{{% story name="Mamun" href="/6-big-ideas/instructions-clear" /%}} a eu des ennuis en raison de sa façon de remplir la partie du formulaire où il devait indiquer les endroits où il a habité. Mamun n’a pas écrit l’adresse de sa tante, parce que le formulaire lui demandait d’écrire les adresses « où vous avez habité ». D’après lui, il n’était chez sa tante que temporairement, et il n’avait pas habité chez elle. Tout ce qu’il possédait et ses meubles étaient encore chez lui. Son courrier était encore livré chez lui. Son téléphone et son service internet étaient encore branchés. Mamun a expliqué à Gary qu’il « habitait » encore à sa résidence, même s’il était forcé de se cacher ailleurs. 

{{% story name="Pia" href="/6-big-ideas/instructions-clear" /%}} a expliqué à Gary qu’elle n’avait pas porté attention aux dates exactes au moment de remplir le formulaire. Elle n’avait pas réalisé à quel point il était important de donner la date exacte de son départ du travail. 

{{< go-back "/6-big-ideas/instructions-clear#Pia" >}}

Gary a demandé à {{% story name="Angela" href="/6-big-ideas/instructions-clear" /%}} : « Pourquoi pensez-vous que votre père vous cherche encore? » Il voulait savoir comment elle était au courant que son père était encore à sa recherche. Mais Angela a compris la question différemment. Elle pensait qu’il s’intéressait à ce qui motivait son père. Elle a expliqué les raisons pour lesquelles son père était à sa recherche. Lorsque Angela a compris ce que Gary voulait savoir, elle lui a expliqué ce qu’elle avait d’abord compris dans sa question.

{{% story name="Farzad" href="/6-big-ideas/instructions-clear" /%}} était confus, car Gary posait des questions qui allait de l’une à l’autre de ses arrestations. Quand il a compris ce qui était arrivé, il a expliqué à Gary qu’il avait mal compris. Il pensait que sa question portait sur la deuxième arrestation. Gary n’a pas eu l’air d’accepter cette explication. Gary pensait que ses questions avaient été très claires. Mais Gary s’est souvenu que Farzad avait soulevé le problème. Après l’audience, Gary a écouté l’enregistrement de ses questions et des réponses de Farzad. Il a alors compris pourquoi Farzad avait été confus. 

{{< go-on "/6-big-ideas/canada-is-great#Farzad" >}}

Quand Gary a demandé à {{% story name="Zoya" href="/6-big-ideas/canada-is-great" /%}} pourquoi elle était venue au Canada, il voulait qu’elle explique pourquoi elle avait quitté son pays. Elle pensait qu’il voulait savoir pourquoi elle était venue au Canada plutôt qu’ailleurs. Quand elle a compris ce qu’il avait voulu dire, elle lui a expliqué comment elle avait compris sa question. Puis, elle a expliqué pourquoi elle avait quitté son pays. 

{{< go-back "/6-big-ideas/canada-is-great" >}}