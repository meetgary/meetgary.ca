---
title: Making clear when an answer is an example or does not tell the full story
linkTitle: Making clear that it is just an example
weight: 60

---

{{% gears %}}

Gary will often ask the same question many times, in different ways. Sometimes this is because he has not understood the claimant’s answer. Sometimes it is because he wants to know more. But sometimes Gary does this to see if the claimant’s answer will change. When a claimant’s story is inconsistent, Gary gets suspicious.

When claimants answer Gary’s questions by giving examples, they have to be careful. The first time he asks, they might give one example, and the next time, they might give a different example. That can make Gary think that they have changed their story.

Claimants have avoided this problem by making clear to Gary that they are **just giving an example**, or that their answer **only tells part of the story**.
{{% /gears %}}

When Gary first asked {{% story name="Samuel" href="/6-big-ideas/memory" /%}} to describe his political activities, Samuel gave many examples of things that he did to help his party. Later, when Gary asked again, Samuel added another example. That made Gary suspicious. If Samuel had stressed that he was giving examples, it could have helped. Instead of saying “I started a student chapter,” he could have said, “For instance, I started a student chapter.” Instead of saying “I handed out flyers,” Samuel could have said, “For example, I handed out flyers.”

{{% story name="Kamala" href="/6-big-ideas/memory" /%}} had several reasons for not going to the police after she was assaulted. When Gary first asked her why, she gave two reasons. When he asked her again on the second day, she added another reason. Gary may have been less suspicious if Kamala explained on the first day that she had many reasons. Instead of saying “Because I did not want my family and friends to know,” Kamala could have said, “One reason was because I did not want my family and friends to know.” She could have used words like: “One of my big concerns was…” or “For one thing, I was worried about…”

{{< go-back "/6-big-ideas/memory#Kamala" >}}