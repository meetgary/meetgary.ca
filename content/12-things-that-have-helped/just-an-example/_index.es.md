---
title: Aclarar que una respuesta es sólo un ejemplo o no cuenta la historia completa
linkTitle: Aclarar que una respuesta es sólo un ejemplo
weight: 60

---

{{% gears %}}

A menudo, Gary hace la misma pregunta muchas veces de maneras distintas. A veces es porque no entiende la respuesta de la persona solicitante. A veces es porque desea saber más. Pero a veces Gary hace esto para ver si la respuesta de la persona solicitante cambia. Cuando la historia de una persona solicitante no es consistente, Gary sospecha.

Cuando las personas solicitantes dan ejemplos como respuesta a las preguntas de Gary, deben tener cuidado. La primera vez que pregunta, puede que den un ejemplo en particular, y la siguiente vez quizá den uno distinto. Eso puede hacer que Gary piense que han cambiado la historia.

Las personas solicitantes han evitado este problema aclarándole a Gary que están **dando un ejemplo solamente**, o que su respuesta **relata solamente una parte de la historia**.
{{% /gears %}}

La primera vez que Gary le preguntó a {{% story name="Samuel" href="/6-big-ideas/memory" /%}} que describiera sus actividades políticas, Samuel dio muchos ejemplos de cosas que hacía para ayudar a su partido. Después, cuando Gary preguntó de nuevo, Samuel añadió otro ejemplo. Esto hizo que Gary sospechara. Podría haber ayudado recalcar que estaba dando ejemplos. En vez de decir, “Empecé un comité estudiantil”, Samuel podría haber dicho: “Por ejemplo, empecé un comité estudiantil”. En vez de decir, “Distribuía panfletos”, Samuel podría haber dicho, “Por ejemplo, distribuía panfletos”.

{{% story name="Kamala" href="/6-big-ideas/memory" /%}} tenía muchas razones para no acudir a la policía tras ser agredida. La primera vez que Gary le preguntó por qué, ella dio dos razones. Cuando le preguntó otra vez el segundo día, ella añadió otra razón. Gary podría haber sospechado menos si Kamala hubiese explicado el primer día que tenía muchas razones. En vez de decir “Porque no quería que mi familia y mis amigos se enteraran”, Kamala podría haber dicho, “Una de las razones es que no quería que mi familia y mis amigos se enteraran”. Ella podría haber usado palabras como “Una de mis mayores preocupaciones era…” o “Entre otras cosas, me preocupaba que…”

{{< go-back "/6-big-ideas/memory#Kamala" >}}