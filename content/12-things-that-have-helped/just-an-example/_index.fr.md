---
title: Expliquer clairement qu’il s’agit d’un exemple ou que la réponse n’est qu’une partie de l’histoire
linkTitle: Expliquer clairement qu’il s’agit seulement d’un exemple
weight: 60

---

{{% gears %}}

Gary pose souvent la même question, mais différemment. Parfois, il le fait parce qu’il n’a pas compris la réponse du demandeur. Parfois, il le fait parce qu’il veut en savoir plus. Et parfois, il le fait pour savoir si la réponse du demandeur va changer. Quand l’histoire d’un demandeur change, Gary a des doutes.

Quand les demandeurs répondent aux questions de Gary par des exemples, ils doivent faire attention. À sa première question, ils peuvent donner un exemple; à sa deuxième question, ils peuvent donner un autre exemple. C’est pourquoi Gary risque de penser qu’ils ont changé leur histoire.

Certains demandeurs ont évité ce problème en précisant clairement qu’ils **donnent seulement un exemple**, ou que leur réponse **n’est qu’une partie de l’histoire**.
{{% /gears %}}

Pour répondre à la question de Gary au sujet de ses activités politiques, {{% story name="Samuel" href="/6-big-ideas/memory" /%}} a donné de nombreux exemples de ce qu’il faisait pour son parti. Quand Gary a reposé la même question plus tard, Samuel a ajouté un autre exemple, ce qui a semé des doutes dans la tête de Gary. Si Samuel avait précisé qu’il donnait des exemples, Gary aurait compris. Au lieu de dire : « J’ai fondé le chapitre étudiant, » il aurait pu dire : « Par exemple, j’ai fondé le chapitre étudiant. » Au lieu de dire : « J’ai distribué des dépliants, » il aurait pu dire : « Par exemple, j’ai distribué des dépliants. »

{{% story name="Kamala" href="/6-big-ideas/memory" /%}} avait de nombreuses raisons pour ne pas alerter la police après son agression. Quand Gary lui a demandé pourquoi, elle a donné deux raisons. Lorsqu’il a posé la même question le deuxième jour, elle a ajouté une autre raison. Gary aurait peut-être eu moins de doutes si Kamala avait d’abord expliqué qu’elle avait plusieurs raisons. Au lieu de dire « parce que je ne voulais pas que ma famille et mes amis le sachent », elle aurait pu dire : « Une des raisons, c’est que je ne voulais pas que ma famille et mes amis le sachent. » Elle aurait pu utiliser des mots comme : « Une de mes préoccupations était… » ou « Entre autres, j’étais inquiète de… »
 
{{< go-back "/6-big-ideas/memory#Kamala" >}}