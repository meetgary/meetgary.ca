---
title: 12 choses qui ont aidé
linkTitle: 12 choses qui ont aidé
weight: 30

---

Lorsque Gary a l’impression qu’un demandeur est malhonnête, il est parfois impossible de lui faire changer d’idée. Mais parfois les demandeurs peuvent l’aider à comprendre qu’ils disent la vérité. Ou, s’ils n’arrivent pas à convaincre Gary, ils pourront aider la Section d’appel des réfugiés ou la Cour fédérale à annuler une décision. 

Pour découvrir **12 choses qu’ont fait ou dit les demandeurs** qui ont aidé, cliquez sur les liens ci-dessous :

{{< children >}}
