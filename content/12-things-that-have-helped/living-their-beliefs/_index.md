---
title: Explaining how people live their beliefs
linkTitle: Explaining how people live their beliefs
weight: 100

---
{{% gears %}}
In any faith group, there are people who devote much of their time to learning about their faith. There are some who devote less time. And there are those who sincerely believe, but mostly have their minds on other things. 

This is also true in any political group. Some people are leaders. Some are active members who spend a lot of time on their cause. Some are supporters who give it what spare time they have. And others are simply well-wishers. 

When religious or political groups are persecuted, sometimes only the most visible or the most dedicated are targeted. But at other times, everyone connected to the group is in danger.

Gary sometimes assumes that every religious or politically active person has studied their belief system in depth. He asks questions to test how much they know about it. If he finds that they do not know very much, he thinks that they are only pretending to care about it.

Claimants have sometimes been able to help Gary to see that they are telling the truth by explaining in detail **how faith or politics fits into their daily lives**. Gary is then better able to gauge how much he should expect them to know.
{{% /gears %}}

If Gary had understood better how {{% story name="Huan" href="/6-big-ideas/memory" /%}} lives his faith, he might not have expected him to know a lot of Bible passages. Huan could have explained that he goes to church every week and says a prayer every night. He enjoys church as a place to meet people. He is comforted by the pastor’s talk of love and peace. When the pastor reads from the Bible, Huan lets the words wash over him. He has never read the Bible himself. He does not think that he needs to because he has put himself in the pastor’s hands, and the pastor’s words are much easier to understand. 

{{% story name="Sunny" href="/6-big-ideas/memory" /%}} became involved in a political party because he was very angry at the corruption and injustice that he saw around him. He encouraged his friends to join because he knew that they were angry too. Going to rallies with his friends felt like rooting for his home football team. He wore his party’s colours and got hoarse from cheering the people who made speeches. Half the time he could not hear clearly what they were saying, but it did not matter. They were on his side. If Gary had understood better why Sunny’s political party was important to him, he might not have expected him to know so much about its platform.

{{< go-on "/6-big-ideas/caution" >}}