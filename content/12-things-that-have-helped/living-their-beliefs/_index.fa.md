---
title: توضیح اینکه افراد چقدر با اعتقاداتشان زندگی می کنند
linkTitle: توضیح اینکه افراد چقدر با اعتقاداتشان زندگی می کنند
weight: 100

---
{{% gears %}}
در هر گروه ایمانی ، افرادی هستند که بیشتر وقت خود را صرف یادگیری در مورد ایمان خود می کنند. عده ای هستند که وقت کمتری را اختصاص می دهند. و کسانی هستند که صمیمانه اعتقاد دارند ، اما بیشتر آنها به چیزهای دیگر فکر می کنند.

این مسئله در هر گروه سیاسی نیز صادق است. برخی از افراد رهبر هستند. برخی از اعضای فعال هستند که زمان زیادی را صرف اهداف خود می کنند. برخی از حامیانی هستند که اوقات فراغت خود را به آن اختصاص می دهند. و دیگران به سادگی هوادار هستند.

هنگامی که گروه های مذهبی یا سیاسی مورد آزار و اذیت قرار می گیرند ، بعضی اوقات فقط افراد مشهور یا خاصترین افراد  هدف قرار می گیرند. اما در مواقع دیگر ، هر کسی که به گروه وصل شده است در معرض خطر است.
گری گاهی تصور می کند که هر فرد مذهبی یا سیاسی فعال ، عمیقا سیستم اعتقادی خود را مورد مطالعه قرار داده است. او سؤالاتی می پرسد تا امتحان کند چقدر آنها راجع به آن آگاهی دارند. اگر فهمید که آنها زیاد نمی دانند ، فکر می کند که آنها فقط وانمود می کنند که به آن اهمیت دهند.

متقاضیان گاهی اوقات توانسته اند با توضیح دادن جزئیات در مورد **چگونگی ایمان یا سیاست در زندگی روزمره** خود به گری کمک کنند تا ببیند که آنها حقیقت را می گویند. پس از آن گری قادر به سنجش میزان اطلاع آنها است.
{{% /gears %}}

اگر گری بهتر از ایمان {{% story name="هوان" href="/6-big-ideas/memory" /%}} در زندگیش اطلاع می داشت، شاید دیگر از او انتظار نداشت که تعداد زیادی از سوره های انجیل را بداند. هوان می توانست توضیح دهد که هر هفته به کلیسا می رود و بگوید که هر شب دعا می کند. از کلیسا به عنوان مکانی برای دیدار با مردم لذت می برد. او با صحبت های کشیش در مورد عشق و صلح آرامش می یابد. وقتی کشیش از کتاب مقدس می خواند ، هوان اجازه می دهد کلمات او را غسل دهند. او هرگز خود کتاب مقدس را نخوانده است. او اینگونه فکر نمی کند چرا که خودش را به دست کشیش سپرده است  ، و درک سخنان کشیش بسیار آسان تر است.

{{% story name="ساني" href="/6-big-ideas/memory" /%}} در یک حزب سیاسی درگیر شد زیرا او از فساد و بی عدالتی که در اطراف خود می دید بسیار عصبانی بود. او دوستان خود را تشویق به پیوستن کرد زیرا می دانست که آنها نیز عصبانی هستند. رفتن به تظاهرات با دوستانش احساس هم ریشگی در یک تیم فوتبال خانگی را داشت. او رنگ های حزب خود را پوشید و از تشویق افرادی که سخنرانی می کردند ، خسته شد. نیمی از زمان او نتوانست به وضوح آنچه را که می گفتند بشنود ، اما مهم نبود. آنها در کنار او بودند. اگر گری بهتر می فهمید که چرا حزب سیاسی سانی برایش مهم است ، شاید انتظار نمی داشت که او از مبانی حزبش اطلاع  داشته باشد.

{{< go-on "/6-big-ideas/caution" >}}