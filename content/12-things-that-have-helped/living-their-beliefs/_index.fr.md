---
title: Expliquer sa façon de vivre ses croyances
linkTitle: Expliquer sa façon de vivre ses croyances
weight: 100

---
{{% gears %}}
Dans tous les groupes confessionnels ou religieux, il y a des personnes qui consacrent beaucoup de temps à l’étude de leur foi. D’autres y consacrent moins de temps. Et d’autres ont la foi, mais ils se consacrent à autre chose. 

C’est aussi vrai pour les groupes politiques. Certaines personnes sont des leaders. D’autres sont membres et y consacrent beaucoup de leur temps. Et d’autres sont partisans et y accordent un peu de leur temps libre, alors que d’autres sympathisent. 

Lorsque les groupes religieux ou politiques font l’objet de persécution, parfois seules les personnes les plus visibles ou les plus dévouées sont ciblées. Mais parfois, toutes les personnes associées au groupe sont en danger. 

Gary présume parfois que toutes les personnes actives au sein de leur groupe politique ou religieux ont étudié les croyances connexes en profondeur. Il pose des questions pour examiner leur niveau de connaissance. S’il découvre qu’elles n’ont pas des connaissances très approfondies, il peut avoir l’impression qu’elles prétendent s’y intéresser. 

Les demandeurs ont parfois réussi à aider Gary à voir qu’ils disent la vérité en expliquant en détail **la place qu’occupe la foi ou la politique dans leur vie de tous les jours**. Gary peut alors mieux évaluer ce qu’ils devraient savoir. 
{{% /gears %}}

Si Gary avait mieux compris la façon de {{% story name="Huan" href="/6-big-ideas/memory" /%}} de vivre sa foi, il ne se serait pas attendu à ce qu’il connaisse les passages de la Bible. Huan aurait pu expliquer qu’il va à l’église toutes les semaines et qu’il prie tous les soirs. Qu’il aime aller à l’église pour rencontrer des gens et qu’il est rassuré par ce que dit le pasteur sur l’amour et la paix. Quand le pasteur lit un passage de la Bible, il se laisse imprégner par les mots. Il aurait pu expliquer qu’il n’a jamais lu la Bible, qu’il pense qu’il n’a pas besoin de la lire, car il s’en remet au pasteur, et les discours du pasteur sont beaucoup plus faciles à comprendre. 

{{% story name="Sunny" href="/6-big-ideas/memory" /%}} s’est joint à un parti politique parce qu’il était très fâché face à la corruption et à l’injustice dont il était témoin. Il a encouragé ses amis à participer avec lui, car ils étaient mécontents aussi. Aller aux rassemblements politiques avec ses amis était en quelque sorte comme aller au match de soccer de son équipe locale. Il portait les couleurs de son parti et sa voix devenait rauque tellement il criait pour encourager les personnes qui faisaient des discours. La moitié du temps, il n’entendait même pas ce qu’elles disaient, mais ça ne lui importait pas. Elles étaient de son côté. Si Gary avait mieux compris pourquoi son parti politique était si important pour Sunny, il n’aurait pas eu autant d’attentes par rapport à ses connaissances de la plateforme du parti.

{{< go-on "/6-big-ideas/caution" >}}