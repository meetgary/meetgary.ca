---
title: Explicar cómo las personas viven sus creencias
linkTitle: Explicar cómo las personas viven sus creencias
weight: 100

---
{{% gears %}}
En todo grupo religioso hay personas que dedican mucho de su tiempo a aprender sobre su religión. Hay personas que le dedican menos tiempo. Y hay quienes creen con sinceridad pero que en general tienen la mente en otras cosas.

Esto ocurre igualmente en todo grupo político. Algunas personas son líderes. Algunas son miembros activos que dedican mucho tiempo a su causa. Algunas son el tipo de simpatizantes que le dedican todo su tiempo libre. Y otras simplemente los apoyan.

Cuando un grupo religioso o político es objeto de persecución, a veces solamente se ataca a las personas más visibles o las más dedicadas. Pero en otros casos todas las personas vinculadas al grupo corren peligro.

Gary a veces presupone que todas las personas con participación activa en una religión o en un partido político han estudiado su sistema de creencia a fondo. Les hace preguntas para evaluar cuánto saben al respecto. Si descubre que no saben mucho, considera que sólo fingen interés.

A veces las personas solicitantes han podido ayudar a que Gary entienda que están diciendo la verdad explicando en detalle **el lugar que tiene la fe o la política en su vida diaria**. Así Gary puede determinar más fácilmente cuánto ha de esperar que sepan.
{{% /gears %}}

Si Gary hubiese entendido cómo {{% story name="Huan" href="/6-big-ideas/memory" /%}} vive su fe, tal vez no hubiese esperado que se supiera muchos pasajes de la Biblia. Huan podría haber explicado que va a la iglesia todas las semanas y dice una oración todas las noches. Disfruta la iglesia para conocer gente. Se siente reconfortado por las palabras de amor y paz del pastor. Cuando el pastor lee la Biblia, Huan deja que las palabras lo inunden. Nunca ha leído la Biblia por su cuenta. Piensa que no necesita hacerlo pues se ha puesto en las manos del pastor, y las palabras del pastor son mucho más fáciles de entender.

{{% story name="Sunny" href="/6-big-ideas/memory" /%}} se involucró con un partido político porque le daba mucha rabia la corrupción y la injusticia que veía a su alrededor. Animó a sus amigos a unirse porque sabía que sentían rabia también. Asistir a mítines con sus amigos era como darle ánimo a su equipo de fútbol local. Se vistió con los colores de su partido y quedó afónico de vitorear a las personas que dieron discursos. La mitad del tiempo no escuchaba claramente lo que decían, pero no importaba. Ellos estaban de su lado. Si Gary hubiese entendido mejor la importancia que daba Sunny al partido político, tal vez no habría esperado que conociera mejor la plataforma de su partido.

{{< go-on "/6-big-ideas/caution" >}}