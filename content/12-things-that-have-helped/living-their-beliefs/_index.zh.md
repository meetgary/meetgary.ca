---
title: 解释人们如何把信仰付诸于生活
linkTitle: 解释人们如何把信仰付诸于生活
weight: 100

---
{{% gears %}}
在任何宗教信仰组织里，都有一些人会奉献出很多时间学习他们的信仰。有一些人会献出较少的时间。还有一些，他们的信仰是真诚的，但是他们大多在想别的事情。 

在任何政治组织里也是这样。有些人是领导者。有些人是活跃的成员，为他们的事业花了很多时间。有一些是支持者，会提供他们有空闲的时间。另外一些人，只是简单的有良好的愿望。 

当宗教或者政治组织遭到迫害时，有些时候，只是针对那些最有辨识度或者最投入的成员。可是，另外的一些时候，每一个和组织有关系的人都有危险。

Gary有时假设每一个在宗教上和政治上积极的人都深入地学习了他们的信仰系统。他提问题来测试他们的了解有多少。如果他发现他们了解的不多，他会认为他们只是假装关心这件事。

申请人有些时候可以通过详细的解释**信仰或政治观点是怎样融入他们的日常生活的**，来帮助Gary了解他们在讲真话。这样，Gary可以更好地规量他预期他们该知道多少。
{{% /gears %}}

如果Gary能够更好地理解 {{% story name="Huan" href="/6-big-ideas/memory" /%}} 是怎么样在信仰中生活的，他可能就不会预期他会了解太多的圣经段落。Huan可以解释他每周都去教堂，他每天晚上都祷告。他喜欢在教会里认识人。牧师讲到“爱”和“和平”让他感到欣慰。当牧师读到圣经里的部分，Huan会被这些文字所感动。他自己从来没有读过圣经，他不认为有需要去读，因为他可以依靠牧师，而且牧师的话更容易理解。 

{{% story name="Sunny" href="/6-big-ideas/memory" /%}} 参与了一个政党，是因为他对周围看到的腐败和不公很气愤。他鼓励他的朋友们参加，因为他知道，他们也是气愤的。和他的朋友一起去参加集会，感觉就像和他们一起为他们家乡的足球队助兴一样。他会穿上他的政党的颜色的衣服，为向讲演的人们欢呼而兴奋。有一半儿的时候他听不清他们在说什么，但那也没关系，他们是站在他这一边的。如果Gary能够更好地理解Sunny的政党为什么对他来讲是重要的，他可能就不会预期他对它的政治纲领有太多的了解。

{{< go-on "/6-big-ideas/caution" >}}