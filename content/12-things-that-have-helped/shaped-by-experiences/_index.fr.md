---
title: Expliquer qu’on est façonné par nos expériences
linkTitle: Expliquer ses motivations
weight: 80

---

{{% gears %}}
Quand Gary essaie de se mettre à la place d’un demandeur, il imagine comment il se serait senti. Ce n’est pas la même chose que d’imaginer comment le demandeur se serait senti. En raison de sa formation, il se demande souvent ce qu’aurait fait « une personne raisonnable ». D’après Gary, cette « personne raisonnable » lui ressemble beaucoup.

Parfois, les demandeurs arrivent à aider Gary à comprendre que, tout comme lui, ils ont été influencés par leurs expériences. Leurs expériences ont été très différentes des siennes. Cela explique pourquoi **ils se sont sentis, ou ont agi ou pensé différemment** de lui, ou même de « la personne raisonnable » qu’il imagine.
{{% /gears %}}

Bien qu’elle avait reçu des menaces de mort, {{% story name="Sonia" href="/6-big-ideas/caution" /%}} n’a pas quitté son pays jusqu’à ce que la guérilla tente de l’assassiner. Gary ne comprend pas. Si Gary recevait une menace de mort, il serait terrifié. Mais il n’a jamais fait l’objet d’une telle menace. Toute sa vie, il a été en sécurité. Sonia a aidé Gary à comprendre que pour elle, avec le temps, les menaces troublantes de la guérilla faisaient partie du paysage. Le danger la rendait mal à l’aise. Parfois la peur l’agrippait au ventre. Mais la plupart du temps, elle essayait de ne pas y penser. Elle a expliqué que c’était comme pour les gens qui fument ou qui conduisent une voiture, elle connaissait les risques, mais elle les poussait à l’arrière de ses pensées et continuait à vivre au jour le jour.  

{{< go-back "/6-big-ideas/caution#Sonia" >}}

Gary présume qu’une personne raisonnable dans la situation de {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} aurait essayé de s’échapper de son mari bien avant. Pour aider Gary, Hee-Young a décrit ce que c’était de vivre pendant de nombreuses années sans pouvoir prendre de décisions. Son mari lui disait ce qu’elle devait porter et manger, où elle pouvait aller et avec qui elle pouvait parler. Elle n’avait jamais imaginé de demander de l’aide dans une ville étrangère dans un pays étranger. Elle a mis des années à comprendre qu’il était possible de s’échapper. Pendant de nombreuses années, elle n’avait jamais même eu cette idée. 

{{< go-back "/6-big-ideas/caution#Hee-Young" >}}

{{% story name="Boris" href="/6-big-ideas/canada-is-great" /%}} a aidé Gary à comprendre qu’il n’était pas prêt à fréquenter d’autres personnes ou à faire partie d’une communauté. Il a expliqué que quand il est arrivé au Canada, ses priorités étaient de trouver un appartement et de trouver un emploi. Après ça, il avait besoin de temps pour s’acclimater. Il a expliqué qu’il avait encore beaucoup de mal à faire confiance aux autres et qu’il voulait seulement être seul pour le moment. Une fois qu’il comprenait mieux Boris, Gary a compris qu’il ne faisait pas semblant d’être gay.

Gary ne comprenait pas pourquoi {{% story name="Marjani" href="/6-big-ideas/canada-is-great" /%}} avait peur de faire confiance à un interprète canadien et qu’elle ne voulait pas parler de son agression sexuelle. Marjani a raconté à Gary les rumeurs qui couraient dans sa communauté. On disait que certains interprètes révélaient ce qu’ils entendaient lors des entrevues d’immigration. Elle ne savait pas si les rumeurs étaient vraies, mais elle ne voulait pas prendre de risque. Elle a aidé Gary à comprendre à quel point cela aurait été épouvantable si cette information avait circulé.

Gary n’a jamais eu de raisons de ne pas faire confiance aux autorités canadiennes. Les policiers, les agents frontaliers et les agents de sécurité ont toujours été courtois avec lui. Pour aider Gary à comprendre le racisme dont il a été victime, {{% story name="Barrington" href="/6-big-ideas/canada-is-great" /%}} lui a raconté qu’il avait subi de la discrimination de la part de policiers et d’agents de sécurité. Il lui a parlé des propos racistes de son employeur. Il lui a raconté ce qu’ont fait et dit des étrangers à son égard. Cela a permis à Gary de mieux comprendre pourquoi Barrington ne faisait pas confiance aux Canadiens et pourquoi il n’avait pas fait sa demande d’asile plus tôt.