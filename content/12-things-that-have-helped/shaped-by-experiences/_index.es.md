---
title: Explicar cómo les han marcado sus experiencias 
linkTitle: Explicar una forma de sentir distinta
weight: 80

---

{{% gears %}}
Cuando Gary intenta ponerse en el lugar de una persona solicitante, se pregunta cómo se habría sentido él. Eso no es lo mismo que intentar imaginar cómo la persona solicitante se habría sentido. A causa de su capacitación, generalmente se pregunta cómo una “persona razonable” habría actuado. Desde el punto de vista de Gary, dicha “persona razonable” se le parece mucho.

A veces las personas solicitantes han logrado que Gary entienda que sus experiencias las han marcado, así como a él las suyas. Sus experiencias han sido muy distintas a las de él. Esto explica por qué **su forma de sentir, pensar o actuar fue distinta** a la de él o, según él, a la de una “persona razonable”.
{{% /gears %}}

A pesar de recibir muchas amenazas de muerte, {{% story name="Sonia" href="/6-big-ideas/caution" /%}} postergó irse del país hasta el día que la guerrilla de verdad intentó matarla. Eso Gary no lo comprende. Si alguna vez hubiese recibido una amenaza de muerte, se sentiría aterrorizado. Pero ese tipo de amenaza sería algo nuevo para él, que ha vivido a salvo toda su vida. Sonia ayuda a Gary a entender que, para ella, con el paso del tiempo, las amenazas de la guerrilla llegaron a transformarse en una especie de ruido de fondo incómodo. Pensar en el peligro la incomodaba. A veces el miedo le hacía un nudo en el estómago. Pero la mayor parte del tiempo simplemente intentaba no pensar en eso. Ella explicó que era como fumar o como conducir; sabía que el riesgo era real, pero se sacaba el pensamiento de la cabeza y seguía adelante.

{{< go-back "/6-big-ideas/caution#Sonia" >}}

Gary imagina que una persona razonable en el lugar de {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}} habría intentado huir de su esposo mucho antes. Pero Hee-Young le describe a Gary cómo era vivir tantos años sin tener permitido tomar ni una decisión. Su esposo decidía lo que ella vestía, lo que comía, adonde iba, con quién podía hablar. Ella nunca se imaginó que podría dejarlo y pedir ayuda en una ciudad desconocida en un país extranjero. Tardó años en darse cuenta que era posible escaparse. Durante muchos años, jamás se le ocurrió semejante idea.

{{< go-back "/6-big-ideas/caution#Hee-Young" >}}

{{% story name="Boris" href="/6-big-ideas/canada-is-great" /%}} ayudó a Gary a entender que aún no está listo para salir a citas ni ser parte de una comunidad. Le explica que cuando acababa de llegar a Canadá, sus prioridades eran encontrar apartamento y trabajo. Después necesitó tiempo para adaptarse. Aún le cuesta mucho confiar en la gente. Por ahora, nada más quiere estar solo. Entenderlo mejor ayudó a convencer a Gary que Boris no estaba fingiendo ser gay.

Gary no lograba entender por qué a {{% story name="Marjani" href="/6-big-ideas/canada-is-great" /%}} le daba miedo confiar a un intérprete canadiense detalles relacionados a la agresión sexual que sufrió. Marjani le contó a Gary de rumores que había oído en su comunidad. Se decía que algunos intérpretes sí comentan lo que oyen en las entrevistas de inmigración. Ella no sabía si dichos rumores eran ciertos, pero sentía que no podía correr ese riesgo. Marjani ayudó a Gary a entender lo desastroso que habría sido si esa información se hubiese dado a conocer.

Gary nunca ha tenido motivo alguno para desconfiar de las autoridades canadienses. Los oficiales de policía, agentes fronterizos y guardias de seguridad siempre lo han tratado con respeto. Para ayudar a Gary a entender el racismo que {{% story name="Barrington" href="/6-big-ideas/canada-is-great" /%}} ha vivido, éste le cuenta cómo fue maltratado por policías y guardias de seguridad canadienses. Le cuenta de cosas racistas que sus jefes dijeron. Le cuenta que gente desconocida lo atacó y lo insultó con palabras racistas. Eso ayuda a Gary a entender por qué Barrington no había confiado en los canadienses lo suficiente como para pedir asilo antes.