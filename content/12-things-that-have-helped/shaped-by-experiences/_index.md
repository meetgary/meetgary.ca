---
title: Explaining how they have been shaped by their experiences
linkTitle: Explaining why they felt differently
weight: 80

---

{{% gears %}}
When Gary tries to put himself in a claimant’s shoes, he asks himself how he would have felt. That is different from trying to imagine how the claimant might have felt. Because of his training, he often asks himself how a “reasonable person” would have acted. To Gary, this “reasonable person” looks a lot like Gary himself.

Sometimes claimants have been able to help Gary see that they, like he, have been shaped by their experiences. Their experiences have been very different from his. This explains why **they felt or thought or acted differently** than he, or his idea of a “reasonable person,” would.
{{% /gears %}}

Despite receiving many death threats, {{% story name="Sonia" href="/6-big-ideas/caution" /%}} put off leaving her country until the guerilla actually tried to kill her. Gary cannot understand this. If Gary ever received a death threat, he would be terrified. But that kind of threat would be new to him. He has lived his whole life in safety. Sonia helped Gary to understand that for her, over time, the guerrilla’s threats had become a kind of unsettling background noise. Whenever she thought about the danger, it made her uneasy. Sometimes fear hit her in the pit of her stomach. But most of the time, she just tried not to think about it. She explained that it was like smoking cigarettes or driving a car; she knew that the risk was real, but she pushed the thought to the back of her mind and carried on.

{{< go-back "/6-big-ideas/caution#Sonia" >}}

Gary imagines that a reasonable person in {{% story name="Hee-Young" href="/6-big-ideas/caution" /%}}’s shoes would have tried to escape from her husband long before now. But Hee-Young described for Gary what it was like to live for many years without being allowed to make any decisions. Her husband decided what she wore, what she ate, where she went, to whom she could speak. She never imagined that she could leave her husband and ask for help in a strange city in a foreign country. It took years before she realized that escape was possible. For many years, the thought simply never crossed her mind.

{{< go-back "/6-big-ideas/caution#Hee-Young" >}}

{{% story name="Boris" href="/6-big-ideas/canada-is-great" /%}} helped Gary to understand that he is not yet ready to date or to be part of a community. He explained that when he first arrived in Canada, his priorities were finding an apartment and finding a job. After that, he needed time to adjust. He still finds it very hard to trust people. For now, he just wants to be alone. Understanding Boris better helped to convince Gary that he was not just pretending to be gay.

Gary could not understand why {{% story name="Marjani" href="/6-big-ideas/canada-is-great" /%}} would be afraid to trust a Canadian interpreter with information about her sexual assault. Marjani told Gary about rumours she had heard in her community. People said that some interpreters do talk about what they have heard in immigration interviews. She did not know whether these rumours were true. But she felt she could not take that risk. She helped Gary to understand how devastating it would have been if this information had become known.

Gary has never had any reason to distrust Canadian authorities. Police officers, border agents, and security guards have always treated him with respect. To help Gary to understand the racism that he has experienced, {{% story name="Barrington" href="/6-big-ideas/canada-is-great" /%}} told him about being mistreated by Canadian police officers and security guards. He told him about racist things his employers had said. He told him about strangers who assaulted him and called him racist names. This helped Gary to understand why Barrington had not trusted Canadians enough to ask for refugee protection sooner.