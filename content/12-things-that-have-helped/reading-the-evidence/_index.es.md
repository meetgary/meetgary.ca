---
title: Leer cada documento detenidamente antes de enviarlo a la Comisión
linkTitle: Leer cada documento detenidamente
weight: 40

---

{{% gears %}}
A Gary le preocupa mucho cuando ve algo en los documentos de una persona solicitante que no coincide con la historia de la persona solicitante. Es posible que las personas solicitantes que leen sus documentos detenidamente antes de enviarlos a la Comisión puedan ayudar a disipar las sospechas de Gary. Si la persona nota un problema, como por ejemplo un error en el documento, tal vez pueda **arreglarlo**. O, si el problema es muy grave, tal vez **decida no utilizar el documento** en absoluto. En caso de que la persona decida usarlo, estará **mejor preparada para responder a las preguntas que Gary pueda tener al respecto**.
{{% /gears %}}

{{% story name="Gordon" href="/6-big-ideas/perfect-match" /%}} no notó que su psiquiatra se había equivocado al anotar su edad en el informe. Si lo hubiese notado, podría haberle pedido que la corrigiera y le enviara una copia nueva.

{{% story name="Ndidi" href="/6-big-ideas/perfect-match" /%}} se sintió sorprendida y confundida con las preguntas de Gary. Esto se debe a que no había notado, antes de la audiencia, que el informe policial no incluía la amenaza de muerte. Si se hubiese dado cuenta, tal vez habría estado mejor preparada para sugerir por qué faltaba ese detalle. Ella podría haber descrito el caos de la comisaría y, con eso, tal vez explicar mejor por qué el oficial no anotó perfectamente su historia palabra por palabra.

{{% story name="Lijuan" href="/6-big-ideas/perfect-match" /%}} no se dio cuenta que el informe médico solamente mencionaba el brazo fracturado y no las heridas en la cara. Si hubiese leído el informe detenidamente, tal vez se habría percatado de ese detalle. Así habría podido enviar a la Comisión las fotos que su familia le tomó con el labio cortado y la nariz sangrando.

La carta de la madre de {{% story name="Badma" href="/6-big-ideas/perfect-match" /%}} va a suscitar las dudas de Gary: ¿Realmente Badma le ocultaba a su madre sus peores experiencias? ¿O inventó esas experiencias y las añadió a su solicitud para darle más peso? Badma podría decidir presentar la carta a la Comisión y explicarle a Gary por qué no le había contado la historia completa a su madre. O bien, si Badma cuenta con bastantes pruebas adicionales en apoyo de su historia, podría decidir no presentar la carta de su madre como parte de su solicitud.

{{< go-on "/6-big-ideas/instructions-clear" >}}

{{% story name="Marjorie" href="/6-big-ideas/canada-is-great" /%}} podría haberle pedido a su padre que escribiera la carta de nuevo. Podría haberle dicho que se enfocara solamente en los puntos que ayudarían a Gary a entender por qué ella corre riesgo en su país. Mencionar que aquí gozaría de mayor prosperidad solamente podría causar que Gary sospechara que ella no era una refugiada.

{{< go-back "/6-big-ideas/canada-is-great#Marjorie" >}}
