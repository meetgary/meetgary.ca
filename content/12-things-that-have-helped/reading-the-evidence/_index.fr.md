---
title: Lire chacun des documents attentivement avant de les envoyer à la Commission 
linkTitle: Lire chacun des documents attentivement
weight: 40

---

{{% gears %}}
Gary est très perplexe quand il découvre que l’histoire du demandeur ne correspond pas à ce qui se trouve dans les documents du demandeur. Les demandeurs qui lisent soigneusement leurs documents avant de les envoyer à la Commission peuvent apaiser les doutes de Gary. S’ils remarquent un problème, comme une erreur dans un document, ils pourraient être en mesure de la **corriger**. Ou si le problème est vraiment sérieux, ils pourraient **décider de ne pas s’en servir** du tout. Même s’ils décident de s’en servir, ils pourraient être **mieux préparés à répondre aux questions de Gary**.
{{% /gears %}}

{{% story name="Gordon" href="/6-big-ideas/perfect-match" /%}} n’a pas remarqué que la psychiatre avait fait une erreur au sujet de son âge dans son rapport. S’il l’avait remarqué, il aurait pu lui demander d’apporter une correction et de lui renvoyer une autre copie.

{{% story name="Ndidi" href="/6-big-ideas/perfect-match" /%}} était surprise et confuse face aux questions de Gary. Elle n’avait pas remarqué avant l’audience que la menace de mort n’était pas mentionnée dans le rapport de la police. Si elle s’en était rendu compte, elle aurait été prête à suggérer une explication pour l’omission. Elle aurait pu décrire le chaos de la station de police. Cela aurait expliqué pourquoi l’agent n’avait pas écrit son histoire dans tous les détails. 

{{% story name="Lijuan" href="/6-big-ideas/perfect-match" /%}} n’avait pas réalisé que le rapport médical ne mentionnait que sa fracture au bras et non les blessures au visage. Si elle avait bien lu le rapport, elle aurait remarqué cette anomalie. Elle aurait pu envoyer à la Commission les photos qu’avait prises sa famille montrant sa lèvre coupée et son saignement de nez.  
	
La lettre de la mère de {{% story name="Badma" href="/6-big-ideas/perfect-match" /%}} suscitera des questions chez Gary : est-ce que Badma a vraiment caché ses pires expériences à sa mère? Ou bien a-t-elle inventé ces histoires pour donner du poids à sa demande? Badma peut décider de donner la lettre à la Commission et expliquer à Gary pourquoi elle n’a pas tout dit à sa mère. Ou, si Badma a beaucoup de preuves à l’appui de son histoire, elle peut décider de ne pas soumettre la lettre de sa mère.

{{< go-on "/6-big-ideas/instructions-clear" >}}

{{% story name="Marjorie" href="/6-big-ideas/canada-is-great" /%}} aurait pu demander à son père de réécrire sa lettre. Elle aurait pu lui demander de mettre l’accent sur ce qui peut aider Gary à comprendre pourquoi elle est en danger dans son pays. Le fait de mentionner qu’elle aura un avenir plus prospère au Canada contribue seulement aux doutes de Gary, qui se demande si elle est vraiment réfugiée.

{{< go-back "/6-big-ideas/canada-is-great#Marjorie" >}}