---
title: Reading each document carefully before sending it to the Board
linkTitle: Reading each document carefully
weight: 40

---

{{% gears %}}
Gary is very troubled if he sees something in a claimant’s documents that does not match the claimant’s story. Claimants who read their documents carefully before sending them to the Board may be able to allay Gary’s suspicions. If they notice a problem, such as a mistake in the document, they might be able to **fix it**. Or if the problem is very serious, they might **decide not to use the document** at all. Even if they do decide to use it, they will be **better prepared to answer Gary’s questions about it**.
{{% /gears %}}

{{% story name="Gordon" href="/6-big-ideas/perfect-match" /%}} did not notice that his psychiatrist made a mistake about his age in her report. If he had, he could have asked her to correct it and send him a new copy.

{{% story name="Ndidi" href="/6-big-ideas/perfect-match" /%}} was surprised and confused by Gary’s questions. This was because she did not notice before the hearing that the death threat was not included in the police report. If she had noticed, she might have been better prepared to suggest why it was missing. She could have described the chaos at the police station. This might have explained why the officer did not take down her story perfectly word for word.

{{% story name="Lijuan" href="/6-big-ideas/perfect-match" /%}} did not realize that her medical report only mentioned her broken arm and not the injuries to her face. If she had carefully read the report, she might have spotted this. Then she might have been able to send the Board photos that her family took showing her cut lip and bleeding nose.
	
The letter from {{% story name="Badma" href="/6-big-ideas/perfect-match" /%}}’s mother will raise questions in Gary’s mind: did Badma really hide her worst experiences from her mother? Or did she make up these experiences and add them to her claim to try to make it stronger? Badma might decide to give the letter to the Board and explain to Gary why she had not told her mother the full story. Or, if Badma has a lot of other evidence to support her story, she might decide not to submit her mother’s letter.

{{< go-on "/6-big-ideas/instructions-clear" >}}

{{% story name="Marjorie" href="/6-big-ideas/canada-is-great" /%}} could have asked her father to rewrite his letter. She could have told him to focus only on points that would help Gary understand why she is at risk in her country. Mentioning that she would be more prosperous here would only make Gary suspect that she was not really a refugee.

{{< go-back "/6-big-ideas/canada-is-great#Marjorie" >}}