---
title: 12 Things that have helped
linkTitle: 12 Things that have helped
weight: 30

---

When Gary thinks that a claimant is dishonest, sometimes nothing will change his mind. But sometimes claimants can help him to understand that they are telling the truth. And even if they do not succeed with Gary, they may be able to help the Refugee Appeal Division or the Federal Court to overturn his decision.

To learn about **12 Things that claimants have said or done** that have helped, click on the links below:

{{< children >}}