---
title: 其他资源
weight: 100
---
了解关于**申请难民程序过程**的信息，请看这里:

{{% button href="https://ccrweb.ca/en/uorap/hearing-preparation-kit" %}}University of Ottawa Refugee Assistance Program（渥太华大学难民协助项目）{{% /button %}}
{{% button href="https://www.cleo.on.ca/en/publications/refhearing" %}} Community Legal Education Ontario（安省社区法律教育）{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemKitTro.aspx" %}}The Refugee Board（难民委员会）{{% /button %}}
{{% button href="https://stepstojustice.ca/legal-topic/refugee-law" %}}Community Legal Education Ontario（安省社区法律教育）{{% /button %}}
<br />

了解 **怎样增补BOC表格**，请看这里:

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes" %}}The Refugee Board（难民委员会）{{% /button %}}
<br />

了解 **反对委员会拒绝难民保护的决定**, 请看这里:

{{% button href="http://lawfacts.ca/refugee/review" %}}Legal Aid Ontario（ 安省法律援助署）{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/filing-refugee-appeal/Pages/refugee1.aspx" %}}The Refugee Board（难民委员会）{{% /button %}}
<br />

**学习如何向委员会呈交文件**，请看这里:
{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}The Refugee Board（难民委员会）{{% /button %}}