---
title: Other sources
weight: 100
---
To learn about **the refugee claim process**, see here:

{{% button href="https://ccrweb.ca/en/uorap/hearing-preparation-kit" %}}University of Ottawa Refugee Assistance Program{{% /button %}}

{{% button href="https://www.cleo.on.ca/en/publications/refhearing" %}}Community Legal Education Ontario{{% /button %}}

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemKitTro.aspx" %}}The Refugee Board{{% /button %}}

{{% button href="https://stepstojustice.ca/legal-topic/refugee-law" %}}Community Legal Education Ontario{{% /button %}}

<br/>

To learn about **how to amend the BOC form**, see here:

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes" %}}The Refugee Board{{% /button %}}

<br/>

To learn about **fighting the Board’s decision to deny refugee protection**, see here:

{{% button href="http://lawfacts.ca/refugee/review" %}}Legal Aid Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/filing-refugee-appeal/Pages/refugee1.aspx" %}}The Refugee Board{{% /button %}}

<br/>

To learn about **submitting documents to the Board**, see here:
{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}The Refugee Board{{% /button %}}