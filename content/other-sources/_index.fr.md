---
title: Autres ressources
weight: 100
---
Pour en savoir plus sur **le processus de demande d’asile**, consultez :
{{% button href="https://ccrweb.ca/fr/paruo" %}}Projet d’assistance pour les réfugiés à l’Université d’Ottawa{{% /button %}}
{{% button href="https://www.cleo.on.ca/fr/publications/refhearingfr" %}}Éducation juridique communautaire Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/fr/demandes-asile/Pages/ClaDemKitTro.aspx" %}} Commission du statut de réfugié {{% /button %}}
{{% button href="https://stepstojustice.ca/fr/legal-topic/refugee-law" %}}Éducation juridique communautaire Ontario{{% /button %}}

<br />

Pour en savoir plus sur **ce qu’il faut faire pour modifier un formulaire FDA**, consultez :
{{% button href="https://irb-cisr.gc.ca/fr/demandes-asile/Pages/ClaDemGuide.aspx#changes" %}}Commission du statut de réfugié{{% /button %}}
<br />

Pour en savoir plus sur **contester la décision de la Commission de refuser la protection d’un réfugié**, consultez :

{{% button href="http://faitsdedroit.ca/refugee/review" %}}Aide juridique Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/fr/interjeter-appel-asile/Pages/refugee1.aspx" %}} Commission du statut de réfugié{{% /button %}}
<br />

Pour savoir **comment soumettre des documents à la Commission**, c'est par ici:
{{% button href="https://irb-cisr.gc.ca/fr/presenter-demande-asile/Pages/index3.aspx" %}}Commission du statut de réfugié{{% /button %}}