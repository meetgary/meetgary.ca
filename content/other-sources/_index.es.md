---
title: Otras fuentes
weight: 100
---
Para aprender más sobre **el proceso de solicitud de asilo**, ver aquí:

{{% button href="https://ccrweb.ca/en/uorap/hearing-preparation-kit" %}}University of Ottawa Refugee Assistance Program{{% /button %}}
{{% button href="https://www.cleo.on.ca/en/publications/refhearing" %}}Community Legal Education Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemKitTro.aspx" %}}The Refugee Board{{% /button %}}
{{% button href="https://stepstojustice.ca/legal-topic/refugee-law" %}}Community Legal Education Ontario{{% /button %}}

<br />

Para aprender más sobre el proceso para **cambiar su formulario BOC**, ver aquí:
{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes" %}}The Refugee Board{{% /button %}}
<br />

Para aprender más sobre el proceso de **contestar à la decisión de la Comisión**, ver aquí:
{{% button href="http://lawfacts.ca/refugee/review" %}}Legal Aid Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/filing-refugee-appeal/Pages/refugee1.aspx" %}}The Refugee Board{{% /button %}}
<br />

Para saber **cómo presentar documentos a la Comisión**, ver aquí:
{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}Información sobre cómo presentar documentos a la Comisión{{% /button %}}