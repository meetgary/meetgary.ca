---
title: مصادر أخرى
weight: 100
---
لمعرفة المزيد عن **عملية طلب اللجوء**، انظر هنا:

{{% button href="https://ccrweb.ca/en/uorap/hearing-preparation-kit" %}}  برنامج مساعدة اللاجئين بجامعة أوتاوا {{% /button %}}
{{% button href="https://www.cleo.on.ca/en/publications/refhearing" %}}التعليم القانوني للمجتمع في أونتاريو{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemKitTro.aspx" %}}مجلس اللجوء{{% /button %}}
{{% button href="https://stepstojustice.ca/legal-topic/refugee-law" %}}التعليم القانوني للمجتمع في أونتاري{{% /button %}}
<br />

للتعرف على **كيفية تعديل نموذج أساس تقديم الطلب**، انظر هنا:

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes" %}}مجلس اللجوء{{% /button %}}
<br />

لمعرفة المزيد حول **محاربة قرار المجلس عدم إعطاء حماية اللجوء**، انظر هنا:

{{% button href="http://lawfacts.ca/refugee/review" %}}المساعدة القانونية في أونتاريو{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/filing-refugee-appeal/Pages/refugee1.aspx" %}}مجلس اللجوء{{% /button %}}
<br />

تعرفوا على كيفية تقديم المستندات إلى المجلس :
{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}مجلس اللجوء{{% /button %}}