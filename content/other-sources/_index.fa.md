---
title: منابع دیگر
weight: 100
---
برای اطلاع از روند درخواست پناهندگی ، به اینجا مراجعه کنید

{{% button href="https://ccrweb.ca/en/uorap/hearing-preparation-kit" %}}University of Ottawa Refugee Assistance Program{{% /button %}}

{{% button href="https://www.cleo.on.ca/en/publications/refhearing" %}}Community Legal Education Ontario{{% /button %}}

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemKitTro.aspx" %}}The Refugee Board{{% /button %}}

{{% button href="https://stepstojustice.ca/legal-topic/refugee-law" %}}Community Legal Education Ontario{{% /button %}}

<br/>

برای کسب اطلاعات در مورد نحوه اصلاح فرم ، به اینجا مراجعه کنید (BOC)

{{% button href="https://irb-cisr.gc.ca/en/refugee-claims/Pages/ClaDemGuide.aspx#changes" %}}The Refugee Board{{% /button %}}

<br/>

برای کسب اطلاعات در مورد مبارزه با تصمیم برد پناهندگان ، اینجا را ببینید

{{% button href="http://lawfacts.ca/refugee/review" %}}Legal Aid Ontario{{% /button %}}
{{% button href="https://irb-cisr.gc.ca/en/filing-refugee-appeal/Pages/refugee1.aspx" %}}The Refugee Board{{% /button %}}

<br/>

برای یادگیری چگونگی ارصال مدارک اینجا را ببینید.
{{% button href="https://irb-cisr.gc.ca/en/applying-refugee-protection/Pages/index3.aspx" %}}The Refugee Board{{% /button %}}