---
title: Quiénes somos
weight: 40

---

El equipo de **Meet Gary** está compuesto de abogados e investigadores en materia de asilo y condición de refugiado. Contamos con décadas de experiencia colectiva representando a solicitantes ante la Comisión de Refugiados y estudiando cómo sus miembros toman decisiones.


## Nuestro sitio web

El equipo de Meet Gary ha tenido la suerte de asociarse con [_Consensus Enterprises_](https://consensus.enterprises), una organización impulsada por su misión que se dedica a cooperar con agencias gubernamentales, organizaciones sin fines de lucro y ONGs a fin de apoyar a unas de las poblaciones más vulnerables de Canadá. En particular, Consensus construye, alberga y apoya a [SettleNet.org](https://settlenet.org), una Comunidad de Prácticas subsidiada por el IRCC para el sector que asiste a las personas inmigrantes y refugiadas de Canadá.

Consensus brindó orientación técnica y apoyo técnico esenciales en creación y perfeccionamiento de nuestro sitio web y continúa aportando contribuciones inmensurables a este proyecto colaborando con nuestro equipo. Realmente ponen en práctica su eslogan: 

> _Ayudamos a equipos pequeños a hacer cosas grandes._

<p><img src="/images/LFO_E_Reverse_Colour_Tag.jpg" class="responsive"> </p>

Una generosa subvención otorgada por [The Law Foundation of Ontario](https://lawfoundation.on.ca/) hizo posible una corrección para lenguaje claro de este sitio web, así como su traducción al árabe, francés, mandarín y español. El equipo de Meet Gary es el único responsable por el contenido del sitio web.

Este sitio web se benefició en gran medida por una corrección para lenguaje claro 
realizada por Sally McBeth de [Clear Language and Design](http://clad.tccld.org/). Las traducciones fueron realizadas por: May Darwiche (árabe); Alexandria Ming Feng (mandarín); [María José Giménez](https://mariajosetranslates.com/contact/) (español). Se agradece comunicar toda consulta sobre el uso del texto en versión en inglés o traducidas al [correo electrónico de MeetGary].

## ¿Algún comentario?

¿Le ha parecido útil este sitio web? Agradecemos su opinión sobre cómo hacerlo más útil. 

{{% button href="/about-us/feedback" %}}Feedback{{% /button %}}