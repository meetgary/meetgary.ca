---
title: About us
weight: 40

---

The **Meet Gary** team is made up of refugee lawyers and refugee law researchers. We have decades of collective experience representing claimants before the Refugee Board and studying how Board members make their decisions.


## This website

The Meet Gary team has been fortunate to partner with [_Consensus Enterprises_](https://consensus.enterprises), a mission-driven organization that partners with government agencies, non-profits and NGOs to support some of Canada's most vulnerable populations. Most notably, Consensus built, hosts and supports [SettleNet.org](https://settlenet.org), an IRCC-funded Community of Practice for Canada's immigrant and refugee serving sector.

Consensus provided critical technical guidance and support in building and refining this website and continues to contribute immeasurably to this project through its ongoing collaboration with the team. They truly live up to their slogan:

> _We help small teams do big things._

<p><img src="/images/LFO_E_Reverse_Colour_Tag.jpg" class="responsive"> </p>

A generous grant from The Law Foundation of Ontario (https://lawfoundation.on.ca/) made possible a clear language edit of this website as well as its translation into Arabic, French, Mandarin and Spanish. The Meet Gary team is solely responsible for the website’s content.

Portions of this website benefited greatly from a clear language edit by Sally McBeth at [Clear Language and Design](http://clad.tccld.org/). Translations were provided by: May Darwiche (Arabic); Alexandria Ming Feng (Mandarin); [María José Giménez](https://mariajosetranslates.com/contact/) (Spanish). Inquiries about the use of the English or translated versions of the text should be sent to [MeetGarywebsite@gmail.com](mailto:MeetGarywebsite@gmail.com).

## Comments?

Did you find this website helpful? Please tell us how we can make it more useful!

{{% button href="/about-us/feedback" %}}Feedback{{% /button %}}