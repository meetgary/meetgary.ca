---
title: Su opinión sobre el sitio web
menuTitle: Su opinión
#hidden: true
type: feedback

intro: ¡Queremos saber su opinión! Apreciamos cualquier aporte sobre cómo mejorar nuestro sitio web.
confirm: Gracias por dedicar parte de su tiempo a comunicarse con nuestro equipo. Tomaremos en cuenta su aporte en nuestros esfuerzos por continuar mejorando nuestro sitio web.

form_id: 1FAIpQLSdK7ZCzg0pdFntaiZ6a5UnnGT3VOzXUKr7pPt3e_Jqzkv4FBQ

fields:
  - id: claimant-email
    name: emailAddress
    type: email
    label: Correo electrónico
  - id: claimant-source
    name: entry.1008183123
    type: text
    label: ¿Cómo supo de este sitio?
  - id: claimant-most-useful
    name: entry.521556952
    type: text
    label: ¿Qué le pareció más útil?
  - id: claimant-more-helpful
    name: entry.1884401757
    type: textarea
    label: ¿Qué habría hecho más útil este sitio web?

---