---
title: 网页反馈
menuTitle: 反馈
#hidden: true
type: feedback

intro: 请告诉我们您的想法！我们感谢您为了帮助我们改进网页而提供的反馈。
confirm: 谢谢您花时间和我们联系。我们在继续努力改进网页的过程中，会考虑到您的反馈。

form_id: 1FAIpQLSdK7ZCzg0pdFntaiZ6a5UnnGT3VOzXUKr7pPt3e_Jqzkv4FBQ

fields:
  - id: claimant-email
    name: emailAddress
    type: email
    label: 邮箱
  - id: claimant-source
    name: entry.1008183123
    type: text
    label: 你是如何听说这个网页的?
  - id: claimant-most-useful
    name: entry.521556952
    type: text
    label: 你觉得最有用的是什么?
  - id: claimant-more-helpful
    name: entry.1884401757
    type: textarea
    label: 什么会令这个网页更有用?

---