---
title: Commentaires sur le site
menuTitle: Commentaires
#hidden: true
type: feedback

intro: Dites-nous ce que vous en pensez! Nous apprécions vos commentaires dans le but d’améliorer le site.
confirm: Nous vous remercions d’avoir pris le temps de nous contacter. Nous tiendrons compte de vos commentaires dans le cadre de l’amélioration continue du site. 

form_id: 1FAIpQLSdK7ZCzg0pdFntaiZ6a5UnnGT3VOzXUKr7pPt3e_Jqzkv4FBQ

fields:
  - id: claimant-email
    name: emailAddress
    type: email
    label: Courriel
  - id: claimant-source
    name: entry.1008183123
    type: text
    label: Comment avez-vous découvert le site?
  - id: claimant-most-useful
    name: entry.521556952
    type: text
    label: Qu’est-ce qui a été le plus utile?
  - id: claimant-more-helpful
    name: entry.1884401757
    type: textarea
    label: Comment le site aurait-il pu être encore plus utile?

---