---
title: بازخورد وب سایت
menuTitle: بازخورد
#hidden: true
type: feedback

intro: لطفا به ما بگویید که شما چه فکر می کنید! ما از بازخورد شما در بهبود وب سایت خود قدردانی می کنیم.
confirm: ممنون که وقت خود را برای تماس با ما سپری کردید. ما در تلاش برای بهبود مداوم وب سایت بازخورد شما را در نظر خواهیم گرفت.


form_id: 1FAIpQLSdK7ZCzg0pdFntaiZ6a5UnnGT3VOzXUKr7pPt3e_Jqzkv4FBQ

fields:
  - id: claimant-email
    name: emailAddress
    type: email
    label: آدرس ایمیل
  - id: claimant-source
    name: entry.1008183123
    type: text
    label: چگونه با این وبسایت آشنا شدید؟
  - id: claimant-most-useful
    name: entry.521556952
    type: text
    label: چه چیزی بیشتر مفید بود؟
  - id: claimant-more-helpful
    name: entry.1884401757
    type: textarea
    label: چه چیزی باعث مفیدتر شدن این وب سایت می شود؟

---