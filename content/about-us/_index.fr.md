---
title: L’équipe
weight: 40

---

L’équipe de **Voici Gary** regroupe des avocats spécialisés en droit des réfugiés et des chercheurs en droit des réfugiés. Ensemble, nous avons une expérience collective de plusieurs décennies dans la représentation des demandeurs auprès de la Commission de l’immigration et du statut de réfugié et dans l’étude des décisions des commissaires.


## Le site Web

L’équipe de Voici Gary a eu la chance de collaborer avec [_Consensus Enterprises_](https://consensus.enterprises), une organisation alignée sur sa mission de soutenir les organismes gouvernementaux, les organisations à but non lucratif et les ONG qui viennent en aide aux populations vulnérables du Canada. Notamment, Consensus a bâti et est maintenant l’hôte de [SettleNet.org](https://settlenet.org), une communauté de pratique financée par IRCC à l’intention du secteur de[MeetGarywebsite@gmail.com](mailto:MeetGarywebsite@gmail.com)s services auprès des immigrants et des réfugiés du Canada.

Consensus a fourni des conseils techniques et un soutien essentiels dans la conception et l’amélioration du site, et continue de contribuer grandement à ce projet par sa collaboration avec l’équipe. Ils sont vraiment à la hauteur de leur slogan :

> _Nous aidons les petites équipes à faire de grandes choses._

<p><img src="/images/LFO_F_Reverse_Colour_Tag.jpg" class="responsive"> </p>

Une généreuse subvention de [La Fondation du droit de l’Ontario](https://lawfoundation.on.ca/) a permis d’obtenir une révision en langage clair et simple du site Web, de même que des traductions en arabe, en français, en mandarin et en espagnol. L’équipe de Voici Gary est seule responsable du contenu du site Web. 

La version anglaise de ce site Web a été grandement améliorée par la révision en langage simple et clair de Sally McBeth de [Clear Language and Design](http://clad.tccld.org/). Les traductions ont été préparées par : May Darwiche (arabe); Alexandria Ming Feng (mandarin); et [María José Giménez](https://mariajosetranslates.com/contact/) (espagnol). Si vous souhaitez utiliser la version en anglais ou les versions traduites, veuillez faire parvenir votre demande à [MeetGarywebsite@gmail.com](mailto:MeetGarywebsite@gmail.com).

## Commentaires?

Avez-vous trouvé ce site utile? Dites-nous comment nous pouvons le rendre encore plus utile!

{{% button href="/about-us/feedback" %}}Commentaires{{% /button %}}