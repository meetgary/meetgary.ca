---
title: ملحوظاتك عن الموقع
menuTitle: ملحوظاتك عن الموقع
#hidden: true
type: feedback


intro: الرجاء إخبارنا برأيكم! نحن نقدر ردودكم في تحسين موقعنا.
confirm: شكرًا لكم على الوقت الذي استغرقتموه في الاتصال بنا. سنأخذ ملاحظاتكم بعين الاعتبار بينما نسعى جاهدين لتحسين الموقع باستمرار.

form_id: 1FAIpQLSdK7ZCzg0pdFntaiZ6a5UnnGT3VOzXUKr7pPt3e_Jqzkv4FBQ

fields:
  - id: claimant-email
    name: emailAddress
    type: email
    label: Email address
  - id: claimant-source
    name: entry.1008183123
    type: text
    label:  كيف سمعتم عن هذا الموقع؟ 
  - id: claimant-most-useful
    name: entry.521556952
    type: text
    label: ما الذي وجدتموه أكثر فائدة؟
  - id: claimant-more-helpful
    name: entry.1884401757
    type: textarea
    label:  ما كان من الممكن أن يجعل هذا الموقع الإلكتروني أكثر فائدة؟


---

