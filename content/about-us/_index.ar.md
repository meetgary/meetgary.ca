---
title: من نحن
weight: 40

---
يتكون فريق **قابلوا غاري** من محامي لجوء وباحثين في قانون اللجوء. لدينا عقود من الخبرة الجماعية في تمثيل المتقدمين أمام مجلس اللجوء ودراسة كيفية اتخاذ أعضاء المجلس لقراراتهم.

## هذا الموقع

كان فريق "قابلوا غاري" محظوظاً لشراكته مع [_Consensus Enterprises_](https://consensus.enterprises)وهي منظمة إرسالية المنحى تتشارك مع الوكالات الحكومية والمنظمات غير الربحية والمنظمات غير الحكومية لدعم بعض السكان الأكثر ضعفاً في كندا. والأهم من ذلك، أن كونسينس قامت ببناء واستضافة ودعم [SettleNet.org](https://settlenet.org)وهو مجتمع مهني ممول من دائرة الهجرة واللجوء والمواطنة الكندية للقطاع الذي يخدم المهاجرين واللاجئين في كندا.

قدمت كونسينسس التوجيه والدعم التقنيين المهمين في بناء هذا الموقع الإلكتروني وتنقيحه وتستمر في المساهمة بشكل لا حد له في هذا المشروع من خلال تعاونها المستمر مع الفريق. إنهم يرقون حقًا إلى شعارهم:

> _نحن نساعد الفرق الصغيرة على القيام بأشياء كبيرة._

<p><img src="/images/LFO_E_Reverse_Colour_Tag.jpg" class="responsive"> </p>

منحة سخية من مؤسسة القانون في أونتاريو (https://lawfoundation.on.ca/) جعلت من الممكن تحرير لغة واضحة لهذا الموقع بالإضافة إلى ترجمته إلى العربية والفرنسية والماندرين والإسبانية. فريق قابلوا غاري هو المسؤول الوحيد عن محتوى الموقع الإلكتروني.


استفاد هذا الموقع بشكل كبير في تحرير اللغة الواضحة بواسطة سالي ماكبيث من Clear Language and Design 
) تمت الترجمة من قبل: مي درويش (العربية) ؛ أليكساندريا مينغ فنغ (الماندرين) ؛ ماريا http://clad.tccld.org/) 
 خوسيه جيمينيز (الإسبانية https://mariajosetranslates.com/contact/) ؛ وميريام ويب (الفرنسية). يجب إرسال الاستفسارات حول استخدام النسخة الإنجليزية أو النسخ المترجمة من النص إلى [email](mailto:meetgarywebsite@gmail.com).


## تعليقات؟

هل وجدتم هذا الموقع مفيداَ؟ من فضلكم إخبارنا كيف يمكننا أن نجعله أكثر فائدة!

{{% button href="/about-us/feedback" %}}ردود الفعل{{% /button %}}



