---
title: 关于我们
weight: 40

---

**见一见Gary／Meet Gary** 团队，是由难民律师和难民法研究人员组成的。在一起，我们有数十年的经验，在难民委员会代表申请人以及研究委员会委员如何做他们的决定。


## 这个网站

Meet Gary团队有幸和 [_Consensus Enterprises_](https://consensus.enterprises)合作 。Consensus Enterprises 是一个为使命驱动的机构，它和政府机构及非营利组织合作，支持加拿大的一些最弱势的人群。尤其，Consensus负责建立，主持和支持维护 [SettleNet.org](https://settlenet.org)，一个IRCC资助的加拿大的移民和难民类服务的工作社区。

Consensus为构建和完善这个网站提供了关键的技术指导和支持，它还通过和团队的持续合作，继续为这个项目做出不可估量的贡献。他们真正做到了他们的口号：

> _我们帮助小团队做大事情。_

<p><img src="/images/LFO_E_Reverse_Colour_Tag.jpg" class="responsive"> </p>

[The Law Foundation of Ontario](https://lawfoundation.on.ca/)／安省法律基金会 的慷慨资助令我们的网页得以获得专业的清晰语言编辑，以及翻译成阿拉伯文，法文，中文和西班牙文。Meet Gary 团队为本网页内容全权负责。

本网页得利于，Sally McBeth at [Clear Language and Design](http://clad.tccld.org/) 的清晰语言编辑。翻译由: May Darwiche (阿拉伯文); Alexandria Ming Feng (中文); [María José Giménez](https://mariajosetranslates.com/contact/) (西班牙文) (法文)提供。如有关于使用本网页英文或其他翻译语言内容的问题，应与[MeetGarywebsite@gmail.com](mailto:MeetGarywebsite@gmail.com)联系。

## 建议?

你觉得这个网站有帮助吗？请告诉我们如何把它做得更有用！

{{% button href="/about-us/feedback" %}} 反馈{{% /button %}}